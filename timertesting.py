#!/usr/bin/python

from ReadIni import ReadIni
import threading
import multiprocessing
import main
#import faux
import timer
import sourcemanager

ufaux = main.koppismear('faux.ini')

fauxmgrcfg = ReadIni('sources_voctest.ini')
fauxtmrcfg = ReadIni('timers_voctest.ini')
fauxtmrcfg = fauxtmrcfg['timer']

running = multiprocessing.Event()

fauxmgr = sourcemanager.manager(fauxmgrcfg, ufaux.controls)
fauxtimer = timer.timer(fauxtmrcfg, running, fauxmgr)
