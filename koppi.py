#!/usr/bin/python
# -*- coding: utf-8 -*-

"""koppi.py: main runner of the background processes, taking commands etc
main.ini:
[ports]
port = module.class
[port]
option = value

[connObjects]
connobject = module.class
[connobject]
port = port # from [ports]
option = value

[dataSources]
dataSource = module.class
[dataSource]
connobject = connobject # from [connObjects]
variables = var1,var2,var3
interval = 1 #seconds
option = value

[dataSinks]
dataSink = module.class
[dataSink]
connobject = connobject # from [connObjects]
variables = var1,var2,var3
interval = 1 #seconds
option = value

[controls]
control = module.class
[control]
connobject = connobject # from [connObjects]
name = str #channel name in module.class
value = 0,1 #possible values (csv or range 0-n)

"""

import multiprocessing
import threading
import time
import os
import logging
import logging.config
import threadedDatamanager
import controlmanager
import controltimer
import utils

from time import sleep
from ReadIni import ReadIni


class koppismear():

    class solidlist(object):
        def __init__(self, maxn):
            self.maxn = maxn
            self.lst = list()

        def get(self):
            return(self.lst)

        def put(self, val):
            self.lst.insert(0, val)
            if len(self.lst) > self.maxn:
                self.lst = self.lst[0:-1]

    def __init__(self, inifile):
        self.inifile = inifile
        self.controlrunning = multiprocessing.Event()
        self.datarunning = multiprocessing.Event()
        self.initialize()

    def initialize(self):
        self.cfg = ReadIni(self.inifile)
        self.pid = str(os.getpid())
        if "pidfile" in self.cfg["general"]:
            self.pidfile = self.cfg["general"]["pidfile"]
        else:
            self.pidfile = "/tmp/koppismear_pid"
        if os.path.isfile(self.pidfile):
            print("Pidfile %s already exists! Another instance of koppismear\
 might be running. Check if this is the case and remove the pidfile if not." % self.pidfile)
            return
        file(self.pidfile, 'w').write(self.pid)
        self.sourcecfg = ReadIni(self.cfg["files"]["sourcecfg"])
        # Old sources.ini
        self.timercfg = ReadIni(self.cfg["files"]["timercfg"])
        # ini file for timer
        if "datadir" in self.cfg["general"]:
            self.datadir = self.cfg["general"]["datadir"]
            try:
                os.makedirs(self.datadir)
            except OSError:
                if not os.path.isdir(self.datadir):
                    raise
        else:
            self.datadir = '.'
        self.tstampcol = self.cfg["general"]["tstampcol"]
        try:
            self.dolog = self.cfg["general"]["logging"]
            self.logname = self.cfg["general"]["logname"]
        except KeyError:
            self.dolog = False
        self.messenger = None  # CHANGE TO REAL MESSENGER!
        self.datalock = threading.Lock()
        # To prevent race condition between data sources and sinks
        self.outvars = dict()
        # For recording the data from data sources
        self.latest = dict()
        self.latestlock = threading.Lock()
        # For keeping books of when the last data was recorded
        # for each variable
        self.maxn = dict()  # How many observations in latest per variable
        if self.dolog:
            logging.config.fileConfig(self.dolog)
            self.logger = logging.getLogger(self.logname)
        self.currSource = ['starting']
        self.currSourcelock = threading.Lock()
        self.ports = self.createport(self.cfg["ports"], createlock=True)
        self.connObjects = self.createobj(self.cfg["connObjects"])
        self.dataSources = self.createsrc(self.cfg["dataSources"])
        self.dataSinks = self.createsink(self.cfg["dataSinks"])
        self.controls = self.createsrc(self.cfg["controls"])
        for so in list(self.dataSources.keys()):
            for v in self.dataSources[so]['obj'].variables:
                self.outvars[v] = dict()
                try:
                    self.latest[v] = self.solidlist(self.maxn[v])
                except KeyError:
                    self.latest[v] = self.solidlist(1)
        try:
            # For PID controls etc that need feedback
            # from data and use the available controls
            self.reacts = self.createreact(self.cfg["reacts"], self.controls)
            self.controls.update(self.reacts)
            # reacts use control.setstate to set the desired
            # value; the setpoint is set with react.setstate
            self.dataSinks.update(self.reacts)
            # feedback to reacts is given via the sink.write
        except KeyError:
            self.reacts = None
        self.controlmanager = controlmanager.manager(cfg=self.sourcecfg,
                                                     controls=self.controls,
                                                     logger=self.logger)
        self.controltimer = controltimer.timer(cfg=self.timercfg,
                                               running=self.controlrunning,
                                               controlmanager=self.controlmanager,
                                               messenger=self.messenger,
                                               currSource=self.currSource,
                                               currSourcelock=self.currSourcelock,
                                               logger=self.logger)
        self.datamanager = threadedDatamanager.manager(sinks=self.dataSinks,
                                                       datasources=self.dataSources,
                                                       outvars=self.outvars,
                                                       latest=self.latest,
                                                       lock=self.datalock,
                                                       latestlock=self.latestlock,
                                                       currSource=self.currSource,
                                                       currSourcelock=self.currSourcelock,
                                                       tstampcol=self.tstampcol,
                                                       logger=self.logger)
        self.datatimer = threadedDatamanager.timer(datamanager=self.datamanager,
                                                   running=self.datarunning,
                                                   logger=self.logger)
        self.dthread = threading.Thread(target=self.datatimer.run)
        self.hk = utils.HouseKeeping(self.outvars, self.latest,
                                     self.controltimer.dostop)

    def go(self):
        """Start the measurements"""
        self.logger.info('Starting run %s', self.controltimer.name)
        self.controltimer.prime()
        self.datarunning.set()
        self.controltimer.start()
        self.dthread.start()

    def datago(self):
        """Start only recording data from sources"""
        self.logger.info('Recording data')
        self.datarunning.set()
        if not self.dthread.is_alive():
            self.dthread.start()

    def pause(self):
        """Pause everything, including data recording"""
        self.logger.info('Pausing everything')
        self.controltimer.pause()
        self.datarunning.clear()

    def pauserun(self):
        """Pause the source changes - need "cont" to continue"""
        self.logger.info('Pausing measurements')
        self.controltimer.pause()

    def cont(self):
        """Continue measurements after pause"""
        self.logger.info('Continuing measurements')
        self.datarunning.set()
        self.controltimer.cont()

    def stop(self):
        """Stop measurements - need "go" to start again"""
        self.logger.info('Stopping measurements')
        print("Remember to use quitnow() if you want to stop measuring for good")
        self.controltimer.stop()
        self.datarunning.clear()
        self.datatimer.stop()
        try:
            self.dthread.join()
            self.dthread = threading.Thread(target=self.datatimer.run)
        except RuntimeError:  # If not started yet
            pass

    def quitnow(self):
        if self.dthread.is_alive():
            self.stop()
        os.unlink(self.pidfile)

    def restart(self):
        """Restart measurements (stop-prime-start)"""
        self.logger.info('Restarting measurements')
        self.controltimer.restart()

    def reinit(self, inifile=None):
        """Reinitialize - read old inifile again or read new inifile"""
        if inifile:
            self.inifile = inifile
        self.logger.info('Reinitialising with inifile %s', self.inifile)
        self.quitnow()
        self.initialize()

    def setsource(self, nextsource):
        """Manually set the source, record change if cfg tells to do so"""
        (source, delay, recchange) = self.controlmanager.setSource(nextsource)
        if recchange:
            with self.currSourcelock:
                self.currSource[0] = source

    def help(self):
        pass

    def readcmd(self):
        pass

    def createport(self, indict, createlock=False):
        """Create dictionary of port objects"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            if 'timeout' in self.cfg[p]:
                self.cfg[p]['timeout'] = int(self.cfg[p]['timeout'])
            mod = __import__(pcfg[0])
            print(pcfg[0])
            if createlock:
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                    (**self.cfg[p])),
                                   ('lock', threading.Lock())])
            else:
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                    (**self.cfg[p]))])
        return outdict

    def createobj(self, indict):
        """Create dictionary of connection objects - mediators between ports
and data inputs and outputs"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            pl = self.ports[self.cfg[p]['port']]
            # includes the port itself and the lock as dict entries
            if len(self.cfg[p]) > 1:
                objcfg = self.cfg[p]
                objcfg.pop('port')
            else:
                objcfg = dict()
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (port=pl, logger=self.logger, **objcfg))])
        return outdict

    def createsrc(self, indict):
        """Create dictionary of data sources or control objects"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            co = self.connObjects[self.cfg[p]['connobject']]
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
                srccfg.pop('connobject')
            else:
                srccfg = dict()
            if 'value' in srccfg:
                if str(srccfg['value']).count('-') == 1:
                    srccfg['value'] = tuple([float(a)
                                             for a in
                                             srccfg['value'].split('-')])
                elif str(srccfg['value']).count(',') > 0:
                    srccfg['value'] = [int(a)
                                       for a in srccfg['value'].split(',')]
                else:
                    try:
                        srccfg['value'] = int(srccfg['value'])
                    except ValueError:
                        pass  # Leave as is
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (connobject=co, **srccfg))])
        return outdict

    def createsink(self, indict):
        """Create dictionary of data sinks (they differ from sources and controls in that they need the time stamp column as parameter)"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            co = self.connObjects[self.cfg[p]['connobject']]
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
                srccfg.pop('connobject')
                srccfg["datadir"] = self.datadir
                try:
                    recsource = self.to_bool(srccfg.pop('recsource'))
                except KeyError:
                    recsource = False
            else:
                srccfg = dict()
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (connobject=co, tstampcol=self.tstampcol,
                                 recsource=recsource,
                                 **srccfg))])
            try:
                n = outdict[p]['obj'].average
            except AttributeError:
                print("No average")
                n = 1
            for v in outdict[p]['obj'].variables:
                try:
                    if self.maxn[v] < n:
                        self.maxn[v] = n
                except KeyError:
                    self.maxn[v] = n
        return outdict

    def to_bool(self, value):
        """
        Converts 'something' to boolean. Raises exception for invalid formats
        Possible True  values: 1, True, "1", "TRue", "yes", "y", "t"
        Possible False values: 0, False, None, [], {}, "", "0", "faLse", "no", "n", "f", 0.0,
        Taken from user Petrucio at Stackoverflow
        """
        if str(value).lower() in ("yes", "y", "true",  "t", "1"):
            return True
        if str(value).lower() in ("no",  "n", "false", "f", "0", "0.0",
                                  "", "none", "[]", "{}"):
            return False
        raise Exception('Invalid value for boolean conversion: ' + str(value))

    def createreact(self, indict, controls):
        """Create dictionary of react objects that take input from a data source and give output to a control"""
        outdict = dict()
        for p in sorted(indict.keys()):
            pcfg = indict[p].split('.')
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
            else:
                srccfg = dict()
            if 'value' in srccfg:
                if srccfg['value'].count('-') == 1:
                    srccfg['value'] = tuple([float(a)
                                             for a in
                                             srccfg['value'].split('-')])
                elif srccfg['value'].count(',') > 0:
                    srccfg['value'] = [int(a)
                                       for a in srccfg['value'].split(',')]
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (controls=controls, logger=self.logger,
                                 **srccfg))])
        return outdict

    def createlogs(self, indict):
        """Create the logger objects (not used at the time?)"""
        self.log = logging.getLogger(__name__)
        for p in sorted(indict.keys()):
            pcfg = indict[p].split('.')
            srccfg = self.cfg[p]
            try:
                level = srccfg.pop('level')
            except KeyError:
                level = None
            mod = __import__(pcfg[0])
            handler = getattr(mod, pcfg[1])(**srccfg)
            if level is not None:
                handler.setLevel(getattr(mod, level))
            self.log.addHandler(handler)

# if __name__ == 'main':
#     koppi = koppismear()
