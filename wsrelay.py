#!/usr/bin/python3
"""WSRelay 

tools for using Waveshare RPi relay boards (3- and 8-channel boards) as relay controllers for koppismear. 
"""

import RPi.GPIO as GPIO
import time
from itertools import cycle

class wsrelconn(object):
    """Connection object for a Waveshare relay board. 
    port: faux, with an empty obj and a threading.Lock as lock. 
    nrel: number of channels on the board (3 or 8)
    timeout: time (seconds) to wait for the channel to become available when setting state"""
    def __init__(self, port, nrel=3, logger=None, timeout = 1):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        self.nrel = nrel
        self.port = port['obj']
        self.lock = port['lock']
        self.logger = logger.getChild(suffix = "WSRelay")
        self.timeout = timeout
        ## self.logger.debug('WSRelayconn: setting up relay module with %s channels' % repr(self.nrel))
        if self.nrel == 3:
            self.relays = {1: 26, 2: 20, 3: 21}
        elif self.nrel == 8:
            self.relays = {1: 5, 2: 6, 3: 13, 4: 16, 5: 19, 6: 20, 7: 21, 8: 28}
        else:
            self.logger.critical('nrel must be 3 or 8!')
            raise ValueError
        [GPIO.setup(a, GPIO.OUT) for a in self.relays.values()]
        [(GPIO.output(a, GPIO.HIGH), time.sleep(0.1)) for a in self.relays.values()]

    def setval(self, relay, state):
        if state == 1:
            state = GPIO.LOW
        elif state == 0:
            state = GPIO.HIGH
        try:
            t = time.time()
            while self.lock.locked():
                time.sleep(0.01)
                if time.time() - t >= self.timeout:
                    self.logger.warning('timeout while setting channel state!')
                    return False
            with self.lock:
                GPIO.output(self.relays[int(relay)], state)
                return True
        except:
            return False

class wsrelay(wsrelconn):
    """Control object for a single channel on a Waveshare relay board.
    connobject: wsrelconn object
    name: channel number of relay on connobject (1-3 or 1-8)
    nick: distinct name for wsrelay instance
    value: allowable values for state setting, must be [0, 1] for 2-state relays"""
    def __init__(self, connobject, name, nick, value=[0, 1]):
        self.connobject = connobject['obj']
        self.channel = name ## Individual channel number
        self.nick = nick ## Nick for the channel
        self.value = value ## List of acceptable values
        self.logger = self.connobject.logger.getChild(suffix = self.nick)
        self.lock = self.connobject.lock
        self.relays = self.connobject.relays
        self.setstate(0)
        self.val = 0

    def setstate(self, value):
        if value in self.value:
            if self.setval(self.channel, value):
                self.logger.debug('set to %s' % value)
                self.val = value
            else:
                self.logger.debug('Exception in setting value!')
                pass

    def getstate(self):
        return self.val

class wvici16(wsrelconn):
    """For controlling VICI multiport valve selector over adam. 

    connobject: wsrelconn object
    nick: distinct name for the VICI valve
    cstep: channel number of step relay on the wsrelconn
    chome: channel number of home relay on the wsrelconn
    value: number of channels available (NOTE! Just 1 number, no range or distinct numbers), channel 1 is home."""

    def __init__(self, connobject, nick, cstep, chome, value=16):
        self.connobject = connobject['obj']
        self.step = wsrelay(connobject, cstep, 'step', [0, 1])
        self.home = wsrelay(connobject, chome, 'home', [0, 1])
        self.nick = nick
        self.value = value
        self.portiter = None  # Are created in the next step (self.gohome)
        self.gasport = None  # Are created in the next step (self.gohome)
        self.gohome()

    def dostep(self):
        self.step.setstate(1)
        time.sleep(0.05)
        self.step.setstate(0)
        self.gasport = next(self.portiter)

    def goton(self, n):
        if n > self.value | n == 0:
            raise ValueError('%s out of range!' % n)
        while self.gasport != n:
            self.dostep()
            time.sleep(0.325)

    def gohome(self):
        self.home.setstate(1)
        time.sleep(0.05)
        self.home.setstate(0)
        self.portiter = cycle(list(range(self.value + 1))[1:])
        self.gasport = next(self.portiter)

    def getstate(self):
        return self.gasport

    def setstate(self, value):
        if value == 1:
            self.gohome()
        else:
            self.goton(value)

