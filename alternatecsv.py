#!/usr/bin/python

import os
import time
import datetime
import re
import threading
import timerutils

class base(object):
    def __init__(self, port):
        pass


class mediator(base):
    """Hand over faux port and real lock to the writer"""

    def __init__(self, port, logger=None):
        base.__init__(self, port['obj'])
        self.base = port['obj']
        self.lock = port['lock']
        self.tmpfilelock = threading.Lock()
        self.logger = logger.getChild(suffix="csv")


class writer(base):
    """CSV writer Capable of writing character-delimited text files with
defined columns, checks columns when opening file and creates a new
one with a timestamp if necessary.

Capable of timed roll-over of files (configurable to seconds, minutes,
hours, days, months (= 30 days), years (= 365 days), counting either
from time of creation or previous midnight.

Capable of size-based rollover of files, configurable to B-GiB
accuracy; if file size limit is exceeded during a day and midnight is
true, give a true time stamp for the new file.

Saves epoch time as well as ISO 8006 time stamp in local time.

Parameters:
connobject: connection object (faux)
variables: string: variable names (other than timestamp, epochtime, Source),
    separated by separator defined in sep
outvars: dictionary from the main process, that is updated by the data sources
outfile: string: basename for output file, including extension
interval: int: interval of writing one observation into the file, in seconds
tstampcol: string: name of time stamp column
average: int: average over how many observations
sizelim: string: limit for file size
sep: string: separator character
recsource: boolean: record the sample source in column "Source"
timelim: string: limit maximum age of file before rollover
    (for example 1 d = 1 day)
midnight: boolean: set file timestamp to previous midnight
checkinterval: int: time between checking file size, in seconds
maxwait: int: maximum time in seconds to wait for variables to become available before they are set to NA for a row 

    """

    def __init__(self, connobject, variables, outvars, outvarslock,
                 latest, latestlock,
                 currsource, currsourcelock, outfile, interval,
                 tstampcol, datarunning, delay=0, average=1, sizelim="10 MB",
                 sep=',', recsource=False, timelim=False,
                 midnight=False, midnightfmt = '%Y-%m-%d 00-00-00',
                 datestampfmt = '%Y-%m-%d %H-%M-%S', timestampfmt = '%Y-%m-%d %H:%M:%S',
                 datadir=".", checkinterval=3600,
                 maxwait=None, maxdone=100, name='CSVout', datestamp=True, timeprec=0, usethread=False):
        self.connobject = connobject['obj']
        self.lock = self.connobject.lock
        self.base = self.connobject.base
        self.midnightfmt = midnightfmt
        self.tmpfilelock = self.connobject.tmpfilelock
        self.logger = self.connobject.logger.getChild(suffix=name)
        self.checkinterval = checkinterval
        self.outvars = outvars
        self.outvarslock = outvarslock
        self.latest = latest
        self.latestlock = latestlock
        self.currsource = currsource
        self.currsourcelock = currsourcelock
        self.sep = sep
        self.datadir = datadir
        self.variables = variables.split(self.sep)
        self.average = average
        self.tstampcol = tstampcol
        self.tstampfmt = timestampfmt
        self.timeprec = timeprec
        self.sleeptime = eval(f"1e{-(self.timeprec+2)}")
        self.delay = timerutils.divsum(delay)
        self.interval = timerutils.divsum(interval)
        self.sizelim = self.parsesize(sizelim)
        self.datestamp = datestamp
        self.datestampfmt = datestampfmt
        self.timestampfmt = timestampfmt
        self.timelim = timelim
        if self.timelim:
            self.timelim = self.parsetime(timelim)
        self.recsource = recsource
        if self.recsource:
            self.extracols = [self.tstampcol, 'epochtime', 'Source']
        else:
            self.extracols = [self.tstampcol, 'epochtime']
        self.colrow = self.extracols + self.variables
        self.colrow = self.sep.join(self.colrow)  # Column row for new files
        self.ctime = time.time()
        self.midnight = midnight
        self.running = datarunning
        self.doing = list()
        self.done = list()
        self.invars = dict()
        if maxwait is None:
            self.maxwait = 5 * self.interval
        else:
            self.maxwait = timerutils.divsum(maxwait)
        self.maxdone = maxdone
        self.dostop = threading.Event()
        self.basename = outfile.split(r'[_\.]')[0] # To recognize current files for rsync ignore file
        self.filecreator(outfile)  # Outfile is base name and
        # extension of data file
        self.name = name
        while self.tmpfilelock.locked():
            sleep(self.sleeptime)
        self.tmpfilelock.acquire()
        try:
            with open('csvoutname.tmp', 'r') as f:
                lines = f.readlines()
        except FileNotFoundError:
            f = open('csvoutname.tmp', 'w')
            f.flush()
            f.close()
        else:
            with open('csvoutname.tmp', 'w') as f:
                for line in lines:
                    if not re.match(self.basename.split('.')[0], line):
                        f.write(line)
        self.tmpfilelock.release()
        if usethread:
            self.thread = threading.Thread(target=self.run_threaded)
        else:
            self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def filecreator(self, filename, newfile=False, oversize=False):
        """ Looks for latest file that starts with the base file name;
if latest file is too large or old, create new file with timestamp.
Notice that . and _ are reserved characters in the name!"""
        mismatch = False
        filename_list = re.split(r'[_\.]', filename)
        if len(filename_list) == 2:  # No timestamp in filename
            filename_list.append(filename_list[1])
            filename_list[1] = None
        if not newfile:
            basename = filename_list[0]
            extension = filename_list[2]
            files = os.listdir(self.datadir)
            files = [f for f in files if re.split(r'[_\.]', f)[0] == basename and
                     f.endswith(extension)]
            if not files:
                # If no file by that name is found, create one
                # jump straight to file creation
                newfile = True
            else:
                # If files with that basename found, seek the latest and
                # check its size and if the first row holds the right columns
                files.sort()
                latestfile = files[len(files)-1]
                latestfilepath = os.path.join(self.datadir, latestfile)
                stats = os.stat(latestfilepath)
                self.outfile = open(latestfilepath, mode='r')
                colline = self.outfile.readline().strip('\n')
                filename_list = re.split(r'[_\.]', latestfile)
                if self.timelim:
                    newfile = self.timelimcheck(filename_list)
                self.outfile.close()
                if not newfile:
                    if stats.st_size >= self.sizelim:
                        self.logger.info('File size limit exceeded, starting \
new file')
                        newfile = True
                        oversize = True
                    elif not all(a in self.extracols + self.variables
                                 for a in colline.split(self.sep)):
                        self.logger.warning("Columns in file don't match \
variables! Starting new file")
                        newfile = True
                        mismatch = True
                    elif not all(b in colline.split(self.sep)
                                 for b in self.extracols + self.variables):
                        self.logger.warning("Variables don't match \
columns in file! Starting new file")
                        newfile = True
                        mismatch = True
        if newfile:
            if not self.datestamp:
                filename_list[1] = str(len(files)+1)
            else:
                if self.midnight and not oversize and not mismatch:
                    filename_list[1] = time.strftime(self.midnightfmt,
                                                     time.localtime())
                else:
                    filename_list[1] = time.strftime(self.datestampfmt,
                                                     time.localtime())
            filename = "{a[0]}_{a[1]}.{a[2]}".format(a=filename_list)
            filenamepath = os.path.join(self.datadir, filename)
            self.outfile = open(filenamepath, mode='w+')
            self.outfile.write(self.colrow + '\n')
            self.outfile.flush()
            if self.datestamp:
                self.ctime = time.mktime(time.strptime(filename_list[1],
                                                       '%Y-%m-%d %H-%M-%S'))
                self.lastcheck = self.ctime
            else:
                self.ctime = time.time()
                self.lastcheck = self.ctime
        else:
            self.outfile = open(latestfilepath, mode='a+')
        self.lastcheck = time.time()
        self.logger.info('Opened file %s for output of variables %s',
                         self.outfile.name, repr(self.variables))
        while self.tmpfilelock.locked():
            time.sleep(self.sleeptime)
        self.tmpfilelock.acquire()
        with open('csvoutname.tmp', 'r') as f:
            lines = f.readlines()
        with open('csvoutname.tmp', 'w') as f:
            for line in lines:
                if not re.match(self.basename.split('.')[0], line):
                    f.write(line.strip('\n') + '\n')
            f.write(self.outfile.name.split('/')[len(self.outfile.name.split('/')) - 1] + '\n')
        self.tmpfilelock.release()

    def filecheck(self):
        """Checks file size, creates new file with timestamp if necessary"""
        filename = self.outfile.name
        oversize = False
        filename_list = re.split(r'[_\.]', os.path.basename(filename))
        if len(filename_list) == 2:  # No timestamp in filename
            filename_list.append(filename_list[1])
            filename_list[1] = None
        stats = os.stat(filename)
        newfile = False
        if self.timelim:
            newfile = self.timelimcheck(filename_list)
        if not newfile:
            if stats.st_size >= self.sizelim:
                self.logger.info('File size limit exceeded, starting \
new file')
                oversize = True
                newfile = True
        if newfile:
            self.outfile.close()
            self.filecreator(os.path.basename(filename), newfile=True,
                             oversize=oversize)
        else:
            self.lastcheck = time.time()

    def timelimcheck(self, filename_list):
        """Check file creation time from name or first row; signal to create
new file if too old"""
        if filename_list[1] is None or not self.datestamp:
            line1 = self.outfile.readline()
            if not line1 == '':
                # Datetime is first column
                ctime = line1.strip('\n').split(self.sep)[0]
                if self.midnight:
                    self.ctime = time.mktime(time.strptime(ctime.split(' ')[0],
                                                           '%Y-%m-%d'))
                else:
                    self.ctime = time.mktime(datetime.datetime.timetuple(datetime.datetime.strptime(ctime,
                                                                        self.timestampfmt))) ## Timestamp in rows is dash-dash, colon-colon
            else:
                # File without lines -> take current time
                self.ctime = time.time()
        else:
            if self.midnight:
                self.ctime = time.mktime(time.strptime(filename_list[1].split(' ')[0],
                                                       '%Y-%m-%d'))
            else:
                self.ctime = time.mktime(time.strptime(filename_list[1],
                                                       self.datestampfmt)) ## Timestamp in file names in all dashes
        self.logger.debug('timelimcheck: ctime %s, time %s, timelim %s' % (self.ctime, round(time.time()), self.timelim))
        if time.time() - self.ctime >= self.timelim:
            self.logger.info('File age limit exceeded, starting \
            new file')
            return True
        return False

    @staticmethod
    def parsesize(size):
        """Parse kb, mb et al into numbers"""
        units = {"B": 1, "KB": 10**3, "MB": 10**6, "GB": 10**9, "TB": 10**12}
        number, unit = [string.strip() for string in size.split()]
        return int(float(number) * units[unit])

    @staticmethod
    def parsetime(intime):
        """Parse S into seconds, M into minutes, H into hours, d into days, w
into weeks, m into months, Y into years"""
        units = {"S": 1, "M": 60, "H": 3600, "d": 3600*24, "w": 7*3600*24,
                 "m": 30*3600*24, "Y": 365*3600*24}
        number, unit = [string.strip() for string in intime.split()]
        return(int(float(number) * units[unit]))

    def run(self):
        """Read the outvars 5 times per interval, check for new data that's become available; if some data is missing, create an entry in list "doing". Insert available data into rows in list "doing"; when a row is full or is older than max wait, write it into the file, add timestamp into list "done". Rows with missing values block consequent writes to keep the arrow of time flying in the right direction in the output file. This function runs continuously as a thread that's started in __init__."""
        while True:
            while not self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('stopping')
                    self.outfile.close()
                    return
                time.sleep(0.5)
            self.previouswrite = timerutils.preveven(self.interval)
            while self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('stopping')
                    self.outfile.close()
                    self.running.clear()
                    return
                while not self.latestlock.acquire(timeout=self.sleeptime):
                    if self.dostop.is_set():
                        self.logger.debug('stopping')
                        self.outfile.close()
                        self.running.clear()
                        return
                self.logger.debug(
                    'acquired latestlock 1')
                try:
                    self.logger.debug("Checking latest..")
                    self.latest_max = max(self.latest[s].get() for s in self.variables)[0]
                except (KeyError, TypeError, IndexError) as e:
                    self.logger.exception(e)
                    self.latestlock.release()
                    time.sleep(min([self.interval/5, 5]))
                    continue
                finally:
                    self.latestlock.release()
                self.logger.debug(
                    'released latestlock 1;latest_max %s, previouswrite %s' % (self.latest_max, self.previouswrite))
                while (time.time() - self.previouswrite) < (self.interval + self.delay):
                    time.sleep(min([self.interval/5, 5]))
                    self.logger.debug('latest_max %s, previouswrite %s' % (
                        self.latest_max, self.previouswrite))
                    if not self.running.is_set():
                        break
                    if len(self.doing) > 0:
                        while not self.outvarslock.acquire(timeout=self.sleeptime):
                            if not self.running.is_set():
                                break
                        while not self.latestlock.acquire(timeout=self.sleeptime):
                            if not self.running.is_set():
                                break
                        self.logger.debug('checking doing list')
                        for t in self.doing:
                            self.logger.debug(
                                'trying %s in doing' % t)
                            self.dovars = [v for v in self.variables
                                           if self.latest[v + ".nextread"].get()[0] <=
                                           self.previouswrite + 2*self.interval
                                           and not
                                           self.latest[v].get()[0] in self.doing]
                            for v in self.variables:
                                if not v in list(self.invars[t].keys()):
                                    if t in list(self.outvars[v].keys()):
                                        self.invars[t][v] = self.outvars[v][t]
                            if (all(v in list(self.invars[t].keys()) for v in self.dovars) or (time.time() - t) > self.maxwait) and t == min(self.doing):
                                self.invars[t].update(
                                    {v: 'NA' for v in self.variables if not v in list(self.invars[t].keys())})
                                self.logger.debug(
                                    'writing at %s' % t)
                                self.writerow(self.invars[t])
                                self.doing.remove(t)
                                self.done = self.done + [t]
                                self.invars.pop(t)
                                while len(self.done) > self.maxdone:
                                    self.done.pop(0)
                        self.latestlock.release()
                        self.outvarslock.release()
                        time.sleep(self.sleeptime)
                    while not self.latestlock.acquire(timeout=self.sleeptime):
                        if not self.running.is_set():
                            return
                    self.logger.debug(
                        'acquired latestlock 2')
                    self.logger.debug("Checking latest..")
                    self.latest_max = max(self.latest[s].get() for s in self.variables)[0]
                    self.logger.debug("Latest max now %s" % self.latest_max)
                    self.latestlock.release()
                    self.logger.debug(
                        'released latestslock 2')
                if not self.latest_max in self.doing + self.done:
                    self.previouswrite = self.latest_max
                    self.logger.debug(
                        'getting new data at %s' % self.latest_max)
                    self.invars[self.latest_max] = dict()
                    self.invars[self.latest_max] = self.addextracols(
                        self.invars[self.latest_max], self.latest_max)
                    
                    if (not all(a == self.latest_max
                                for a in
                                list(self.latest[s].get()[0] for s in self.variables
                                                 if self.latest[s + ".nextread"].get()[0]
                                                 <= self.previouswrite + 2*self.interval))) or len(self.doing) > 0:
                        self.doing = self.doing + [self.latest_max]
                    else:
                        while True:
                            if not self.running.is_set():
                                return
                            if self.outvarslock.acquire(timeout=self.sleeptime):
                                break
                        self.logger.debug(
                            'acquired outvarslock 3')
                        for v in self.variables:
                            try:
                                self.invars[self.latest_max].update({v: self.outvars[v][self.latest_max]})
                            except KeyError:
                                self.invars[self.latest_max].update({v: "NA"})
                        self.outvarslock.release()
                        self.logger.debug(
                            'released outvarslock 3')
                        self.writerow(self.invars[self.latest_max])
                        self.logger.debug('wrote row %s' % repr(self.invars[self.latest_max]))
                        self.invars.pop(self.latest_max)
                        self.done = self.done + [self.latest_max]
                while len(self.done) > self.maxdone:
                    self.done.pop(0)
                time.sleep(min([self.interval/5, 5]))

    def addextracols(self, vals, latest_max):
        vals.update({self.tstampcol: datetime.datetime.strftime(
            datetime.datetime.fromtimestamp(latest_max), self.timestampfmt), 'epochtime': latest_max})
        if self.recsource:
            while True:
                if not self.running.is_set():
                    return
                if self.currsourcelock.acquire(timeout=self.sleeptime):
                    break
            vals.update({'Source': self.currsource[0]})
            self.currsourcelock.release()
        return vals

    def writerow(self, vals):
        """Write data row ('vals') to output file after adding extra columns
such as timestamps and source if applicable"""
        outrow = list()
        for col in self.extracols + self.variables:
            try:
                if vals[col] is None:
                    vals[col] = 'NA'
                outrow.append(vals[col])
            except KeyError:
                outrow.append('NA')
                self.logger.debug('variable {0} not found in row at \
                {1}'.format(col, time.strftime('%H:%M', time.localtime())))
        if all(k == 'NA' for k in outrow):
            self.logger.debug('Empty row!')
        unknown = [a for a in list(vals.keys()) if a not in [self.tstampcol, 'epochtime', 'Source']
                   + self.variables]
        if len(unknown) > 0:
            self.logger.debug('unknown variables in input: \
            {0}'.format(' '.join(unknown)))
        with self.lock:
            self.logger.debug('writing %s',
                              self.sep.join(map(str, outrow)))
            self.outfile.write(self.sep.join(map(str, outrow)) + '\n')
            self.outfile.flush()
            self.logger.debug('wrote')
        now = time.time()
        if ((now - self.lastcheck >= self.checkinterval) or
                (now - self.ctime >= self.timelim)):
            # Check for file size once every checkinterval seconds
            self.filecheck()

    def checknextread(self):
        with self.latestlock:
            nextread = min(self.latest[v + ".nextread"].get()[0] for v in self.variables)
        return nextread

    def write(self, row):
        """Write method for datamanager"""
        self.writerow(row)

    def run_threaded(self):
        rows = dict()
        self.rowslock = threading.Lock()
        while not self.running.is_set():
            if self.dostop.is_set():
                self.logger.debug('stopping')
                self.outfile.close()
                return
            time.sleep(min([self.interval/5, 5]))
        self.collectors = dict()
        (self.lastwrite, self.nextwrite) = (timerutils.preveven(self.interval), timerutils.nexteven(self.interval))
        ## self.logger.warning(f"Initial next write is {self.nextwrite}, last write {self.lastwrite}")
        while True:
            if self.dostop.is_set():
                self.logger.debug('stopping')
                self.outfile.close()
                return
            if not self.running.is_set():
                time.sleep(min([self.interval/5, 5]))
                continue
            if time.time() >= self.nextwrite + self.delay:
                self.collectors.update({self.nextwrite: threading.Thread(target = self.rowcollector, name = self.nextwrite, args=(self.variables, self.nextwrite, self.nextwrite + self.maxwait, rows))})
                self.collectors[self.nextwrite].start()
                ## self.logger.debug(f"next write was {self.nextwrite}")
                self.nextwrite = timerutils.nexteven(self.interval) - self.delay
                ## self.logger.debug(f"next write is {self.nextwrite}, last write is {self.lastwrite}, interval is {self.interval}")
            coltopop = list()
            for c in self.collectors:
                if self.collectors[c].is_alive():
                    self.collectors[c].join(timeout = self.sleeptime)
                if c > self.lastwrite:
                    if not self.collectors[c].is_alive():
                        ## self.logger.debug(f"Popping {c} from collectors")
                        self.writerow(rows.pop(c))
                        self.lastwrite = c
                        coltopop.append(c)
            [self.collectors.pop(s) for s in coltopop]
            time.sleep(min([self.interval/5, 5]))


    def updatelatest(self, nextread, latest):
        latest.update([(v, self.latest[v].get()[0]) for v in latest.keys()])
        nextread.update([(v, self.latest[v + '.nextread'].get()[0]) for v in nextread.keys()])
        return (nextread, latest)

    def rowcollector(self, variables, rowtime, dyingtime, rows):
        allfound = 0
        latest = dict()
        latest.update([(v, None) for v in variables])
        nextread = dict()
        nextread.update([(v, None) for v in variables])
        row = self.addextracols(dict(), rowtime)
        self.logger.debug(f"{rowtime}: thread starting")
        while time.time() < dyingtime and self.running.is_set():
            while True:
                if not self.running.is_set():
                    try:
                        self.lockgetter(self.rowslock, (self.dostop,), rows.update, {rowtime: row})
                    except InterruptedError:
                        return
                    return
                try:
                    (nextread, latest) = self.lockgetter(self.latestlock, (self.dostop,), self.updatelatest, nextread, latest)
                except InterruptedError:
                    return
                if self.outvarslock.acquire(timeout=self.sleeptime):
                    break
            for v in [s for s in variables if not s in row.keys()]:
                if (latest[v] < rowtime and nextread[v] > rowtime + self.interval) or (latest[v] > rowtime and nextread[v] > rowtime + 2*self.interval):
                    self.logger.debug(f"{rowtime}: slower input variable {v} than output? (latest {latest[v]}, nextread {nextread[v]}") 
                    row.update({v: None})
                    continue
                try:
                    keys = list(self.outvars[v])
                    keys.reverse()
                    for k in keys:
                        self.logger.debug(f"{rowtime}: testing for {k} in {v}")
                        if k > rowtime - self.interval/2 and k < rowtime + self.interval/2:
                            break
                        if k < rowtime - self.interval/2:
                            raise KeyError
                    row.update({v: self.outvars[v][k]})
                except (KeyError, UnboundLocalError):
                    self.logger.debug(f"{rowtime}: {v} not found in outvars")
                    continue
            self.outvarslock.release()
            if all([v in row for v in variables]):
                self.logger.debug(f"{rowtime}: all variables found")
                allfound = 1
                break
            time.sleep(min([5, self.interval/5]))
        if not allfound:
            self.logger.debug(f"{rowtime}: not all variables found")
            row.update([(v, None) for v in [s for s in variables if not s in row.keys()]])
        while True:
            if self.dostop.is_set():
                self.logger.debug(f"{rowtime}: stopped while waiting for rowslock")
                return
            if self.rowslock.acquire(timeout = self.sleeptime):
                break
        rows.update({rowtime: row})
        self.rowslock.release()
        self.logger.debug(f"{rowtime}: finished")
        return
        
            
    def stop(self):
        self.dostop.set()

    def lockgetter(self, lock, events, fun=None, *args):
        while True:
            if any(e.is_set() for e in list(events)):
                raise InterruptedError
            if lock.acquire(timeout = self.sleeptime):
                break
        if fun is not None:
            ret = fun(*args)
            lock.release()
            if ret is not None:
                return ret
        return
