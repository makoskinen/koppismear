#!/usr/bin/python
"""Utility functions for chamber system"""

import time
import math
import datetime
import pickle
import ReadIni
import threading
import timerutils
from time import sleep


class NiceScale:
    def __init__(self, minv,maxv):
        self.maxTicks = 6
        self.tickSpacing = 0
        self.lst = 10
        self.niceMin = 0
        self.niceMax = 0
        self.minPoint = minv
        self.maxPoint = maxv
        try:
            self.calculate()
        except ValueError:
            pass

    def calculate(self):
        self.lst = self.niceNum(self.maxPoint - self.minPoint, False)
        self.tickSpacing = self.niceNum(self.lst / (self.maxTicks - 1), True)
        self.niceMin = math.floor(self.minPoint / self.tickSpacing) * self.tickSpacing
        self.niceMax = math.ceil(self.maxPoint / self.tickSpacing) * self.tickSpacing

    def niceNum(self, lst, rround):
        self.lst = lst
        exponent = 0 # exponent of range */
        fraction = 0 # fractional part of range */
        niceFraction = 0 # nice, rounded fraction */

        exponent = math.floor(math.log10(self.lst));
        fraction = self.lst / math.pow(10, exponent);

        if (self.lst):
            if (fraction < 1.5):
                niceFraction = 1
            elif (fraction < 3):
                niceFraction = 2
            elif (fraction < 7):
                niceFraction = 5;
            else:
                niceFraction = 10;
        else :
            if (fraction <= 1):
                niceFraction = 1
            elif (fraction <= 2):
                niceFraction = 2
            elif (fraction <= 5):
                niceFraction = 5
            else:
                niceFraction = 10

        return niceFraction * math.pow(10, exponent)

    def setMinMaxPoints(self, minPoint, maxPoint):
          self.minPoint = minPoint
          self.maxPoint = maxPoint
          self.calculate()

    def setMaxTicks(self, maxTicks):
        self.maxTicks = maxTicks;
        self.calculate()


class ChamberVolume(object):
    """Calculate chamber volume based on gas addition
1. set trace flow to 0
2. record start time
3. set trace flow to preset value
4. ???
5. PROFIT"""
    def __init__(self, cfg, controls, outvars, latest, logger=None):
        self.logger = logger
        self.control = controls[cfg['cname']]['obj']  # MFC object
        self.var = outvars[cfg['trace']]  # Variable = parameter name of
        # trace
        self.latest = latest[cfg['trace']]
        self.T = outvars[cfg['Tvar']]  # name of temperature variable
        self.flowtime = cfg['flowtime']  # period to flow gas for (seconds)
        self.flow = cfg["flow"]  # flow setting of MFC (% max)
        self.vdelay = cfg["vdelay"]  # delay of air flow from chamber to
        # analyzer (seconds)
        self.gconc = cfg["gconc"]  # concentration of trace in gas, ppm
        if "vwa" in cfg:
            # "a" and "b" parameters of trace for the Van der Waals equation
            self.vwa = cfg["vwa"]
            self.vwb = cfg["vwb"]
        else:
            self.vwa = None
            self.vwb = None

    def go(self):
        self.control.setstate(0)  # set flow to 0
        self.control.zerototal()  # Zero the totalizer on the MFC
        self.starttime = time.time()
        self.control.setstate(self.flow)
        while True:
            pass

    def vdwn(self, vol):
        """ Calculate n of trace based on van der Waals equation:
n = """
        pass


class HouseKeeping(object):
    def __init__(self, outvars, latest, dostop, datalock, logger, interval, spare):
        self.outvars = outvars
        self.latest = latest
        self.lastdone = time.time()
        self.dostop = dostop
        self.datalock = datalock
        self.logger = logger.getChild(suffix = "Housekeeping")
        self.interval = interval
        self.spare = spare

    def clean_outvars(self):
        today = time.time()
        if self.lastdone is None or today - self.lastdone >= self.interval * 86400:
            while not self.datalock.acquire(timeout = 0.01):
                if self.dostop.is_set():
                    return
            self.clean_outvars_now()
            self.datalock.release()
            self.lastdone = today

    def run(self):
        while True:
            if self.dostop.is_set():
                return
            sleep(1)
            self.clean_outvars()

    def clean_outvars_now(self):
        self.logger.info('Clearing outvars')
        pickle.dump(self.outvars, open("oldoutvars.dat", "wb"))
        for k in self.outvars.keys():
            # print(k)
            # try:
            #     print(len(self.outvars[k].keys()), min(self.outvars[k].keys()))
            # except ValueError:
            #     print(self.outvars[k].keys())
            #     return
            # print(time.time(), self.latest[k].get()[0])
            try:
                self.outvars[k] = dict([(s, self.outvars[k][s])
                                      for s in self.outvars[k].keys()
                                      if s > (self.latest[k].get()[0] - self.spare)])
            except (ValueError,IndexError):
                continue

class StatusReader(object):
    def __init__(self, connobject, outvars, outvarslock, variables, interval, name, cfgfile, defname, maxtries=20):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger.getChild(suffix = name)
        self.logger.debug("Starting..")
        self.dostop = threading.Event()
        self.outvars = outvars
        self.outvarslock = outvarslock
        self.interval = timerutils.divsum(interval)
        self.maxtries = maxtries
        self.name = name
        self.invariables = variables.split(',')
        self.logger.debug("Invariables: %s" % repr(self.invariables))
        self.cfgfile = cfgfile
        self.cfg = ReadIni.ReadIni(self.cfgfile)
        self.defs = dict()
        self.defs.update({s: self.cfg[s] for s in defname.split(',')})
        self.variables = list()
        for k in self.defs.keys():
            for s in self.defs[k].keys():
                self.variables.append(s)
                with(self.outvarslock):
                    self.outvars[s] = dict()

    def read(self):
        self.starttime = time.time()
        i = 0
        while True:
            if i >= self.maxtries:
                return {s: 'NA' for s in self.variables}
            if i:
                time.sleep(self.interval/100)
            while not self.outvarslock.acquire(timeout = 0.01):
                if self.dostop.is_set():
                    self.logger.debug("Stopping")
                    return
            try:
                val = self.getval()
                break
            except ValueError:
                self.logger.debug("No data available")
                i += 1
                continue
            except IndexError:
                i += 1
                continue
            finally: self.outvarslock.release()
        out = self.parse(val, self.defs)
        return out

    def getval(self):
        self.logger.debug("getval: reading..")
        now = dict([(s, max(self.outvars[s].keys())) for s in self.invariables])
        self.logger.debug("getval: now is %s" % repr(now))
        if any(t < (self.starttime - self.interval) for t in now.values()):
            self.logger.debug("getval: data too old")
            raise IndexError
        vals = {s: self.outvars[s][now[s]] for s in self.invariables}
        self.logger.debug("getval: read %s" % repr(vals))
        return vals

    def parse(self, vals, defs):
        out = dict()
        for s in defs.keys():
            for k in defs[s].keys():
                self.logger.debug("parsing %s from %s (val is %s and def is %s)" % (k, s, vals[s], defs[s][k]))
                
                if int(vals[s]) == 0 and int(defs[s][k]) == 0:
                    out.update({k: 1})
                    continue
                out.update({k: int(bool(int(vals[s]) & defs[s][k]))})
        return out

    def stop(self):
        self.dostop.set()
