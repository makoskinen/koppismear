#!/usr/bin/python3

"""Starter script for koppismear using the faux.ini testing configuration"""

import koppi_alternate as koppi
import time
import threading
import os
import sys
import socket_server

gc = koppi.koppismear('main_cart.ini', forcestart=True)
if gc.cfg['general']['startserver']:
    stopper = threading.Event()
    ss = socket_server.koppiServer(gc=gc, host=gc.cfg['general']['hostaddr'], port=65432, stopper=stopper)
    ss.run()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            stopper.set()
            gc.stop()
            gc.quitnow()
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
