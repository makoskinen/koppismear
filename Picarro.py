#!/usr/bin/python
"""Picarro - interface for the Picarro analyzers if no serial port is
given, ip and port for telnet must be provided in the cfg datastr must
contain the names of values provided by this Picarro instance in the
order they appear in the response
"""
import datetime

from time import sleep


class Picarro:
    def __init__(self, port, ptype='serial', logger=None, name=None, series=2, debug=False, ntries=3):
        self.ptype = ptype
        self.logger = logger.getChild(suffix = "Picarro " + str(name))
#        if port == None:
#            import telnetlib
#            self.port = telnetlib.Telnet(cfg['ip'], port = cfg['port'])
#            print "Hoho"
#            self.endstr = '\r\n'
#            self.ptype = 'telnet'
        self.port = port['obj']
        self.lock = port['lock']
        self.name = name
        self.debug = debug
        self.series = int(series)
        if self.series == 2:
            self.endstr = '\n\r'
            self.replendstr = self.endstr
        elif self.series == 1:
            self.endstr = '\r'
            self.replendstr = '\r\r'
        if self.ptype == 'serial':
            with self.lock:
                self.port.flushInput()
                self.port.flushOutput()
        self.ntries = ntries
        self.Ready = bool()
        self.MeasActive = bool()
        self.ErrorInBuffer = bool()
        self.GasFlowing = bool()
        self.PressureLocked = bool()
        self.CavityTempLocked = bool()
        self.WarmboxTempLocked = bool()
        self.WarmingUp = bool()
        self.SystemErr = bool()

    def stop(self):
        if self.ptype == 'telnet':
            self.port.close()
        try:
            self.lock.release()
        except Exception:
            pass

    def write(self, msg, reply=True, debug=False):
        """Sends a command to the picarro device, optionally expecting a
reply"""
        with self.lock:
            if self.ptype == 'serial':
                self.port.reset_input_buffer()
                self.port.reset_output_buffer()
            msg = msg + self.endstr
            if debug:
                print(msg)
            self.port.write(msg.encode('utf-8'))
            if reply:
                worked = False
                attempts = 1
                while not worked and attempts < 5:
                    sleep(.1)
                    if self.ptype == 'serial':
                        if debug:
                            print("Picarro write reading..")
                        datain = self.port.read_until(self.replendstr.encode('utf-8')).decode()
                        if debug:
                            print("Picarro write read %s ." % datain.strip('\r'))
                        if datain == '':
                            if debug:
                                print("Picarro empty reply!")
                            self.logger.warning('Picarro %s: Empty reply!' % self.name)
                            self.logger.warning('Picarro %s: Trying to close and reopen port' % self.name)
                            self.port.close()
                            sleep(1)
                            self.port.open()
                            attempts = attempts + 1
                            self.port.write(msg.encode('utf-8'))
                        else:
                            worked = True
                    elif self.ptype == 'telnet':
                        try:
                            datain = self.port.read_until(self.replendstr.encode('utf-8'))
                            worked = True
                        except Exception:
                            datain = None
                            worked = True
        if reply:
            return(datain)

    def getstatus(self):
        """Requests a status report from the picarro device and writes the
reply to local variables"""
        msg = self.write('_Instr_Getstatus')
        tries = 1
        while tries < self.ntries:
            try:
                msg = int(msg)
                break
            except ValueError:
                sleep(0.1)
                tries = tries + 1
                msg = self.write('_Instr_Getstatus')
        if not type(msg) == int:
            msg = 0
        self.Ready = bool(msg & 1)
        self.MeasActive = bool(msg & 2)
        self.ErrorInBuffer = bool(msg & 4)
        self.GasFlowing = bool(msg & 64)
        self.PressureLocked = bool(msg & 128)
        self.CavityTempLocked = bool(msg & 256)
        self.WarmboxTempLocked = bool(msg & 512)
        self.WarmingUp = bool(msg & 8192)
        self.SystemErr = bool(msg & 16384)

    def showstatus(self):
        print(('{:<30} : {:<6}'.format('Status', 'Value')))
        for k, v in self.statusdict.items():
            print(('{:<30} : {:<6}'.format(k, v)))


class reader(Picarro):
    """Reader object for the Picarro analyzers"""
    def __init__(self, connobject, interval, variables,
                 includedatetime=False, debug=False, **kwargs):
        self.debug = debug
        self.connobject = connobject['obj']
        self.ntries = self.connobject.ntries
        self.includeDatetime = includedatetime
        self.name = self.connobject.name
        self.series = self.connobject.series
        self.Datetimecol = 'Picarrodatetime-%s' % self.name
        self.interval = interval
        self.variables = variables.split(',')
        if self.Datetimecol not in self.variables and self.includeDatetime:
            self.variables = [self.Datetimecol] + self.variables
        self.logger = self.connobject.logger
        self.ptype = self.connobject.ptype
        self.port = self.connobject.port
        self.lock = self.connobject.lock
        self.endstr = self.connobject.endstr
        self.replendstr = self.connobject.replendstr
        self.write('_Meas_Clearbuffer', reply=False)
        sleep(.1)
        with self.lock:
            dump = self.port.read_until(self.replendstr.encode('utf-8'))
        #        self.outdata = {self.Datetimecol: []}
        self.outdata = {}
        self.getstatus()
        self.statusdict = dict([('Ready', self.Ready),
                                ('MeasActive', self.MeasActive),
                                ('ErrorInBuffer', self.ErrorInBuffer),
                                ('GasFlowing', self.GasFlowing),
                                ('PressureLocked', self.PressureLocked),
                                ('CavityTempLocked', self.CavityTempLocked),
                                ('WarmboxTempLocked', self.WarmboxTempLocked),
                                ('WarmingUp', self.WarmingUp),
                                ('SystemErr', self.SystemErr)])

    def read(self):
        """Uses getval to retrieve data from the picarro device and optionally takes out the timestamp from the observation"""
        self.getstatus()
        if not self.Ready:
            self.logger.debug('not ready (%s)' % self.WarmingUp)
            data = dict()
            for s in self.variables:
                data[s] = None
            return(data)
        data = self.getval()
        if not self.includeDatetime and self.Datetimecol in data:
            data.pop(self.Datetimecol)
        return(data)

    def process(self, data):
        """Processes the data into observations or error messages"""
        data = data.strip('\r').strip('\n')
        if self.debug:
            print("Picarroread: process: %s" % data)
        try:
            data = int(data)
            return(data)
        except ValueError:
            if data.startswith('ERR'):
                self.logger.warning('%s' % data)
                raise PicarroException
            data = data.split(';')
            if self.debug:
                print("Picarroread: process split: %s" % repr(data))
            if self.series == 2:
                data[0] = datetime.datetime.strptime(
                    data[0], '%Y-%m-%d %H:%M:%S.%f').strftime('%Y-%m-%d %T')
            elif self.series == 1:
                try:
                    data[0] = datetime.datetime.strptime(
                        data[0], '%y/%m/%d %H:%M:%S.%f').strftime('%Y-%m-%d %T')
                except ValueError:
                    pass
                #             self.outdata['Datetime'] = data.pop(0)
            if self.debug:
                print("Picarroread: process datetime: %s" % repr(data))
            for s in self.variables:
                try:
                    self.outdata[s] = data.pop(0)
                except Exception:
                    self.logger.debug('exception in data processing')
                    self.outdata[s] = None

    def getval(self):
        """Send command to the picarro device to retrieve latest observation with timestamp"""
        self.outdata = {}
        try:
            datain = self.write('_Meas_GetConcEx')
            if self.debug:
                print("Picarroread: %s." % datain.strip('\r').strip('\n'))
            self.write('_Meas_ClearBuffer', reply=False)
        except PicarroException as p_e:
            return(None)
        try:
            self.process(datain)
        except PicarroException as p_e:
            return(None)
        except AttributeError:
            for s in self.variables:
                self.outdata[s] = None
        return(self.outdata)


class PicarroException(Exception):
    pass


def excepttest():
    raise PicarroException(
        'ERR:3001 Measurement System Disabled\x00x092018-07-05 10:32:58.834')
