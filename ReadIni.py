def ReadIni(inifile=None):  # Copied almost straight from jahfilelib.py
    from configparser import RawConfigParser

    class RCParser(RawConfigParser):
        def __init__(self, defaults=None):
            RawConfigParser.__init__(self, defaults)

            def optionxform(self, optionstr):
                return optionstr
    parser = RCParser()
    parser.read(inifile)
    dct = {}  # This is the main configuration dictionary
    for sect in parser.sections():
        dct[sect] = {}  # Create subdictionaries for each section
        for opt in parser.options(sect):
            dct[sect][opt] = parser.get(sect, opt)
            # remove comments
            dct[sect][opt] = dct[sect][opt].split('#')[0]
            # remove whitespace if there were comments
            dct[sect][opt] = dct[sect][opt].strip()
            # convert ints to ints and floats to floats
            try:
                dct[sect][opt] = int(dct[sect][opt])
            except ValueError:
                try:
                    dct[sect][opt] = float(dct[sect][opt])
                except ValueError:
                    # if not, keep as is
                    pass
    return dct
