#!/usr/bin/python

""" Module for using ADVATECH's ADAM 4000-series relay modules """


import time
import threading
from time import sleep
from itertools import cycle


class adamconn(object):  # Basic connection object for ADAM MODBUS devices
    """Create one connection object per ADAM module with distinct address"""

    def __init__(self, name, port, logger):
        self.port = port['obj']
        self.lock = port['lock']
        self.address = '0' + str(name)
        self.logger = logger.getChild(suffix = "Adam " + self.address)

    def send(self, cmd, reply=False):
        worked = False
        attempts = 0
        with self.lock:
            while not worked and attempts < 5:
                if not cmd.endswith('\r'):
                    cmd = cmd + '\r'
                self.port.flushInput()
                self.port.write(cmd.encode('utf-8'))
                attempts = attempts + 1
                msg = self.getval()
                try:
                    msg = msg.decode()
                    if msg.startswith('?'):
                        self.logger.info('Invalid command!')
                    worked = True
                except AttributeError:
                    self.logger.warning('expected bytes, got %s (attempt %s)'
                                        % (type(msg), attempts))
                    msg = ''
                    self.logger.warning('trying to close and open port')
                    self.port.close()
                    sleep(1)
                    self.port.open()
                    self.port.flushOutput()
                    self.port.flushInput()
            if reply:
                return msg.strip('\r')

    def getval(self):
        msg = self.port.read_until('\r'.encode('utf-8'))
        if not msg:
            return None
        return msg

    def changeaddr(self, newaddr):
        newaddr = str(newaddr)
        cmd = '%%%s%s400600' % (self.address, newaddr)
        rep = self.send(cmd, reply=True)
        if rep.startswith('!' + newaddr):
            self.logger.info('address changed to %s' % newaddr)
            self.address = newaddr
            self.logger = self.logger.parent.getChild(suffix = "Adam " + self.address)
        else:
            self.logger.warning('address unchanged!')

    def setval(self, chan, value):
        chan = repr(int(chan))  # Make both numeric and ascii characters work
        value = repr(int(value))
        if len(chan) == 1:
            chan = '1' + chan
        if len(value) == 1:
            value = '0' + value
        cmd = '#%s%s%s' % (self.address, chan, value)
        rep = self.send(cmd, reply=True)
        if rep == '>':
            self.logger.debug('channel %s set to value %s' %
                              (chan, value))
            return True
        else:
            self.logger.warning('no reply!' % self.address)
            return False

    def speedtest(self):
        t0 = time.time()
        for j in (0, 1):
            for i in range(8):
                self.setval(i, j)
        t1 = time.time()
        print('%s seconds for full round' % repr(t1-t0))

    def scan(self):
        for s in range(10):
            address = '0' + str(s)
            print(address)
            cmd = '#%s0100' % address
            rep = self.send(cmd, reply=True)
            print(rep)


class adamsetup(adamconn):  # Stand-alone version for setting up addresses of adam modules

    def __init__(self, port, name = 0):
        self.port = port
        self.address = '0' + str(name)

    def send(self, cmd, reply=False):
        if not cmd.endswith('\r'):
            cmd = cmd + '\r'
        self.port.flushInput()
        self.port.write(cmd.encode('utf-8'))
        msg = self.getval()
        try:
            msg = msg.decode()
            if msg.startswith('?'):
                print("Invalid command!")
        except AttributeError:
            print('No reply!')
            msg = ''
        if reply:
            return msg.strip('\r')


    def changeaddr(self, newadd):
        newaddr = str(newaddr)
        cmd = '%%%s%s400600' % (self.address, newaddr)
        rep = self.send(cmd, reply=True)
        if rep.startswith('!' + newaddr):
            print('Address changed to %s' % newaddr)
            self.address = newaddr
        else:
            print('Address unchanged!')
        


class a4k(adamconn):
    """Adam object for single channel of Adam 4000 series"""

    def __init__(self, connobject, nick, name, value):
        self.connobject = connobject['obj']
        self.chan = name
        self.nick = nick
        self.logger = self.connobject.logger
        self.lock = self.connobject.lock
        self.address = self.connobject.address
        self.port = self.connobject.port
        self.value = value
        self.setstate(0)
        self.val = 0

    def setstate(self, value):
        if value in self.value:
            if self.setval(self.chan, value):
                self.val = value
        else:
            self.logger.warning('Adam %s: Invalid value!' % self.nick)

    def getstate(self):
        return self.val


class avici16(adamconn):
    """For controlling VICI multiport valve selector over adam. Parameter value is number of channels available (NOTE! Just 1 number, no range or distinct numbers), channel 1 is home. cstep is adam channel for step forward, chome is adam channel for go home."""

    def __init__(self, connobject, nick, cstep, chome, value=16):
        self.connobject = connobject['obj']
        self.step = a4k(connobject, 'step', cstep, [0, 1])
        self.home = a4k(connobject, 'home', chome, [0, 1])
        self.nick = nick
        self.value = value
        self.portiter = None  # Are created in the next step (self.gohome)
        self.gasport = None  # Are created in the next step (self.gohome)
        self.gohome()

    def dostep(self):
        self.step.setstate(1)
        sleep(0.05)
        self.step.setstate(0)
        self.gasport = next(self.portiter)

    def goton(self, n):
        if n > self.value | n == 0:
            raise ValueError('%s out of range!' % n)
        while self.gasport != n:
            self.dostep()
            sleep(0.325)

    def gohome(self):
        self.home.setstate(1)
        sleep(0.05)
        self.home.setstate(0)
        self.portiter = cycle(list(range(self.value + 1))[1:])
        self.gasport = next(self.portiter)

    def getstate(self):
        return self.gasport

    def setstate(self, value):
        if value == 1:
            self.gohome()
        else:
            self.goton(value)


class peltier():
    """React for temperature control with peltier elements (or any on-off
temperature controlling system)
name is name of control to set the cooling element on or off
reference is the reference temperature sensor channel name
monitor is the temperature channel to monitor
diffs are allowed temperature differences
abs are allowed highest and lowest temperatures"""

    def __init__(self, controls, name, nick, outvars, outvarslock,
                 datarunning, reference, monitor, interval, logger,
                 value, upperdiff=False, lowerdiff=False,
                 upperabs=False, lowerabs=False):
        self.interval = int(interval)
        self.outvars = outvars
        self.outvarslock = outvarslock
        self.running = datarunning
        self.dostop = threading.Event()
        self.doing = list()
        self.done = list()
        self.invars = dict()
        self.upperdiff = int(upperdiff) if not isinstance(
            upperdiff, bool) else upperdiff
        self.lowerdiff = int(lowerdiff) if not isinstance(
            lowerdiff, bool) else lowerdiff
        self.upperabs = int(upperabs) if not isinstance(
            upperabs, bool) else upperabs
        self.lowerabs = int(lowerabs) if not isinstance(
            lowerabs, bool) else lowerabs
        self.reference = reference
        self.monitor = monitor
        self.recsource = False
        self.variables = [reference, monitor]
        self.logger = logger.getChild(suffix = "Peltier" + str(nick))
        self.nick = nick
        self.control = controls[name]['obj']
        self.lastoutput = 0
        self.output = 0
        self.status = False
        self.value = value
        self.current_time = time.time()
        self.last_time = self.current_time

    def update(self, reference, monitor):
        """If current temperature in monitor channel is higher than current
temperature in reference channel plus upperdiff OR current temperature
in monitor channel is higher than upperabs (is upperabs is not False),
turn on cooling. If current temperature in monitor channel is lower
than current temperature in refenrece channel minus lowerdiff OR
current temperature in monitor channel is lower than lowerabs (if
lowerabs is not False), turn off cooling"""
        if not self.running.is_set():
            return
        if self.upperdiff:
            if monitor > (reference + self.upperdiff):
                if self.lowerabs:
                    if monitor < self.lowerabs:
                        if self.output == 1:
                            self.control.setstate(0)
                            self.output = 0
                        return
                if self.output == 0:
                    self.control.setstate(1)
                    self.output = 1
                return
        if self.lowerdiff:
            if monitor < (reference - self.lowerdiff):
                if self.lowerabs:
                    if monitor < self.lowerabs:
                        if self.output == 0:
                            self.control.setstate(1)
                            self.output = 1
                        return
                if self.output == 1:
                    self.control.setstate(0)
                    self.output = 0
                return
        if self.upperabs:
            if monitor > self.upperabs:
                if self.output == 0:
                    self.control.setstate(1)
                    self.output = 1

    def setstate(self, value):
        """Turn temperature control on or off"""
        if value == 0:
            self.output = self.control.getstate()
            self.status = False
        if value == 1:
            self.output = self.control.getstate()
            self.status = True

    def getstate(self):
        return int(self.status)

    def stop(self):
        pass

    def write(self, value):
        """Update the control setting with current temperature values"""
        self.lastoutput = self.output
        try:
            self.update(float(value[self.reference]),
                        float(value[self.monitor]))
        except KeyError:
            self.logger.debug('%s: No data?' % self.nick)
            return
        self.last_time = time.time()
        if self.output != self.lastoutput:
            self.logger.info('%s: peltier turned %s' %
                             (self.nick, ['off', 'on'][self.output]))

    def run(self):
        """Read the outvars 5 times per interval, check for new data that's become available; if some data is missing, create an entry in list "doing". Insert available data into rows in list "doing"; when a row is full or is older than max wait, update the control setting if all variables are available, discard if not, add timestamp into list "done". This function runs continuously as a thread that's started in __init__."""
        while True:
            while not self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('stopping')
                    return
                time.sleep(0.5)
            previouswrite = int(time.time())
            while self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('stopping')
                    return
                while self.outvarslock.locked():
                    time.sleep(0.01)
                self.outvarslock.acquire()
                self.logger.debug('acquired outvarslock 1')
                try:
                    latest = {s: max(list(self.outvars[s].keys()))
                              for s in self.variables}
                    latest_max = max(list(latest.values()))
                except (ValueError, KeyError):
                    self.outvarslock.release()
                    self.logger.debug(
                        'released outvarslock 1 after exception')
                    time.sleep(self.interval/5)
                    continue
                self.outvarslock.release()
                self.logger.debug(
                    'released outvarslock 1;latest_max %s, previouswrite %s' % (latest_max, previouswrite))
                while (latest_max - previouswrite) < self.interval:
                    self.logger.debug('latest_max %s, previouswrite %s' % (
                        latest_max, previouswrite))
                    if not self.running.is_set():
                        break
                    if len(self.doing) > 0:
                        self.logger.debug(
                            'checking doing list')
                        for t in self.doing:
                            self.logger.debug(
                                'trying %s in doing' % t)
                            for v in self.variables:
                                if not v in list(self.invars[t].keys()):
                                    if t in list(self.outvars[v].keys()):
                                        self.invars[t][v] = self.outvars[v][t]
                            if (all(v in list(self.invars[t].keys()) for v in self.variables) or (time.time() - t) > self.maxwait) and t == min(self.doing):
                                self.invars[t].update(
                                    {v: 'NA' for v in self.variables if not v in list(self.invars[t].keys())})
                                self.logger.debug(
                                    'writing at %s' % t)
                                if 'NA' in self.invars[t]:
                                    self.doing.remove(t)
                                    self.done = self.done + [t]
                                    continue
                                self.write(self.invars[t])
                                self.doing.remove(t)
                                self.done = self.done + [t]
                        time.sleep(self.interval/5)
                    while self.outvarslock.locked():
                        time.sleep(0.01)
                    self.outvarslock.acquire()
                    self.logger.debug(
                        'acquired outvarslock 2')
                    latest = {s: max(list(self.outvars[s].keys()))
                              for s in self.variables}
                    self.outvarslock.release()
                    self.logger.debug(
                        'released outvarslock 2')
                    latest_max = max(list(latest.values()))
                    time.sleep(self.interval/5)
                if not latest_max in self.doing + self.done:
                    previouswrite = latest_max
                    self.logger.debug(
                        'getting new data at %s' % latest_max)
                    self.invars[latest_max] = dict()
                    ##self.invars[latest_max], latest_max)
                    if (not all(a == latest_max for a in list(latest.values()))) or len(self.doing) > 0:
                        self.doing = self.doing + [latest_max]
                    else:
                        while self.outvarslock.locked():
                            time.sleep(0.01)
                        self.outvarslock.acquire()
                        self.logger.debug(
                            'acquired outvarslock 3')
                        self.invars[latest_max].update(
                            {v: self.outvars[v][latest_max] for v in self.variables})
                        self.outvarslock.release()
                        self.logger.debug(
                            'released outvarslock 3')
                        self.write(self.invars[latest_max])
                        self.logger.debug('wrote row %s' % (
                            repr(self.invars[latest_max])))
                        self.done = self.done + [latest_max]
                time.sleep(self.interval/5)
