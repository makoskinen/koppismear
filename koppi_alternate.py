#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""koppi.py: main runner of the background processes, taking commands etc
main.ini:
[ports]
port = module.class
[port]
option = value

[connObjects]
connobject = module.class
[connobject]
port = port # from [ports]
option = value

[dataSources]
dataSource = module.class
[dataSource]
connobject = connobject # from [connObjects]
variables = var1,var2,var3
interval = 1 #seconds
option = value

[dataSinks]
dataSink = module.class
[dataSink]
connobject = connobject # from [connObjects]
variables = var1,var2,var3
interval = 1 #seconds
option = value

[controls]
control = module.class
[control]
connobject = connobject # from [connObjects]
name = str #channel name in module.class
value = 0,1 #possible values (csv or range 0-n)

"""

import multiprocessing
import threading
import time
import os
import sys
sys.path.append('./requirements')
import subprocess
import logging
import logging.config
import threadedDatamanager_alternate as threadedDatamanager
import controlmanager
import controltimer
import timerutils
import pickle
import utils

from time import sleep
from ReadIni import ReadIni


class koppismear():

    class solidlist(object):
        def __init__(self, maxn):
            self.maxn = maxn
            self.lst = list()

        def get(self):
            return(self.lst)

        def put(self, val):
            self.lst.insert(0, val)
            if len(self.lst) > self.maxn:
                self.lst = self.lst[0:-1]

    def __init__(self, inifile, forcestart=False, prestart=None, postquit=None):
        self.prestart = prestart
        self.postquit = postquit
        if self.prestart is not None:
            psret=subprocess.run(self.prestart, check=True)
        self.inifile = inifile
        self.forcestart = forcestart
        self.controlrunning = multiprocessing.Event()
        self.datarunning = multiprocessing.Event()
        self.outvars = dict()
        self.initialize()

    def initlogging(self):
        logging.config.fileConfig(self.dolog)
        self.logger = logging.getLogger(self.logname)

    def initialize(self):
        self.cfg = ReadIni(self.inifile)
        self.pid = str(os.getpid())
        if "pidfile" in self.cfg["general"]:
            self.pidfile = self.cfg["general"]["pidfile"]
        else:
            self.pidfile = "/tmp/koppismear_pid"
        if os.path.isfile(self.pidfile) and not self.forcestart:
            print("Pidfile %s already exists! Another instance of koppismear\
 might be running. Check if this is the case and remove the pidfile if not." % self.pidfile)
            return
        with open(self.pidfile, 'w') as pf:
            pf.write(self.pid)
        # Set up cleaning of outvars
        if "janitordays" in self.cfg["general"]:
            self.janitordays = self.cfg["general"]["janitordays"]
        else:
            self.janitordays = 0
        if "janitorspare" in self.cfg["general"]: # How many
                                                  # observations to
                                                  # keep per variable
                                                  # in outvars
            self.janitorspare = self.cfg["general"]["janitorspare"]
        else:
            self.janitorspare = 300
        if "timeprec" in self.cfg["general"]:
            self.timeprec = self.cfg["general"]["timeprec"]
        self.sourcecfg = ReadIni(self.cfg["files"]["sourcecfg"])
        self.currsourcelock = threading.Lock()
        # Old sources.ini
        self.timercfg = ReadIni(self.cfg["files"]["timercfg"])
        # ini file for timer
        if "datadir" in self.cfg["general"]:
            self.datadir = self.cfg["general"]["datadir"]
            try:
                os.makedirs(self.datadir)
            except OSError:
                if not os.path.isdir(self.datadir):
                    raise
        else:
            self.datadir = '.'
        self.tstampcol = self.cfg["general"]["tstampcol"]
        try:
            self.dolog = self.cfg["general"]["logging"]
            self.logname = self.cfg["general"]["logname"]
        except KeyError:
            self.dolog = False
        self.messenger = None  # CHANGE TO REAL MESSENGER!
        self.datalock = threading.Lock()
        # To prevent race condition between data sources and sinks
        # For recording the data from data sources
        self.latest = dict()
        self.latestlock = threading.Lock()
        # For keeping books of when the last data was recorded
        # for each variable
        self.maxn = dict()  # How many observations in latest per variable
        if self.dolog:
            self.initlogging()
        self.currSource = ['starting']
        self.currSourcelock = threading.Lock()
        self.ports = self.createport(self.cfg["ports"], createlock=True)
        self.connObjects = self.createobj(self.cfg["connObjects"])
        self.dataSources = self.createsrc(self.cfg["dataSources"])
        self.controls = self.createsrc(self.cfg["controls"])
        self.createoutvars() # create the outvars dictionary for
                             # collected variables
        self.dataSinks = self.createsink(self.cfg["dataSinks"])
        try:
            # For PID controls etc that need feedback
            # from data and use the available controls
            self.reacts = self.createreact(self.cfg["reacts"], self.controls)
            self.controls.update(self.reacts)
            # reacts use control.setstate to set the desired
            # value; the setpoint is set with react.setstate
            self.dataSinks.update(self.reacts)
            # feedback to reacts is given via the sink.write
        except KeyError:
            self.reacts = None
        # Controlmanager sets the controls to values needed for
        # different sources based on the sources cfg
        self.controlmanager = controlmanager.manager(cfg=self.sourcecfg,
                                                     controls=self.controls,
                                                     logger=self.logger)
        # controltimer sends commands to controlmanager based on timercfg
        self.controltimer = controltimer.timer(cfg=self.timercfg,
                                               running=self.controlrunning,
                                               controlmanager=self.controlmanager,
                                               messenger=self.messenger,
                                               currSource=self.currSource,
                                               currSourcelock=self.currSourcelock,
                                               logger=self.logger)
        # datamanager creates reader and writer objects for data
        # sources and sinks
        self.datamanager = threadedDatamanager.manager(sinks=self.dataSinks,
                                                       datasources=self.dataSources,
                                                       outvars=self.outvars,
                                                       latest=self.latest,
                                                       lock=self.datalock,
                                                       latestlock=self.latestlock,
                                                       currSource=self.currSource,
                                                       currSourcelock=self.currSourcelock,
                                                       tstampcol=self.tstampcol,
                                                       logger=self.logger,
                                                       timeprec = self.timeprec)
        # datatimer controls the reader and writer objects of
        # datamanager, keeping books of the latest operations
        self.datatimer = threadedDatamanager.timer(datamanager=self.datamanager,
                                                   running=self.datarunning,
                                                   logger=self.logger)
        self.dthread = threading.Thread(target=self.datatimer.run)
        if self.janitordays > 0:
            self.janitorstopper = threading.Event()
            self.hk = utils.HouseKeeping(outvars=self.outvars,
                                         latest=self.latest,
                                         dostop=self.janitorstopper,
                                         datalock=self.datalock,
                                         logger=self.logger,
                                         interval=self.janitordays,
                                         spare=self.janitorspare)
            self.janitor = threading.Thread(target=self.hk.run)

    def createoutvars(self):
        """Creates a dict object with elements for all variables defined in
the data sources"""
        if len(self.outvars):
            while self.datalock.locked():
                sleep(0.01)
            self.datalock.acquire()
            pickle.dump(self.outvars, open("oldoutvars.dat", "wb"))
            self.datalock.release()
        self.outvars.clear()
        for so in list(self.dataSources.keys()):
            for v in self.dataSources[so]['obj'].variables:
                self.outvars[v] = dict()
                try:
                    self.latest[v] = self.solidlist(self.maxn[v])
                    self.latest[v + ".nextread"] = self.solidlist(1)
                except KeyError:
                    self.latest[v] = self.solidlist(1)
                    self.latest[v + ".nextread"] = self.solidlist(1)

    def go(self):
        """Start the measurements and control timers"""
        self.logger.info('Starting run %s', self.controltimer.name)
        self.controltimer.prime()
        self.datarunning.set()
        self.controltimer.start()
        try:
            self.dthread.start()
        except RuntimeError: ## If data thread is already running
            pass
        if self.janitordays > 0:
            try:
                self.janitorstopper.clear()
                self.janitor.start() ## If janitor thread is already running
            except RuntimeError:
                pass

    def datago(self):
        """Start only recording data from sources"""
        self.logger.info('Recording data')
        self.datarunning.set()
        if not self.dthread.is_alive():
            self.dthread.start()
        if self.janitordays > 0:
            if not self.janitor.is_alive():
                self.janitor.start()

    def pause(self):
        """Pause everything, including data recording"""
        self.logger.info('Pausing everything')
        self.controltimer.pause()
        self.datarunning.clear()

    def pauserun(self):
        """Pause the source changes - need "cont" to continue"""
        self.logger.info('Pausing measurements')
        self.controltimer.pause()

    def cont(self):
        """Continue measurements after pause"""
        self.logger.info('Continuing measurements')
        self.datarunning.set()
        self.controltimer.cont()

    def stop(self):
        """Stop measurements - need "go" to start again"""
        self.logger.info('Stopping measurements')
        print("Remember to use quitnow() if you want to stop measuring for good")
        self.controltimer.stop()
        self.datarunning.clear()
        self.datatimer.stop()
        try:
            self.dthread.join()
            self.dthread = threading.Thread(target=self.datatimer.run)
        except RuntimeError:  # If not started yet
            pass
        if self.janitordays > 0:
            try:
                self.janitorstopper.set()
                self.janitor.join()
            except RuntimeError:
                pass
            self.janitor = threading.Thread(target=self.janitor.run)

    def quitnow(self):
        if self.dthread.is_alive():
            self.stop()
        try:
            subprocess.run(self.postquit, check=True)
        except TypeError:
            pass
        finally:
            os.unlink(self.pidfile)

    def restart(self):
        """Restart measurements (stop-prime-start)"""
        self.logger.info('Restarting measurements')
        self.controltimer.restart()

    def reinit(self, inifile=None):
        """Reinitialize - read old inifile again or read new inifile"""
        if inifile:
            self.inifile = inifile
        self.logger.info('Reinitialising with inifile %s', self.inifile)
        self.quitnow()
        self.initialize()

    def setsource(self, nextsource):
        """Manually set the source, record change if cfg tells to do so"""
        (source, delay, recchange) = self.controlmanager.setSource(nextsource)
        if recchange:
            with self.currSourcelock:
                self.currSource[0] = source

    def help(self):
        pass

    def readcmd(self):
        pass

    def createport(self, indict, createlock=False):
        """Create dictionary of port objects"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            if 'timeout' in self.cfg[p]:
                self.cfg[p]['timeout'] = int(self.cfg[p]['timeout'])
            mod = __import__(pcfg[0])
            print(pcfg[0])
            if createlock:
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                    (**self.cfg[p])),
                                   ('lock', threading.Lock()),('errcount', 0)])
            else:
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                    (**self.cfg[p])), ('errcount', 0)])
        return outdict

    def createobj(self, indict):
        """Create dictionary of connection objects - mediators between ports
and data inputs and outputs"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            pl = self.ports[self.cfg[p]['port']]
            # includes the port itself and the lock as dict entries
            if len(self.cfg[p]) > 1:
                objcfg = self.cfg[p]
                objcfg.pop('port')
            else:
                objcfg = dict()
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (port=pl, logger=self.logger, **objcfg))])
        return outdict

    def createsrc(self, indict):
        """Create dictionary of data sources or control objects"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            co = self.connObjects[self.cfg[p]['connobject']]
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
                srccfg.pop('connobject')
            else:
                srccfg = dict()
            if 'value' in srccfg:
                if str(srccfg['value']).count('-') == 1:
                    srccfg['value'] = tuple([float(a)
                                             for a in
                                             srccfg['value'].split('-')])
                elif str(srccfg['value']).count(',') > 0:
                    srccfg['value'] = [int(a)
                                       for a in srccfg['value'].split(',')]
                else:
                    try:
                        srccfg['value'] = int(srccfg['value'])
                    except ValueError:
                        pass  # Leave as is
            if 'needsoutvars' in srccfg:
                srccfg.pop('needsoutvars')
                mod = __import__(pcfg[0])
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                    (connobject = co, outvars = self.outvars, outvarslock = self.datalock, **srccfg))])
            else:
                mod = __import__(pcfg[0])
                outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (connobject=co, **srccfg))])
        return outdict

    def createsink(self, indict):
        """Create dictionary of data sinks (they differ from sources and
controls in that they need the time stamp column as parameter)"""
        outdict = dict()
        for p in sorted(indict.keys()):
            print(p)
            pcfg = indict[p].split('.')
            co = self.connObjects[self.cfg[p]['connobject']]
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
                srccfg.pop('connobject')
                srccfg["datadir"] = self.datadir
                try:
                    recsource = self.to_bool(srccfg.pop('recsource'))
                except KeyError:
                    recsource = False
            else:
                srccfg = dict()
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (connobject=co,
                                 tstampcol=self.tstampcol,
                                 recsource=recsource,
                                 outvars=self.outvars,
                                 outvarslock=self.datalock,
                                 latest=self.latest,
                                 latestlock=self.latestlock,
                                 currsource=self.currSource,
                                 currsourcelock=self.currSourcelock,
                                 datarunning=self.datarunning,
                                 timeprec=self.timeprec,
                                 **srccfg))])
            try:
                n = outdict[p]['obj'].average
            except AttributeError:
                print("No average")
                n = 1
            for v in outdict[p]['obj'].variables:
                try:
                    if self.maxn[v] < n:
                        self.maxn[v] = n
                except KeyError:
                    self.maxn[v] = n
        return outdict

    def to_bool(self, value):
        """
        Converts 'something' to boolean. Raises exception for invalid formats
        Possible True  values: 1, True, "1", "TRue", "yes", "y", "t"
        Possible False values: 0, False, None, [], {}, "", "0", "faLse", "no", "n", "f", 0.0,
        Taken from user Petrucio at Stackoverflow
        """
        if str(value).lower() in ("yes", "y", "true",  "t", "1"):
            return True
        if str(value).lower() in ("no",  "n", "false", "f", "0", "0.0",
                                  "", "none", "[]", "{}"):
            return False
        raise Exception('Invalid value for boolean conversion: ' + str(value))

    def createreact(self, indict, controls):
        """Create dictionary of react objects that take input from a data
source and give output to a control"""
        outdict = dict()
        for p in sorted(indict.keys()):
            pcfg = indict[p].split('.')
            if len(self.cfg[p]) > 1:
                srccfg = self.cfg[p]
            else:
                srccfg = dict()
            if 'value' in srccfg:
                if srccfg['value'].count('-') == 1:
                    srccfg['value'] = tuple([float(a)
                                             for a in
                                             srccfg['value'].split('-')])
                elif srccfg['value'].count(',') > 0:
                    srccfg['value'] = [int(a)
                                       for a in srccfg['value'].split(',')]
            mod = __import__(pcfg[0])
            outdict[p] = dict([('obj', getattr(mod, pcfg[1])
                                (controls=controls,
                                 logger=self.logger,
                                 outvars=self.outvars,
                                 outvarslock=self.datalock,
                                 datarunning=self.datarunning,
                                 **srccfg))])
        return outdict

    def createlogs(self, indict):
        """Create the logger objects (not used at the time?)"""
        self.log = logging.getLogger(__name__)
        for p in sorted(indict.keys()):
            pcfg = indict[p].split('.')
            srccfg = self.cfg[p]
            try:
                level = srccfg.pop('level')
            except KeyError:
                level = None
            mod = __import__(pcfg[0])
            handler = getattr(mod, pcfg[1])(**srccfg)
            if level is not None:
                handler.setLevel(getattr(mod, level))
            self.log.addHandler(handler)
