#!/usr/bin/python
"""LGR - interface for the LGR analysers, receives data over serial
port as it comes
"""
import datetime
import threading

import time

import pandas as pd


class LGR(object):
    def __init__(self, port, logger=None, name=None):
        self.logger = logger.getChild(suffix="LGR " + str(name))
        self.port = port['obj']
        self.plock = port['lock']
        self.name = name


class reader(LGR):
    def __init__(self, connobject, logger=None, dtype="latest",
                 debug=False, includedatetime=True,
                 keyfile="LGRkeys.csv", skipcols=["C30", "C31", "C32"],
                 timestampcol="LGRTimestamp", maxobs=20, sep = ",", **kwargs):
        self.connobject = connobject['obj']
        self.logger = logger
        self.port = self.connobject.port
        self.plock = self.connobject.plock
        self.vlock = threading.Lock()
        self.name = self.connobject.name
        self.sep = sep
        self.values = dict()
        self.maxobs = maxobs
        if "nmean" in kwargs:
            self.nmean = kwargs["nmean"]
        else:
            self.nmean = 5
        if "tmean" in kwargs:
            self.tmean = kwargs["tmean"]
        else:
            self.tmean = 5
        if "interval" in kwargs:
            self.interval = kwargs["interval"]
        else:
            self.interval = self.nmean
        self.dostop = threading.Event()
        self.rthread = threading.Thread(target=self.reader)
        self.debug = debug
        self.getkeys(keyfile)
        self.timestampcol = timestampcol
        self.skipcols = skipcols
        if "variables" in kwargs:
            self.variables = kwargs["variables"].split(",")
        else:
            self.variables = [a for a in self.keys if a not in self.skipcols]
        if not includedatetime:
            self.skipcols.append(self.timestampcol)
        else:
            if not self.timestampcol in self.variables:
                self.variables.append(self.timestampcol)
        if dtype == "latest":
            self.getval = self.getlatest
        if dtype == "nmean":
            self.getval = self.getmeanobs
        self.read = self.getval
        self.rthread.start()

    def getkeys(self, filename):
        with open(filename, "r") as f:
            self.keys = f.readline().strip().split(self.sep)

    def tryconv(self, val):
        """Converts val to int if possible, or to float if possible, or
returns it as it is"""
        try:
            return int(val)
        except ValueError:
            pass
        try:
            return float(val)
        except ValueError:
            return(val)

    def reader(self):
        """LGR reader is a continuing process watching the serial port and
receiving and handling data when necessary"""
        self.port.reset_input_buffer()
        discard = self.port.read_until('\n'.encode('utf-8'))
        if self.debug:
            print((repr(discard)))
        while True:
            if self.dostop.is_set():
                break
            timestamp = int(time.time())
            with self.plock:
                datain = self.port.read_until('\n'.encode('utf-8'))
                datain = datain.decode()
            if self.debug:
                print((repr(datain)))
            datain = datain.strip().split(self.sep)
            datain = [self.tryconv(a.strip(' ')) for a in datain]
            self.addobs(timestamp, datain)

    def addobs(self, timestamp, data):
        """Add an observation to the dictionary of timestamped observations"""
        while self.vlock.locked():
            time.sleep(0.01)
            if self.dostop.is_set():
                return
        with self.vlock:
            if len(self.values) >= self.maxobs:
                if self.debug:
                    print("too many observations, removing oldest")
                keys = list(self.values.keys())
                keys.sort()
                self.values.pop(keys[0])
            outvals = dict(list(zip(self.keys, data)))
            for s in self.skipcols:
                try:
                    outvals.pop(s)
                except KeyError:
                    pass
            if self.debug:
                print((repr(outvals)))
            self.values.update({timestamp: outvals})

    def getlatest(self):
        """Return the latest observation"""
        while self.vlock.locked():
            time.sleep(0.01)
            if self.dostop.is_set():
                return
        with self.vlock:
            keys = list(self.values.keys())
            keys.sort()
            n = len(keys)
            outval = self.values[keys[n-1]]
        return outval

    def getmeanobs(self):
        while self.vlock.locked():
            time.sleep(0.01)
            if self.dostop.is_set():
                return
        with self.vlock:
            keys = list(self.values.keys())
            keys.sort()
            keys = keys[-self.nmean:]
            n = len(keys)
            outval = {a: self.values[a] for a in keys}
        timestamp = outval[keys[int(n/2)]][self.timestampcol]
        outval = pd.DataFrame(outval).transpose().mean().round(6)
        outval = outval.to_dict()
        timestamp = datetime.datetime.strptime(
            timestamp, '%m/%d/%Y %H:%M:%S.%f').strftime('%Y-%m-%d %T')
        outval.update({self.timestampcol: timestamp})
        return outval

    def stop(self):
        self.dostop.set()
        self.rthread.join()
