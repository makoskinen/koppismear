# README #

General purpose controlling software for data acquisition and controlling of different outputs (1/0, range, PID, etc)

Currently capable of recording into comma-delimited text files
- with datetime stamp in local time

Module koppi in koppi_alternate.py is the main program that controls the rest. See faux.ini for an example of a faux dummy measurement setup. Current interface is the Python command line.

requirements.txt includes the required external pacakges.

Example session:

import koppi_alternate as koppi

gc = koppi.koppismear('faux.ini') # Initiates the measurement and source controlling system

gc.go() # Starts the measurements

gc.stop() # stops the measurements

gc.quitnow() # cleans the pidfile

KaappiSMEARin ohjaussofta