
import operator
import minimalmodbus
from time import time, sleep
from functools import reduce

class Nokevalconnmodbus(object):
    def __init__(self, name, port, logger):
        self.port = port['obj']
        self.lock = port['lock']
        self.logger = logger.getChild(suffix = "Nokevalmodbus" + str(name))
        self.address = name
        self.instrument = minimalmodbus.Instrument(self.port.port, int(self.address))
        self.instrument.serial.baudrate = self.port.baudrate
        self.proto = 'Modbus'


class Nokevalconn(object):
    def __init__(self, name, port, logger):
        self.port = port['obj']
        self.lock = port['lock']
        self.logger = logger.getChild("NokevalSCL" + str(name))
        self.rdelay = 0.05
        self.address = name + 128
        self.proto = 'SCL'

    def bcc(self, cmd):
        c = [int(i, 16) for i in
             [cmd.encode('utf-8').hex()[j:j + 2] for
              j in range(0, len(cmd.encode('utf-8').hex()), 2)]]
        c.append(int('0x03', 16))
        csum = hex(reduce(operator.xor, c, 0)).split('x')[1]
        return csum

    def send(self, cmd):
        csum = self.bcc(cmd)
        cmdout = '%s%s\x03%s' % (chr(self.address), cmd, chr(int(csum, 16)))
        with self.lock:
            self.logger.debug('send: sending %s' % cmdout)
            self.port.write(cmdout.encode('utf-8'))
            sleep(self.rdelay)
            ret = self.port.read_all()
            # print(list(ret.decode()))
            # ret = ret.decode()
            self.logger.debug('send: read %s' % ret)
        if ret == '':
            self.logger.debug('send: No reply!')
            return None
        self.logger.debug('send: returning %s' % ret[1:len(ret)-2])
        ret = ret[1:len(ret)-2]
        return ret

    def read(self):
        outvars = self.getval()
        return outvars

    def stop(self):
        pass


class Nokevalchan(Nokevalconn):
    def __init__(self, connobject, channel, variables, interval, calibration=None, digits=None):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger.getChild(suffix = str(channel))
        self.lock = self.connobject.lock
        self.port = self.connobject.port
        self.address = self.connobject.address
        self.rdelay = self.connobject.rdelay
        self.interval = interval
        self.channel = channel
        if self.connobject.proto == 'Modbus':
            self.channel = 2 * (int(self.channel) - 1)
            self.getval = self.getvalModbus
        if self.connobject.proto == 'SCL':
            self.getval = self.getvalSCL
        self.variables = [variables]
        self.calibration = calibration
        self.digits = digits

    def getvalSCL(self):
        cmd = 'MEA CH %s' % self.channel
        val = self.send(cmd)
        self.logger.debug(
            'getval: got %s from self.send' % val)
        try:
            val = float(val)
        except ValueError:
            self.logger.debug(
                'getval: ValueError when converting %s' % val)
            val = None
        except TypeError:
            self.logger.debug(
                'getval: TypeError when converting %s' % val)
            val = None
        try:
            val = self.process(val)
        except TypeError:
            val = None
        return {self.variables[0]: val}

    def getvalModbus(self):
        val = self.connobject.instrument.read_float(self.channel, functioncode=4, number_of_registers=2, byteorder=3)
        try:
            val = self.process(val)
        except TypeError:
            val = None
        return {self.variables[0]: val}

    def process(self, val):
        if self.calibration is not None:
            val = val/self.calibration
        if self.digits is not None:
            val = round(val, self.digits)
        return val


class Nokevalrange(Nokevalconn):
    def __init__(self, connobject, channels, variables, interval, calibration=None, digits=None):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger.getChild(suffix = channels)
        self.lock = self.connobject.lock
        self.port = self.connobject.port
        self.address = self.connobject.address
        self.interval = interval
        chanlist = channels.split('-')
        self.channels = list(range(int(chanlist[0]), int(chanlist[1])+1))
        self.chanends = chanlist
        if self.connobject.proto == 'Modbus':
            self.channels = [2 * (i - 1) for i in self.channels]
            self.getval = self.getvalModbus
        if self.connobject.proto == 'SCL':
            self.getval = self.getvalSCL
        self.rdelay = 0.05 + (0.02 * len(self.channels))
        self.variables = [a.strip() for a in variables.split(',')]
        if calibration is not None:
            self.calibration = dict((a.split(':')[0].strip(), float(a.split(':')[1].strip())) for a in calibration.split(','))
        else:
            self.calibration=dict()
        for s in self.variables:
            if not s in self.calibration:
                self.calibration[s] = 1
        self.digits = digits
        if len(self.variables) != len(self.channels):
            self.logger.critical(
                "Number of channels and variables don't match!")
            raise AttributeError

    def getvalSCL(self):
        cmd = 'MEA SCAN %s %s' % (self.chanends[0], self.chanends[1])
        try:
            val = self.send(cmd)
            val = val.split(b' ')
            for i in range(len(val)):
                try:
                    val[i] = float(val[i])
                except ValueError:
                    val[i] = None
        except AttributeError:
            val = []
            for i in range(len(self.variables)):
                val.append(None)
        outdict = dict(list(zip(self.variables, val)))
        for k in outdict:
            outdict[k] = outdict[k]/self.calibration[k]
            if self.digits is not None:
                outdict[k] = round(outdict[k], self.digits)
        return outdict

    def readModbus(self, channel):
        val = self.connobject.instrument.read_float(channel, functioncode=4, number_of_registers=2, byteorder=3)
        return val

    def getvalModbus(self):
        val = [self.readModbus(i) for i in self.channels]
        outdict = dict(list(zip(self.variables, val)))
        for k in outdict:
            outdict[k] = outdict[k]/self.calibration[k]
            if self.digits is not None:
                outdict[k] = round(outdict[k], self.digits)
        return outdict

class Nokevalext(Nokevalconn):
    def __init__(self, connobject, value, name=None):
        self.connobject = connobject
        self.logger = self.connobject.logger.getChild(suffix = f"ext {name}")
        self.name = name
        self.values = value
        self.state = None
        self.setstate(0)

    def setstate(self, value):
        """Set the EXT channel value on Nokeval RMD680"""
        if value in self.values:
            cmd = f'OUT CH 1 {value}'
            self.connobject.send(cmd)
            self.state = value
        else:
            self.logger.warning(f"Invalid value {value}!")
