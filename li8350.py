#!/usr/bin/python

"""Module for reading and controlling Li-830 and Li-850 devices over RS232"""

import re
import xmltodict
import time
import datetime
import threading
from time import sleep
from lxml import etree
from itertools import compress


class liconnect(object):
    def __init__(self, port, logger, name, model, usepump=True):
        """Connect to a li-830 or 850 analyser."""
        self.port = port['obj']
        self.lock = port['lock']
        self.name = name
        self.logger = logger.getChild(suffix = "LI " + str(model) + " " + str(name))
        self.roottag = 'li%s' % re.sub(r'[a-zA-Z-]', "", str(model))
        self.model = re.sub('-', '', str(model))
        self.endstr = '\r'
        self.dostop = threading.Event()
        self.datainit()
        if usepump:
            pumpmsg = "true"
        else:
            pumpmsg = "false"
        msg = self.preparemsg("pump", "enabled", pumpmsg)
        self.write(msg, reply=False)
        self.set_outrate = 0
        self.outrate(0)

    def datainit(self):
        self.status = dict()
        self.cal = dict()
        self.cfg = dict()
        self.output = dict()
        self.pump = dict()
        # self.co2lastzero = None
        # self.co2kzero = None
        # self.co2lastspan = None
        # self.co2lastspan2 = None
        # self.co2kspan = None
        # self.co2kspan2 = None
        # self.h2olastzero = None
        # self.h2okzero = None
        # self.h2olastspan = None
        # self.h2olastspan2 = None
        # self.h2okspan = None
        # self.h2okspan2 = None
        # self.pump_enabled = None
        # self.pump_time = None
        # self.pump_drive = None
        # self.pump_status = None
        # self.rs232_co2 = None
        # self.rs232_h2o = None
        # self.rs232_celltemp = None
        # self.rs232_cellpres = None
        # self.rs232_ivolt = None
        # self.rs232_co2abs = None
        # self.rs232_h2oabs = None
        # self.rs232_h2odewpoint = None
        # self.rs232_raw = None
        # self.rs232_flowrate = None
        # self.rs232_echo = None
        # self.rs232_strip = None

    def write(self, msg, reply=True, delay=False):
        """Write to device and get possibly delayed reply"""
        with self.lock:
            self.port.flushInput()
            self.port.flushOutput()
            self.port.write(msg + self.endstr.encode('utf-8'))
            if reply:
                sleep(.01)
                if delay:
                    while self.port.inWaiting() == 0:
                        if self.dostop.is_set():
                            return
                        sleep(0.1)
                datain = self.port.read_until()
                return(datain)
            else:
                self.port.flushInput()

    def getstatus(self, parent=None, child=None):
        """Get the current status of pump, calibration and config (parent =
pump, cal or cfg)"""
        msg = self.preparemsg(parent=parent, child=child, text='?')
        if self.set_outrate > 0:
            self.outrate(0)
        rep = self.write(msg)
        if parent is None:
            self.pump = self.processrep(msg, rep, parent='pump')
            self.cfg = self.processrep(msg, rep, parent='cfg')
            self.cal = self.processrep(msg, rep, parent='cal')
            return
        if 'pump' in parent:
            self.pump = self.processrep(msg, rep, parent='pump')
        if 'cfg' in parent:
            self.cfg = self.processrep(msg, rep, parent='cfg')
        if 'cal' in parent:
            self.cal = self.processrep(msg, rep, parent='cal')
        if self.set_outrate > 0:
            self.outrate(self.set_outrate)

    def preparemsg(self, parent=None, child=None, text=None):
        """Prepare an XML message using the default root tag and possible
parent and child tags, include text in the lowest-level element"""
        msg = etree.Element(self.roottag)
        if not isinstance(text, list):
            text = [text]
        if parent is None:
            msg.text = text[0]
            return(etree.tostring(msg))
        if not isinstance(parent, list):
            parent = [parent]
        p1 = [etree.SubElement(msg, p) for p in parent]
        if child is None:
            for p, t in zip(p1, text):
                p.text = t
            return(etree.tostring(msg))
        if not isinstance(child, list):
            child = [child]
        for p in p1:  # This will only work if we have 1 parent
            p2 = [etree.SubElement(p, c) for c in child]
            for l, t in zip(p2, text):
                l.text = str(t)
        return(etree.tostring(msg))

    def processrep(self, msg, rep, parent=None, query=True):
        """Check if the reply is OK (ack true) and return message converted
to dict"""
        rep = xmltodict.parse(rep)
        rep = rep[self.model]
        if query:
            if rep['ack'] != 'true':
                self.logger.debug('bad command %s' % msg)
                return None
        else:
            if parent is not None:
                out = dict([(c, b) for c, b in rep[parent].items()])
                return(out)
            else:
                return(True)

    def stop(self):
        self.outrate(0)
        self.dostop.set()

    def outrate(self, rate):
        """ Set the output rate (rate/s)"""
        msg = self.preparemsg(parent='cfg',
                              child='outrate',
                              text=rate)
        self.write(msg)

    def setfilter(self, rate):
        """ Set the output rate (rate/s)"""
        msg = self.preparemsg(parent='cfg',
                              child='filter',
                              text=rate)
        self.write(msg)
        self.getstatus(parent='cfg')

class reader(object):
    """Licor 830/850 reader. variables is comma-separated names for
variables to be returned from the object, askvariables is names of
variables read from the device.

    """

    def __init__(self, connobject, variables, interval, name, nick, mode="query", askvariables=None):
        self.connobject = connobject['obj']
        self.interval = interval
        self.nick = nick
        self.port = self.connobject.port
        self.lock = self.connobject.lock
        self.logger = self.connobject.logger
        self.model = self.connobject.model
        self.connobject.setfilter(interval)
        self.variables = variables.split(',')
        if askvariables is None:
            self.askvariables = [a.lower() for a in self.variables]
        else:
            self.askvariables = askvariables.split(',')
        if mode == "query":
            self.connobject.outrate(0)
            self.getval = self.query
        if mode == "poll":
            self.connobject.outrate(self.interval)
            self.connobject.set_outrate = self.interval
            self.getval = self.poll
            self.lastread = time.time()
        self.stop = self.connobject.stop

    def query(self):
        """ Read the variables from the device"""
        msg = self.connobject.preparemsg(parent='data', text='?')
        rep = self.connobject.write(msg)
        try:
            rep = self.connobject.processrep(msg, rep, parent='data')
        except ExpatError:
            rep = dict()
        return(rep)

    def read(self):
        """Read the variables and return a dict of them matching the input
and output variable names"""
        vals = self.getval()
        out = dict()
        for a, b in zip(self.variables, self.askvariables):
            try:
                out[a] = float(vals[b])
            except ValueError:
                out[a] = vals[b]
            except KeyError:
                out[a] = None
        return(out)

    def poll(self):
        """Wait for data to appear on the port, return a dict of values matching the input and output variable names"""
        while True:
            if self.connobject.dostop.is_set():
                return
            if self.lock.acquire(timeout=self.interval/10):
                break
        if self.port.inWaiting() > 0 and self.lastread < time.time() - self.interval:
            self.port.flushInput()
        if self.port.timeout is None:
            self.port.timeout = self.interval/5
        rep = self.port.read_until()
        self.lock.release()
        rep = self.connobject.processrep(msg=None, rep=rep, parent="data", query=False)
        self.lastread = time.time()
        return rep

class control(object):
    """Licor 830/850 controller object. Controlled thing is set with
parameter "target": currently allowed value is "pump"."""

    def __init__(self, connobject, name, nick, value, co2span=None, target='pump'):
        self.connobject = connobject['obj']
        self.port = self.connobject.port
        self.lock = self.connobject.lock
        self.logger = self.connobject.logger
        self.model = self.connobject.model
        self.nick = nick
        self.name = name
        try:
            self.setstate = getattr(self, target)
        except AttributeError:
            self.logger.critical('Invalid target %s!' % target)
            raise
        self.target = target
        self.co2span = co2span
        self.value = value

    def pump(self, state):
        """ Turn the onboard pump on or off"""
        msg = self.connobject.preparemsg(parent='pump',
                                         child='enabled',
                                         text=repr(bool(state)))
        self.connobject.outrate(0)
        self.connobject.write(msg)
        self.connobject.getstatus(parent='pump')
        self.connobject.outrate(self.connobject.set_outrate)

    def getstate(self):
        if self.target == 'pump':
            self.connobject.getstatus(parent='pump')
            return
        if self.target == 'outrate':
            self.connobject.getstatus(parent='cfg')
            return

    def calibrate(self, auto=False, span=False, zero=False):
        """ Perform CO2 zero or span calibration"""
        operation = list(compress(['span', 'zero'], [span, zero]))
        if len(operation) > 1:
            print("Only one calibration operation per call!")
            return
        operation = operation[0]
        if not auto:
            ans = input('Perform CO2 %s of %s? (Y/N)' %
                        (operation, self.connobject.name))
            if ans.upper() == 'Y':
                if self.co2span is None and span:
                    co2span = float(
                        input('Give the CO2 concentration (ppm) in the span gas:'))
                if self.co2span is not None:
                    co2span = self.co2span
                ans = input(
                    'Make sure calibration gas is flowing and press enter.. (cancel by entering "C"')
                if ans.upper == 'C':
                    return
            else:
                print("No calibration done")
                return
        currdate = str(datetime.date.today())
        msg = self.connobject.preparemsg(parent='cal',
                                         child=['date', 'co2%s' % operation],
                                         text=[currdate, '%s' % list(compress(['true', co2span], [zero, span]))[0]])
        rep = self.connobject.write(msg, delay=True)
        success = self.connobject.processrep(msg, rep)
        if success:
            self.logger.info('Successful CO2 %s' %
                             operation)
        else:
            self.logger.critical('Unsuccessful CO2 %s!' %
                                 operation)
