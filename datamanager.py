#!/usr/bin/python

"""Datamanager

Includes manager class for data handling (data sources and data
sinks). Reads data from sources and writes them into sinks, according
to which variables are meant to go where in main.ini

Class timer cycles through the sources and sinks and
reads from sources and writes to sinks according to their intervals

"""

import time
import threading
from prettytable import PrettyTable

from time import sleep


class manager(object):
    """Creates the sources and sinks.

Parameters:
sinks: dictionary created by createSinks in koppi
datasources: dictionary created by createSources in koppi
outvars: variable dictionary created by koppi
latest: list of latest observations per input created by koppi
lock: threading.Lock object for outvars
tstampcol: name for time stamp column (in local time)
currSource: list whose sole element has the name of current sample source
currSourcelock: lock for writing and reading the currSource variable
logger: logger object created by koppi
"""

    def __init__(self, sinks, datasources, outvars, latest,
                 lock, tstampcol, currSource=None,
                 currSourcelock=None, logger=None):
        self.logger = logger
        self.sinks = [self.sinkwriter(sink=sinks[si]['obj'],
                                      outvars=outvars, latest=latest,
                                      lock=lock, tstampcol=tstampcol, name=si,
                                      logger=self.logger,
                                      currSource=currSource,
                                      currSourcelock=currSourcelock,
                                      recsource=sinks[si]['obj'].recsource)
                      for si in list(sinks.keys())]
        self.datasources = [self.datasourcereader(datasource=datasources[so]['obj'],
                                                  outvars=outvars,
                                                  latest=latest,
                                                  lock=lock, name=so,
                                                  logger=self.logger)
                            for so in list(datasources.keys())]

    def getSinks(self, obj=False):
        if obj:
            return self.sinks
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.sinks:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)
        return None

    def getSources(self, obj=False):
        if obj:
            return self.datasources
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.datasources:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)

    class datasourcereader:
        """Reads a datasource and writes its variables into the outvars dictionary.
Reading interval and variables are defined in the datasource.

Locking for communications ports is handled by the datasources internally

Parameters:
datasource: data source object
outvars: dictionary for observations of variables
latest: list for times of latest observations of variables
lock: threading.Lock object for outvars
name: string: name of the datasource
logger: logger object
"""

        def __init__(self, datasource, outvars, latest, lock, name, logger):
            self.name = name
            self.datasource = datasource
            self.logger = logger
            self.interval = int(self.datasource.interval)
            self.nextread = round(time.time()) + self.interval
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict to keep count of when variables have last been recored
            self.lock = lock
            # for locking the outvars dict to prevent race conditions
            self.variables = self.datasource.variables
            self.now = time.time()
            self.row = False

        def read(self, now=False):
            try:
                self.logger.debug('%s reading' % self.name)
                self.row = self.datasource.read()
                self.logger.debug('%s read' % self.name)
            except Exception as e:
                self.row = dict((s, 'NA') for s in self.variables)
                self.logger.debug('reader %s: %s', self.name, e.message)
            # source.read must return dict of values for variables
            if not now:
                self.now = time.time()
            else:
                self.now = now
            self.nextread = round(self.now) + self.interval
            # When to read the next time

        def collect(self):  # Gather the read data into the outvars dict
            with self.lock:
                for s in self.variables:
                    self.outvars[s][self.now] = self.row[s]
                    self.latest[s].put(self.now)

    class sinkwriter:
        """Reads variables from the outvars dictionary and writes them to a
data sink.  Capable of outputting averaged values over n observations,
defined in the data sink.

Parameters:
sink: data sink object
outvars: the outvars dictionary
latest: the list of latest observations per input
lock: threading.Lock for outvars
tstampcol: name for time stamp column
name: name of the sink
logger: logger object
currSource: list containing the current sample source
currSourcelock: lock for the currSource list
recsource: boolean: record the current source in the sink

        """

        def __init__(self, sink, outvars, latest, lock, tstampcol, name,
                     logger, currSource, currSourcelock, recsource):
            self.name = name
            self.sink = sink
            self.logger = logger
            self.currSource = currSource
            self.currSourcelock = currSourcelock
            self.tstampcol = tstampcol
            self.recsource = recsource
            self.interval = int(self.sink.interval)
            try:
                self.average = int(self.sink.average)
            except AttributeError:
                self.average = 1
            try:
                self.vwindow = int(self.sink.vwindow)
            except AttributeError:
                self.vwindow = 1
            self.nextwrite = round(time.time()) + \
                (self.interval * self.average)
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict object that has times of latest records of all variables
            self.lock = lock
            # for locking the outvars dict to prevent race conditions
            self.variables = sink.variables
            self.row = dict()
            self.lastwrite = None
            self.sinknow = round(time.time())

        def write(self):  # Write data into the sink
            self.lastwrite = time.time()
            self.logger.debug('%s writing' % self.name)
            self.sink.write(self.row)
            self.logger.debug('%s wrote' % self.name)
            # sink.write must accept dict object with var names and time stamp
            self.nextwrite = round(self.lastwrite) + \
                (self.interval * self.average)
            self.row = None

        def collect(self):  # Collect the data to write from the outvars
            with self.lock:
                if self.average > 1:
                    self.averager()
                # elif self.vwindow > 1:
                #     self.window()
                else:
                    self.unival()
            self.addtime()
            self.addsource()

        def averager(self):  # For sending out averaged values
            if all([self.testlatest(p) for p in range(self.average)]):
                self.row = dict()
                gettimes = self.latest[self.variables[0]].get()[0:self.average]
                self.sinknow = sum(gettimes) / self.average
                # Record timestamp for write step (mean of
                # time stamps for averaging sinks)
                for s in self.variables:
                    self.row[s] = []
                    for m in gettimes:
                        self.row[s].append(self.outvars[s][m])
                        #                        print self.row[s]
                    self.row[s] = sum(self.row[s]) / self.average
            else:
                raise ValueError('Not all variables have same time stamp!')

        def unival(self):  # For sending out single values
            if self.testlatest(0):
                self.row = dict()
                gettimes = self.latest[self.variables[0]].get()[0]
                self.sinknow = gettimes
                for s in self.variables:
                    self.row[s] = self.outvars[s][gettimes]
            else:
                raise ValueError('Not all variables have same time stamp!')

        # def window(self):  # For sending out time windows of single values
        #     """ Don't use this, there's no reason for it! """
        #     if all([self.testlatest(p) for p in range(self.vwindow)]):
        #         self.row = dict()
        #         gettimes = self.latest[self.variables[0]].get()[self.vwindow]
        #         self.sinknow = gettimes
        #         for s in self.variables:
        #             self.row[s] = [self.outvars[s][t] for t in gettimes]
        #     else:
        #         raise ValueError('Not all variables have same time stamp!')

        def addtime(self):
            """Add timestamp here to not necessitate naming
               the timestamp column in each sink"""
            self.row[self.tstampcol] = time.strftime('%Y-%m-%d %H:%M:%S',
                                                     time.localtime(self.sinknow))
            self.row['epochtime'] = self.sinknow

        def addsource(self):
            if self.recsource:
                with self.currSourcelock:
                    self.row['Source'] = self.currSource[0]

        def testlatest(self, n):
            testl = iter({k: self.latest[k].get()[n] for k in self.variables
                          if k not in
                          [self.tstampcol, 'epochtime', 'Source']}.values())
            first = next(testl)
            return all(first == item for item in testl)


class timer(object):
    """Timer for data reading and writing. Performs threaded reads and
writes to speed up the operation.

Parameters:
datamanager: datamanager.manager object
running: threading.Event: are we running?
logger: logger object
sleeptime: float: time to sleep between checking if something should be done,
    seconds
"""

    def __init__(self, datamanager, running, logger=None, sleeptime=0.05):
        self.datamanager = datamanager
        self.running = running
        self.status = 'starting'
        self.sleeptime = sleeptime
        self.dostop = threading.Event()
        self.now = time.time()
        self.logger = logger

    def run(self):
        while True:
            self.status = 'waiting'
            while not self.running.is_set():
                if self.dostop.is_set():
                    return
                sleep(1)
            while self.running.is_set():
                self.status = 'running'
                self.now = time.time()
                # self.act()
                self.act_th()  # Try the threading interface
                if self.dostop.is_set():
                    return
                sleep(self.sleeptime)

    def act(self):
        for so in self.datamanager.datasources:
            if self.now >= so.nextread:
                so.read(self.now)
                so.collect()
        for si in self.datamanager.sinks:
            if self.now >= si.nextwrite:
                try:
                    si.collect()
                    si.write()
                except IndexError:  # Not enough observations for write yet
                    self.logger.debug('datatimer: Not enough \
observations for write')
                except ValueError as e:
                    self.logger.debug('datatimer: %s', e.message)

    def act_th(self):
        rthreads = list()
        wthreads = list()
        for so in self.datamanager.datasources:
            if self.now >= so.nextread:
                t = threading.Thread(target=so.read, args=(self.now, ))
                rthreads.append(t)
                t.start()
        for th in rthreads:
            th.join()
        for so in self.datamanager.datasources:
            so.collect()
        for si in self.datamanager.sinks:
            if self.now >= si.nextwrite:
                try:
                    si.collect()
                    t = threading.Thread(target=si.write)
                    wthreads.append(t)
                    t.start()
                except IndexError:  # Not enough observations for write yet
                    self.logger.debug('datatimer %s: Not enough \
observations for write' % si.name)
                    si.nextwrite = round(time.time()) + \
                        (si.interval * si.average)
                except ValueError as e:
                    self.logger.debug('datatimer: %s', e.message)
        for th in wthreads:
            th.join()

    def stop(self):
        self.dostop.set()
