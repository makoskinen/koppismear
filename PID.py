#!/usr/bin/python
#
# This file is part of IvPID.
# Copyright (C) 2015 Ivmech Mechatronics Ltd. <bilgi@ivmech.com>
#
# IvPID is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# IvPID is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# title           :PID.py
# description     :python pid controller
# author          :Caner Durmusoglu
# date            :20151218
# version         :0.1
# notes           :
# python_version  :2.7
# ==============================================================================

"""Ivmech PID Controller is simple implementation of a
Proportional-Integral-Derivative (PID) Controller in the Python
Programming Language.  More information about PID Controller:
http://en.wikipedia.org/wiki/PID_controller

"""
import time

from time import sleep

import threading


class PID:
    """PID Controller
    """

    def __init__(self, controls, variables, outvars, outvarslock,
                 datarunning, name, nick, interval, setpoint, value,
                 logger=None, p=0.2, i=0.0, d=0.0, function='PID',
                 duration=None, level=None, sleeptime=None,
                 initialdelay=20):
        self.value = value
        self.outvars = outvars
        self.outvarslock = outvarslock
        self.running = datarunning
        self.doing = list()
        self.done = list()
        self.invars = dict()
        self.mfcthread = None
        self.interval = int(interval)
        self.nick = nick
        self.recsource = False
        self.addvars = False
        self.dostop = threading.Event()  # Really not used..
        if variables.split(','):
            self.variables = variables.split(',')
            self.addvars = True
        else:
            self.variables = [variables]
        self.control = controls[name]['obj']
        self.Kp = float(p)
        self.Ki = float(i)
        self.Kd = float(d)
        self.logger = logger
        self.sample_time = 0.00
        self.initialdelay = initialdelay
        self.current_time = time.time()
        self.changetime = 0
        if function == 'dose':
            self.last_time = self.current_time - sleeptime
        else:
            self.last_time = self.current_time
        self.windup_guard = 40.0
        self.sp = setpoint
        self.lastoutput = None
        self.output = None
        self.function = function
        if function == 'dose':
            if duration > self.interval:
                self.thread_dose = True
            else:
                self.thread_dose = False
            self.duration = duration  # duration of dose
            self.level = level  # output value of dose
            self.sleeptime = sleeptime  # time to be nonresponsive ...
            # ... between doses
        self.clear()
        self.thread = threading.Thread(target = self.run)
        self.thread.start()

    def clear(self):
        """Clears PID computations and coefficients"""
        self.SetPoint = self.sp

        self.PTerm = 0.0
        self.ITerm = 0.0
        self.DTerm = 0.0
        self.last_error = 0.0

        # Windup Guard
        self.int_error = 0.0
        self.windup_guard = 20.0

        self.output = 0.0

    def dose(self, duration, level):
        """Sends a dose of CO2 from the MFC, at specified % of maximum flow
for a specified duration"""
        self.logger.debug('%s: sending dose of %s for %s seconds' %
                          (self.nick, level, duration))
        self.control.setstate(level)
        time.sleep(duration)
        self.control.setstate(0)
        return

    def update(self, fb):
        """Calculates PID value for given reference feedback

        .. math::
            u(t) = K_p e(t) + K_i \int_{0}^{t} e(t)dt + K_d {de}/{dt}

        .. figure:: images/pid_1.png
           :align:   center

           Test PID with Kp=1.2, Ki=1, Kd=0.001 (test_pid.py)

        """
        if self.function == 'dose':
            if time.time() < self.changetime + self.initialdelay:
                self.logger.debug('%s: waiting delay after source change (%s s)' % (
                    self.nick, self.initialdelay))
                return
            if self.last_time > time.time() - (self.sleeptime + self.duration):
                self.logger.debug(
                    '%s: waiting for CO2 level to settle after injection' % self.nick)
                return
            self.logger.debug('%s: comparing %s to %s' % (self.nick,
                                                          fb, self.SetPoint))
            if fb < self.SetPoint:
                if self.mfcthread is None:
                    self.mfcthread = threading.Thread(target=self.dose,
                                                      args=(self.duration,
                                                            self.level))
                    self.mfcthread.start()
                else:
                    if self.mfcthread.is_alive():
                        self.logger.debug(
                            '%s: previous dose still going on' % self.nick)
                        return
                    else:
                        self.mfcthread.join()
                        self.mfcthread = threading.Thread(target=self.dose,
                                                          args=(self.duration,
                                                                self.level))
                        self.mfcthread.start()

                self.last_time = time.time()
            return
        feedback_value = fb
        error = self.SetPoint - feedback_value

        self.current_time = time.time()
        delta_time = self.current_time - self.last_time
        delta_error = error - self.last_error

        if (delta_time >= self.sample_time):
            self.PTerm = self.Kp * error
            self.ITerm += error * delta_time

            if (self.ITerm < -self.windup_guard):
                self.ITerm = -self.windup_guard
            elif (self.ITerm > self.windup_guard):
                self.ITerm = self.windup_guard

            self.DTerm = 0.0
            if delta_time > 0:
                self.DTerm = delta_error / delta_time

            # Remember last time and last error for next calculation
            self.last_time = self.current_time
            self.last_error = error

            self.output = (self.PTerm
                           + (self.Ki * self.ITerm)
                           + (self.Kd * self.DTerm))
            if self.output > self.control.value[1]:
                self.logger.debug('%s: output value %s \
exceeds maximum value of control %s',
                                  self.nick,
                                  self.output,
                                  self.control.nick)
                self.output = self.control.value[1]
            elif self.output < self.control.value[0]:
                self.logger.debug('%s: output value %s \
is less than minimum value of control %s',
                                  self.nick,
                                  self.output,
                                  self.control.name)
                self.output = self.control.value[0]
            if self.output == self.lastoutput:
                return
            if self.mfcthread is None:
                self.startmfcthread()
            else:
                if not self.mfcthread.is_alive():
                    self.mfcthread.join()
                    self.startmfcthread()
                else:
                    self.logger.info(
                        '%s: previous setting thread still running!', self.nick)

    def startmfcthread(self):
        """Create and start the thread to set new control value"""
        self.mfcthread = threading.Thread(target=self.control.setstate,
                                          args=(self.output,))
        self.mfcthread.start()

    def setKp(self, proportional_gain):
        """Determines how aggressively the PID reacts to the
        current error with setting Proportional Gain"""
        self.Kp = proportional_gain

    def setKi(self, integral_gain):
        """Determines how aggressively the PID reacts to the
        current error with setting Integral Gain"""
        self.Ki = integral_gain

    def setKd(self, derivative_gain):
        """Determines how aggressively the PID reacts to the
        current error with setting Derivative Gain"""
        self.Kd = derivative_gain

    def setWindup(self, windup):
        """Integral windup, also known as integrator windup or reset windup,
        refers to the situation in a PID feedback controller where
        a large change in setpoint occurs (say a positive change)
        and the integral terms accumulates a significant error
        during the rise (windup), thus overshooting and continuing
        to increase as this accumulated error is unwound
        (offset by errors in the other direction).
        The specific problem is the excess overshooting.
        """
        self.windup_guard = windup

    def setSampleTime(self, sample_time):
        """PID that should be updated at a regular interval.
        Based on a pre-determined sampe time, the PID decides
        if it should compute or return immediately.
        """
        self.sample_time = sample_time

    def setstate(self, value):
        if value == 0:
            if self.SetPoint == 0:
                return
            self.SetPoint = 0
            self.output = 0
            self.control.setstate(0)
            self.control.zerototal()
        else:
            self.SetPoint = float(value)

    def getstate(self):
        return self.SetPoint

    def stop(self):
        self.dostop.set()

    def write(self, value):
        ##self.logger.info('%s: received %s' % (self.nick, value))
        if self.SetPoint == 0:
            pass
        else:
            try:
                self.update(sum([float(value[i]) for i in self.variables]))
            except (TypeError, ValueError):
                self.logger.debug('%s: Bad value %s for input' % (
                    self.nick, [str(value[i]) for i in self.variables]))
                pass
            except KeyError:
                self.logger.debug(
                    '%s: not all variables available' % self.nick)
                #                pass

    def run(self):
        """Read the outvars 5 times per interval, check for new data that's become available; if some data is missing, create an entry in list "doing". Insert available data into rows in list "doing"; when a row is full or is older than max wait, update the control setting if all variables are available, discard if not, add timestamp into list "done". This function runs continuously as a thread that's started in __init__."""
        while True:
            while not self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('PID %s stopping' % self.nick)
                    return
                time.sleep(0.5)
            previouswrite = int(time.time())
            while self.running.is_set():
                if self.dostop.is_set():
                    self.logger.debug('PID %s stopping' % self.nick)
                    return
                while self.outvarslock.locked():
                    time.sleep(0.01)
                self.outvarslock.acquire()
                self.logger.debug(
                    'PID %s acquired outvarslock 1' % self.nick)
                try:
                    latest = {s: max(list(self.outvars[s].keys()))
                              for s in self.variables}
                    latest_max = max(list(latest.values()))
                except (ValueError, KeyError):
                    self.outvarslock.release()
                    self.logger.debug(
                        'PID %s released outvarslock 1 after exception' % self.nick)
                    time.sleep(self.interval/5)
                    continue
                self.outvarslock.release()
                self.logger.debug(
                    'PID %s released outvarslock 1;latest_max %s, previouswrite %s' % (self.nick, latest_max, previouswrite))
                while (latest_max - previouswrite) < self.interval:
                    self.logger.debug('PID %s: latest_max %s, previouswrite %s' % (
                        self.nick, latest_max, previouswrite))
                    if not self.running.is_set():
                        break
                    if len(self.doing) > 0:
                        self.logger.debug(
                            'PID %s: checking doing list' % self.nick)
                        for t in self.doing:
                            self.logger.debug(
                                'PID %s: trying %s in doing' % (self.nick, t))
                            for v in self.variables:
                                if not v in list(self.invars[t].keys()):
                                    if t in list(self.outvars[v].keys()):
                                        self.invars[t][v] = self.outvars[v][t]
                            if (all(v in list(self.invars[t].keys()) for v in self.variables) or (time.time() - t) > self.maxwait) and t == min(self.doing):
                                self.invars[t].update(
                                    {v: 'NA' for v in self.variables if not v in list(self.invars[t].keys())})
                                self.logger.debug(
                                    'PID %s: writing at %s' % (self.nick, t))
                                if 'NA' in self.invars[t]:
                                    self.doing.remove(t)
                                    self.done = self.done + [t]
                                    continue
                                self.write(self.invars[t])
                                self.doing.remove(t)
                                self.done = self.done + [t]
                        time.sleep(self.interval/5)
                    while self.outvarslock.locked():
                        time.sleep(0.01)
                    self.outvarslock.acquire()
                    self.logger.debug(
                        'PID %s acquired outvarslock 2' % self.nick)
                    latest = {s: max(list(self.outvars[s].keys()))
                              for s in self.variables}
                    self.outvarslock.release()
                    self.logger.debug(
                        'PID %s released outvarslock 2' % self.nick)
                    latest_max = max(list(latest.values()))
                    time.sleep(self.interval/5)
                if not latest_max in self.doing + self.done:
                    previouswrite = latest_max
                    self.logger.debug(
                        'PID %s: getting new data at %s' % (self.nick, latest_max))
                    self.invars[latest_max] = dict()
                        ##self.invars[latest_max], latest_max)
                    if (not all(a == latest_max for a in list(latest.values()))) or len(self.doing) > 0:
                        self.doing = self.doing + [latest_max]
                    else:
                        while self.outvarslock.locked():
                            time.sleep(0.01)
                        self.outvarslock.acquire()
                        self.logger.debug(
                            'PID %s acquired outvarslock 3' % self.nick)
                        self.invars[latest_max].update(
                            {v: self.outvars[v][latest_max] for v in self.variables})
                        self.outvarslock.release()
                        self.logger.debug(
                            'PID %s released outvarslock 3' % self.nick)
                        self.write(self.invars[latest_max])
                        self.logger.debug('PID %s wrote row %s' % (
                            self.nick, repr(self.invars[latest_max])))
                        self.done = self.done + [latest_max]
                time.sleep(self.interval/5)
