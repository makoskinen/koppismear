#!/usr/bin/python

import time
import threading
from prettytable import PrettyTable
from ReadIni import ReadIni
from xml.parsers.expat import ExpatError


class manager(object):
    """Manages the control objects and sets sample sources"""

    def __init__(self, cfg, controls, logger=None):
        try:
            self.default = cfg['general']['default']
            self.cfg = cfg
        except TypeError:
            self.cfg = ReadIni(cfg)
            self.default = self.cfg['general']['default']
        self.sources = {i: self.cfg[i] for i in self.cfg if i != 'general'}
        self.controls = controls
        self.logger = logger.getChild(suffix = "controlmanager")
        self.values = dict()
        # to sort keys
        self.ckeys = list(self.controls.keys())
        self.ckeys.sort()
        for k in self.ckeys:
            try:
                self.values[k] = self.controls[k]['obj'].value
            except AttributeError:
                continue
        self.log = open(self.cfg['general']['logfile'], 'a+')

    def setSource(self, source):
        """Set source to parameter source"""
        if source == 'default':
            source = self.default
        setvals = self.sources[source]
        self.logger.debug('setSource: %s' % setvals)
        setkeys = list(setvals.keys())
        try:
            delay = setvals['delay']
            setkeys.remove('delay')
        except KeyError:
            delay = 0
        try:
            recchange = setvals['record']
            setkeys.remove('record')
        except KeyError:
            recchange = False
        setkeys.sort()
        cthreads = list()
        for c in setkeys:
            self.logger.debug('setting \
control %s to value %s', self.controls[c]['obj'].nick,
                              setvals[c])
            t = threading.Thread(target=self.controls[c]['obj'].setstate,
                                 args=(setvals[c],))
            cthreads.append(t)
            t.start()
        for th in cthreads:
            th.join()
        self.logState()
        self.logger.debug(
            'setSource: %s, delay: %s, recchange: %s', source, delay, recchange)
        return((source, delay, recchange))

    def logState(self):
        """Write the current states of all controls to control log file"""
        logout = dict([(self.controls[k]['obj'].nick,
                        self.controls[k]['obj'].getstate()) for k in list(self.controls.keys())])
        self.log.write('%s: %s\n' % (time.strftime('%Y-%m-%d %H:%M:%S',
                                                   time.localtime()), repr(logout).strip('{}')))
        self.log.flush()

    def setState(self, control, value):
        """Set state of named control to value"""
        try:
            if isinstance(self.values[control], list):
                # Sets of individual accepted values are given as lists
                if value not in self.values[control]:
                    raise ValueError('Invalid value {} for control {} (accepted \
values {})'.format(value, control, ','.join(v for v in self.values[control])))
            elif isinstance(self.values[control], tuple):
                # Ranges of accepted values are given as two-element tuples
                if not self.values[control][0] <= value <= self.values[control][1]:
                    raise ValueError('Invalid value {} for control {} (accepted values \
between {} and {})'.format(value, control, self.values[control][0], self.values[control][1]))
        except ValueError as e:
            self.logger.warning('setState: %s', e)
        else:
            self.controls[control]['obj'].setstate(value)

    def show_controls(self):
        """Print table of available controls and their possible values"""
        pt = PrettyTable(['name', 'nick', 'values', 'current'])
        for a in sorted(self.controls.keys()):
            try:
                vals = self.controls[a]['obj'].value
            except AttributeError as ExpatError:
                vals = None
            pt.add_row([a, self.controls[a]['obj'].nick,
                        vals, self.controls[a]['obj'].getstate()])
        print(pt)

    def get_controls(self):
        """Get the controls as an object (dict)"""
        return self.controls
