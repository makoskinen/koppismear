#!/usr/bin/python

"""Database (SQLite) management: insert rows, get csv out, stored
procedures for exporting users and stuff"""

import sqlite3
import csv

from sqlite3 import OperationalError


class base:
    def __init__(self, dbfile, tabledef):
        self.db = sqlite3.connect(dbfile)
        self.dbc = self.db.cursor()
        self.tables = self.gettables()
        if len(self.tables) == 0:
            self.createtables(tabledef)

    def __del__(self):
        self.db.commit()
#        self.db.close()

    # Create tables based on table definition, tabledef is filename of a file containing sql commands
    def createtables(self, tabledef):
        self.parsesqlfile(tabledef)

    # Dangerous in the extreme! But never mind as we are only using sqlite files...
    def parsesqlfile(self, filename):
        # Open and read the file as a single buffer
        fd = open(filename, 'r')
        sqlFile = fd.read()
        fd.close()

        # all SQL commands (split on ';')
        sqlCommands = sqlFile.split(';')

        # Execute every command from the input file
        for command in sqlCommands:
            # This will skip and report errors
            # For example, if the tables do not yet exist, this will skip over
            # the DROP TABLE commands
            try:
                self.dbc.execute(command)
            except OperationalError as msg:
                print("Command skipped: ", msg)

    def gettables(self):
        self.dbc.execute("select name from sqlite_master where type = 'table'")
        tables = self.dbc.fetchall()
        return(tables)

    def getcols(self, table):
        self.dbc.execute("PRAGMA table_info(%s)" % table)
        columns = self.dbc.fetchall()
        columns = [k[1] for k in columns]
        return(columns)

    def writerow(self, table, cols, vals):
        if len(vals) > 1:
            vals = ','.join(vals)
        sql = "insert into %s (%s) values (%s)" % (table, cols, vals)
#        print sql
        self.dbc.execute(sql)

    def getdata(self, table, username=None, datestart=None, dateend=None, tstampcol='tstamp'):
        if (username, datestart, dateend) == (None, None, None):  # Get all data
            self.dbc.execute("select * from %s") % table
        elif (datestart, dateend) == (None, None):  # Get all data from certain user
            self.dbc.execute(
                "select * from %s where username = %s") % (table, username)
        elif username is None:  # Get all data from a time period
            self.dbc.execute("select * from %s where %s between %s and %s") % (
                table, tstampcol, datestart.split(";")[0], dateend.split(";")[0])
        else:  # Get data from certain user from certain daterange
            self.dbc.execute("select * from %s where username = %s and %s between %s and %s") % (
                table, username, tstampcol, datestart.split(";")[0], dateend.split(";")[0])
        cols = [member[0] for member in self.dbc.description]
        data = self.dbc.fetchall()
        return([cols, data])

    def csvout(self, table, user, datestart, dateend, filename, append=False):
        (cols, data) = self.getdata(table, user, datestart, dateend)
        if append:
            outf = open(filename, 'ab')
        else:
            outf = open(filename, 'wb')
        writer = csv.writer(outf, delimiter=";")
        if not append:
            writer.writerow(cols)
        for r in data:
            writer.writerow(r)
        outf.close()


class mediator(base):
    def __init__(self, port, logger=None):
        self.database = port['obj']
        self.lock = port['lock']
        self.logger = logger
        self.dbc = self.database.dbc
        self.db = self.database.db


class writer(base):
    def __init__(self, connobject, table, interval, variables, tstamp, **kwargs):
        self.table = table  # table name as text
        self.tstampcol = tstamp
        self.connobject = connobject['obj']
        self.db = self.connobject.db
        self.dbc = self.connobject.dbc
        self.logger = self.connobject.logger
        self.database = self.connobject.database  # database-object
        self.lock = self.connobject.lock
        self.variables = variables
        # Won't work if there's only 1 column!
        self.columns = ','.join(self.database.getcols(self.table))
        self.ncol = self.columns.count(',') + 1
        self.interval = interval

    def write(self, row):
        if self.tstampcol in row:
            if not row[self.tstampcol].startswith('"'):
                row[self.tstampcol] = '"{0}"'.format(row[self.tstampcol])
#        print row
        outrow = [str(row[r]) for r in self.columns.split(',')]
#        print outrow
        if len(outrow) != self.ncol:
            raise ValueError("Number of values and columns don't match!")
        self.database.writerow(self.table, self.columns, outrow)
