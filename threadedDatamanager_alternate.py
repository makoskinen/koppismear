#!/usr/bin/python

"""Datamanager

Includes manager class for data handling (data sources and data
sinks). Reads data from sources and writes them into sinks, according
to which variables are meant to go where in main.ini

Class timer cycles through the sources and sinks and
reads from sources and writes to sinks according to their intervals

"""

import time
import datetime
import timerutils
import threading
from prettytable import PrettyTable

from time import sleep


class manager(object):
    """Creates the sources and sinks.

Parameters:
sinks: dictionary created by createSinks in koppi
datasources: dictionary created by createSources in koppi
outvars: variable dictionary created by koppi
latest: list of latest observations per input created by koppi
lock: threading.Lock object for outvars
tstampcol: name for time stamp column (in local time)
currSource: list whose sole element has the name of current sample source
currSourcelock: lock for writing and reading the currSource variable
logger: logger object created by koppi
"""

    def __init__(self, sinks, datasources, outvars, latest,
                 lock, latestlock, tstampcol, currSource=None,
                 currSourcelock=None, logger=None, timeprec=0):
        self.logger = logger.getChild("Datamanager")
        self.timeprec = timeprec
        self.sinks = [self.sinkwriter(sink=sinks[si]['obj'],
                                      outvars=outvars, latest=latest,
                                      lock=lock,
                                      latestlock=latestlock,
                                      tstampcol=tstampcol, name=si,
                                      logger=self.logger,
                                      currSource=currSource,
                                      currSourcelock=currSourcelock,
                                      recsource=sinks[si]['obj'].recsource,
                                      outofsync=threading.Event())
                      for si in list(sinks.keys())]
        self.datasources = [self.datasourcereader(datasource=datasources[so]['obj'],
                                                  outvars=outvars,
                                                  latest=latest,
                                                  lock=lock, latestlock=latestlock, name=so,
                                                  logger=self.logger, timeprec = self.timeprec)
                            for so in list(datasources.keys())]
        self.dostop = threading.Event()
        self.sleeptime = min(so.interval for so in self.datasources)/5

    def getSinks(self, obj=False):
        if obj:
            return self.sinks
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.sinks:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)
        return None

    def getSources(self, asobj=False, asdict=False):
        if asobj:
            return self.datasources
        if asdict:
            return dict([(a, a.variables) for a in self.datasources])
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.datasources:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)

    class datasourcereader:
        """Reads a datasource and writes its variables into the outvars dictionary.
Reading interval and variables are defined in the datasource.

Locking for communications ports is handled by the datasources internally

Parameters:
datasource: data source object
outvars: dictionary for observations of variables
latest: list for times of latest observations of variables
lock: threading.Lock object for outvars
name: string: name of the datasource
logger: logger object
"""

        def __init__(self, datasource, outvars, latest, lock, latestlock, name, logger, timeprec):
            self.name = name
            self.datasource = datasource
            self.logger = logger.getChild(self.name)
            self.timeprec = timeprec
            self.sleeptime = eval(f"1e{-(self.timeprec + 2)}")
            self.interval = timerutils.divsum(self.datasource.interval)
            self.nextread = timerutils.nexteven(self.interval)
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict to keep count of when variables have last been recored
            self.lock = lock
            # for locking the outvars dict to prevent race conditions
            self.latestlock = latestlock
            self.dostop = threading.Event()
            self.variables = self.datasource.variables
            self.now = time.time()
            self.row = False
            self.joined = False
            while not self.latestlock.acquire(timeout = self.sleeptime):
                if self.dostop.is_set():
                    self.stop()
            for s in self.variables:
                self.latest[s].put(0)
                self.latest[s + ".nextread"].put(self.nextread)
            self.latestlock.release()


        def read(self, now=False):
            self.joined = False
            if not now:
                self.now = time.time()
            else:
                self.now = now
            self.nextread = timerutils.nexteven(timedefn = self.interval, now = self.now)
            with self.latestlock:
                for s in self.variables:
                    self.latest[s + '.nextread'].put(self.nextread)
            try:
                self.logger.debug('reading at %s' % self.now)
                self.row = self.datasource.read()
                self.logger.debug('read %s at %s' %
                                  (self.row, self.now))
            except Exception as e:
                self.row = dict((s, 'NA') for s in self.variables)
                self.logger.debug('reader %s: %s', self.name, repr(e))
            # source.read must return dict of values for variables
            self.collect()
            self.logger.debug('next reading at %s' % self.nextread)
            # When to read the next time

        def collect(self):  # Gather the read data into the outvars dict
            lc = 0
            while True:
                if self.dostop.is_set():
                    self.logger.debug('collect: dostop set, exiting..')
                    return
                lc += 1
                if lc == 1000:
                    self.logger.debug("collect: waiting for outvars lock..")
                    lc = 0
                if self.lock.acquire(timeout=self.sleeptime):
                    break
            self.logger.debug(
                'collect: acquired outvars lock..')
            while True:
                if self.dostop.is_set():
                    self.logger.debug('collect: dostop set, exiting..')
                    self.lock.release()
                    return
                if self.latestlock.acquire(timeout=self.sleeptime):
                    break
            self.logger.debug('collect: acquired lastestlock..')
            for s in self.variables:
                try:
                    self.logger.debug(
                        f'collect: writing {s} at {self.now}')
                    self.outvars[s][self.now] = self.row[s]
                    self.latest[s].put(self.now)
                except (KeyError, TypeError) as e:
                    self.logger.debug(
                        'collect: %s not found at %s' % (s, self.now))
                    self.outvars[s][self.now] = None
                    self.latest[s].put(self.now)
            self.lock.release()
            self.latestlock.release()
            self.logger.debug(
                'collect: released outvars and latest lock..')

        def stop(self):
            self.logger.debug('stopping')
            self.datasource.stop()
            self.dostop.set()
            return

    class sinkwriter:
        """Reads variables from the outvars dictionary and writes them to a
data sink.  Capable of outputting averaged values over n observations,
defined in the data sink.

Parameters:
sink: data sink object
outvars: the outvars dictionary
latest: the list of latest observations per input
lock: threading.Lock for outvars
tstampcol: name for time stamp column
name: name of the sink
logger: logger object
currSource: list containing the current sample source
currSourcelock: lock for the currSource list
recsource: boolean: record the current source in the sink

        """

        def __init__(self, sink, outvars, latest, lock, latestlock,
                     tstampcol, name, logger, currSource,
                     currSourcelock, recsource, outofsync):
            self.name = name
            self.sink = sink
            self.logger = logger.getChild(name)
            self.timeprec = self.sink.timeprec
            self.timestampfmt = self.sink.timestampfmt
            self.currSource = currSource
            self.currSourcelock = currSourcelock
            self.tstampcol = tstampcol
            self.recsource = recsource
            self.outofsync = outofsync
            self.interval = timerutils.divsum(self.sink.interval)
            try:
                self.average = int(self.sink.average)
            except AttributeError:
                self.average = 1
            try:
                self.vwindow = int(self.sink.vwindow)
            except AttributeError:
                self.vwindow = 1
            try:
                self.nextwrite = timerutils.nexteven((self.interval * self.average))
            except ZeroDivisionError:
                self.logger.exception(f"Zero division in nexteven, interval {self.interval}, average {self.average}, sink interval {self.sink.interval}")
                raise ZeroDivisionError
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict object that has times of latest records of all variables
            self.lock = lock
            # for locking the outvars dict to prevent race condition
            self.latestlock = latestlock
            self.variables = sink.variables
            self.row = dict()
            self.lastwrite = None
            self.sinknow = round(time.time(), self.timeprec)
            self.joined = False

        def write(self, now=False):  # Write data into the sink
            if not now:
                self.lastwrite = round(time.time(), self.timeprec)
            else:
                self.lastwrite = round(now, self.timeprec)
            self.joined = False
            self.collect()
            if self.sink.dostop.is_set():
                return
            self.logger.debug('%s writing at %s' % (self.name, self.lastwrite))
            self.sink.write(self.row)
            self.logger.debug('%s wrote' % self.name)
            # sink.write must accept dict object with var names and time stamp
            self.row = None
            self.nextwrite = timerutils.nexteven((self.interval * self.average), now=self.lastwrite)

        def collect(self):  # Collect the data to write from the outvars
            complete = False
            c = 0
            while c < 18 and not complete:
                while self.latestlock.locked():
                    if self.sink.dostop.is_set():
                        return
                    sleep(0.01)
                with self.lock:
                    if self.average > 1:
                        complete = self.averager()
                    # elif self.vwindow > 1:
                    #     self.window()
                    else:
                        complete = self.unival()
                if not complete:
                    c += 1
                    sleep(self.interval/10)
            if not complete:
                self.unival(force=True)
                self.logger.debug(
                    'writecollect: Possible missing variables at %s' % self.sinknow)
            self.logger.debug(
                'writecollect: adding time and source at %s' % self.sinknow)
            self.addtime()
            self.addsource()

        def averager(self):  # For sending out averaged values
            try:
                if all([self.testlatest(p) for p in range(self.average)]):
                    self.row = dict()
                    gettimes = self.latest[self.variables[0]].get()[
                        0:self.average]
                    self.sinknow = sum(gettimes) / self.average
                    # Record timestamp for write step (mean of
                    # time stamps for averaging sinks)
                    for s in self.variables:
                        self.row[s] = []
                        for m in gettimes:
                            self.row[s].append(self.outvars[s][m])
                            #                        print self.row[s]
                        self.row[s] = sum(self.row[s]) / self.average
                    return True
            except IndexError:
                return False
            return False

        def unival(self, force=False):  # For sending out single values
            self.logger.debug("Unival running")
            try:
                if self.testlatest(0) or force:
                    self.row = dict()
                    if force:
                        print([self.latest[i].get()[0]
                               for i in self.variables])
                        gettimes = min([self.latest[i].get()[0]
                                        for i in self.variables])
                    else:
                        gettimes = max([self.latest[i].get()[0]
                                        for i in self.variables])
                    self.sinknow = gettimes
                    if force:
                        self.logger.debug(
                            "unival: used force at %s" % self.sinknow)
                    for s in self.variables:
                        try:
                            self.row[s] = self.outvars[s][gettimes]
                        except KeyError:
                            if self.latest[s + ".nextread"].get()[0] > self.nextwrite:
                                self.logger.debug("%s: appending NA as no data available" % s)
                                self.row[s] = "NA"
                            else:
                                self.row[s] = None
                    return True
            except IndexError:
                return False
            return False

        # def window(self):  # For sending out time windows of single values
        #     """ Don't use this, there's no reason for it! """
        #     if all([self.testlatest(p) for p in range(self.vwindow)]):
        #         self.row = dict()
        #         gettimes = self.latest[self.variables[0]].get()[self.vwindow]
        #         self.sinknow = gettimes
        #         for s in self.variables:
        #             self.row[s] = [self.outvars[s][t] for t in gettimes]
        #     else:
        #         raise ValueError('Not all variables have same time stamp!')

        def addtime(self):
            """Add timestamp here to not necessitate naming
               the timestamp column in each sink"""
            self.row[self.tstampcol] = datetime.datetime.strftime(datetime.datetime.fromtimestamp(self.sinknow), self.timestampfmt)
            self.row['epochtime'] = self.sinknow

        def addsource(self):
            if self.recsource:
                self.logger.debug('datamanager: Adding source')
                with self.currSourcelock:
                    self.row['Source'] = self.currSource[0]

        def testlatest(self, n):
            testl = iter({k: self.latest[k].get()[n] for k in self.variables
                          if
                          self.latest[k + ".nextread"].get()[0] < self.sinknow + self.interval
                          and
                          k not in [self.tstampcol, 'epochtime', 'Source']
                          }.values())
            first = next(testl)
            if int(first) == self.sinknow:
                return False
            diffs = [first - item for item in testl]
            nsync = [i % self.interval for i in diffs]
            if not all(s == 0 for s in nsync):
                self.logger.debug('datanamager: %s out of sync' % self.name)
                self.outofsync.set()
            return all(item == 0 for item in diffs)

        def stop(self):
            self.sink.stop()


class timer(object):
    """Timer for data reading and writing. Performs threaded reads and
writes to speed up the operation.

Parameters:
datamanager: datamanager.manager object
running: threading.Event: are we running?
logger: logger object
sleeptime: float: time to sleep between checking if something should be done,
    seconds
"""

    def __init__(self, datamanager, running, logger=None, sleeptime=0.001):
        self.datamanager = datamanager
        self.running = running
        self.status = 'starting'
        self.sleeptime = self.datamanager.sleeptime
        self.dostop = threading.Event()
        self.now = time.time()
        self.logger = logger.getChild(suffix="Datatimer")
        self.rthreads = dict([(so.name, None)
                              for so in self.datamanager.datasources])
        self.wthreads = dict([(si.name, None)
                              for si in self.datamanager.sinks])

    def run(self):
        while True:
            self.status = 'waiting'
            while not self.running.is_set():
                if self.dostop.is_set():
                    for so in self.datamanager.datasources:
                        so.stop()
                    for si in self.datamanager.sinks:
                        si.stop()
                    return
                sleep(1)
            while self.running.is_set():
                self.now = time.time()
                self.status = 'running'
                # self.act()
                if self.dostop.is_set():
                    for so in self.datamanager.datasources:
                        so.stop()
                    for si in self.datamanager.sinks:
                        si.stop()
                    return
                self.act_th()  # Try the threading interface
                timerutils.sleep_until(self.now+self.sleeptime)

    def act_th(self):
        #        self.logger.debug('datatimer: acting')
        for so in self.datamanager.datasources:
            if self.rthreads[so.name] is not None and not so.joined:
                if not self.rthreads[so.name].is_alive():
                    self.logger.debug('Joining %s', so.name)
                    so.joined = True
            if self.now >= so.nextread:
                self.rthreads[so.name] = threading.Thread(
                    target=so.read, args=(round(self.now, so.timeprec), ))
                self.logger.debug('Starting %s', so.name)
                self.rthreads[so.name].start()
#         for si in self.datamanager.sinks:
#             if self.wthreads[si.name] is not None:
#                 if not self.wthreads[si.name].is_alive() and not si.joined:
#                     self.wthreads[si.name].join()
#                     si.joined = True
#             if self.now >= si.nextwrite:
#                 if si.outofsync.is_set():
#                     sources = self.datamanager.getSources(asdict = True)
#                     tosync = []
#                     for v in si.variables:
#                         tosync = tosync + [so for so in self.datamanager.datasources if v in so.variables]
#                     tosync = list(set(tosync))
#                     synctime = round(self.now) + si.interval
#                     self.logger.debug('Syncing %s with time %s' % (tosync, synctime))
#                     for so in tosync:
#                         so.nextread = synctime
#                     si.nextwrite = round(self.now) + si.interval
#                     si.outofsync.clear()
#                     continue
#                 try:
#                     if self.wthreads[si.name] is not None:
#                         if not self.wthreads[si.name].is_alive():
#                             self.wthreads[si.name] = threading.Thread(target=si.write)
#                             self.wthreads[si.name].start()
#                             self.logger.debug('Started %s', si.name)
#                     else:
#                         self.wthreads[si.name] = threading.Thread(target=si.write)
#                         self.wthreads[si.name].start()
#                         self.logger.debug('Started %s', si.name)
#                 except IndexError:  # Not enough observations for write yet
#                     self.logger.debug('datatimer %s: Not enough \
# observations for write' % si.name)
#                     si.nextwrite = round(time.time()) + (si.interval * si.average)
#                 except ValueError as e:
#                     self.logger.info('datatimer: %s', e.message)

    def stop(self):
        self.dostop.set()
