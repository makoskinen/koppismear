#!/usr/bin/python
"""li78X0 - interface for Licor portable gas analysers, connecting
over TCP with MQTT protocol; data is collected as it arrives"""

import datetime
import threading

import time

import pandas as pd

import paho.mqtt.client as mqtt


class li78X0conn(object):
    def __init__(self, port, logger=None, name=None, debug=False, ipaddr="192.168.10.1", portno=1883, ctype="concentration", maxobs=100, haltonstop=False):
        self.logger = logger.getChild(suffix="Li78X0 " + str(name))
        self.port = port['obj']
        self.plock = port['lock']
        if name is not None:
            self.name = name
        else:
            self.name = "Licor78X0connobject"
        self.debug = debug
        self.ipaddr = ipaddr
        self.portno = portno
        if ctype == "concentration":
            self.subscription = "licor/niobrara/output/concentration"
        self.client = mqtt.Client()
        self.client.on_subscribe = self.on_subscribe
        self.client.on_message = self.on_message
        self.client.connect(self.ipaddr, self.portno)
        self.client.subscribe(self.subscription, qos = 1)
        self.maxobs = maxobs
        self.status = None
        self.keys = None
        self.values = dict()
        self.lastwarn = None
        self.lastmsg = None
        self.haltonstop = haltonstop
        self.vlock = threading.Lock()
        self.stoplock = threading.Lock()
        self.dostop = threading.Event()
        self.rthread = threading.Thread(target = self.client.loop_forever)
        if self.debug:
            self.logger.warning("Starting client loop")
        self.rthread.start()
        while self.keys is None:
            time.sleep(0.1)
        self.wthread = threading.Thread(target = self.watcher)
        self.wthread.start()

    def on_subscribe(self, client, userdata, mid, granted_qos):
        self.logger.info("Subscribed: "+str(mid)+" "+str(granted_qos))

    def on_message(self, client, userdata, msg):
        timestamp = int(time.time())
        msg = msg.payload
        if self.debug:
            if self.lastmsg is None or timestamp - self.lastmsg >= 10:
                self.logger.debug("received message" + str(msg))
                self.lastmsg = timestamp
#        else:
#            self.logger.debug("Li78X0 "+ str(self.name) + ": received message" + str(msg))
        self.dataprocess(msg, timestamp)

    def dataprocess(self, msg, timestamp):
        try:
            datain = eval(msg) ## Creates a dictionary
        except NameError:
            return
        datain = datain["Concentration"]
        if self.keys is None:
            self.keys = list(datain.keys())
        self.status = datain["DIAG"]
        if self.status < 9:
            while not self.vlock.acquire(timeout = 0.01):
                if self.dostop.is_set():
                    return
                pass
            self.values.update({timestamp: datain})
            if len(self.values) > self.maxobs:
                remkeys = list(self.values.keys())
                remkeys.sort()
                self.values.pop(remkeys[0])
            self.vlock.release()
        else:
            if self.lastwarn is None or timestamp - self.lastwarn >= 10:
                self.logger.debug("Unit not ready for measurements yet")
                self.lastwarn = timestamp

    def watcher(self):
        while not self.dostop.is_set():
            time.sleep(1)
        if self.haltonstop:
            self.client.publish("licor/niobrara/system/shutdown","shutdown")
        self.client.disconnect()
        self.rthread.join()


class reader(object):
    def __init__(self, connobject, dtype="latest", includedatetime=True, timestampcol="default", name=None, maxobs=20, sep=",", **kwargs):
        self.connobject = connobject['obj']
        self.port = self.connobject.port
        self.plock = self.connobject.plock
        self.vlock = self.connobject.vlock
        if name is None:
            self.name = self.connobject.name
        else:
            self.name = name
        self.logger = self.connobject.logger.getChild(suffix = str(self.name))
        self.sep = sep
        self.maxobs = maxobs
        if "nmean" in kwargs:
            self.nmean = int(kwargs["nmean"])
        else:
            self.nmean = 5
        if "interval" in kwargs:
            self.interval = kwargs["interval"]
        else:
            self.interval = self.nmean
        self.dostop = threading.Event()
        self.cstop = self.connobject.dostop
        self.cstoplock = self.connobject.stoplock
        self.rthread = threading.Thread(target=self.reader)
        self.debug = self.connobject.debug
        if timestampcol == 'default':
            self.timestampcol = str(self.name + "_timestamp")
        else:
            self.timestampcol = timestampcol
        if "outvariables" in kwargs:
            self.outvariables = kwargs["outvariables"].split(",")
        else:
            self.outvariables = self.connobject.keys
        if "invariables" in kwargs:
            self.invariables = kwargs["invariables"].split(",")
        else:
            self.invariables = self.outvariables
        if len(self.invariables) != len(self.outvariables):
            self.logger.critical("In- and outvariables need to have equal number of values!")
            raise ValueError("In- and outvariables need to be of same length!")
        self.variables = self.outvariables
        self.includetimestamp = False
        if includedatetime:
            self.includetimestamp = True
            if not self.timestampcol in self.variables:
                self.variables.append(self.timestampcol)
        if dtype == "latest":
            self.getval = self.getlatest
        if dtype == "nmean":
            self.getval = self.getmeanobs
        self.read = self.getval
        self.rthread.start()

    def reader(self):
        while True:
            if self.dostop.is_set():
                while not self.cstoplock.acquire(timeout=0.01) and not self.cstop.is_set():
                    pass
                if not self.cstop.is_set():
                    self.cstop.set()
                self.cstoplock.release()
                return
            time.sleep(1)

    def getlatest(self):
        if self.connobject.status > 8:
            return
        while not self.vlock.acquire(timeout=0.01):
            if self.dostop.is_set():
                return
        keys = list(self.connobject.values.keys())
        keys.sort()
        n = len(keys)
        outval = {j: self.connobject.values[keys[n-1]].get(k, None) for (j, k) in zip(self.outvariables, self.invariables)}
        timestamp = self.connobject.values[keys[n-1]].get("SECONDS")
        self.vlock.release()
        if self.includetimestamp:
            outval.update({self.timestampcol: timestamp})
        return outval

    def getmeanobs(self):
        """This doesn't work yet..."""
        return
        while not self.vlock.acquire(timeout=0.01):
            if self.dostop.is_set():
                return
        keys = list(self.values.keys())
        keys.sort()
        keys = keys[-self.nmean:]
        n = len(keys)
        outval = {a: self.values[a] for a in keys}
        self.vlock.release()
        timestamp = outval[keys[int(n/2)]][self.timestampcol]
        outval = pd.DataFrame(outval).transpose().mean().round(6)
        outval = outval.to_dict()
        timestamp = datetime.datetime.strptime(
            timestamp, '%m/%d/%Y %H:%M:%S.%f').strftime('%Y-%m-%d %T')
        outval.update({self.timestampcol: timestamp})
        return outval

    def stop(self):
        self.dostop.set()
        self.rthread.join()
        self.connobject.wthread.join()
