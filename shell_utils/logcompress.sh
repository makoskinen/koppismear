#!/bin/bash
# when used with logging-hasc.cfg, this compresses the timed rotated log files
LOGDIR=/mnt/datadrive/measlogs/hasc

cd $LOGDIR
find ./* -type f -regextype 'gnu-awk' -regex '.*\.log\.[0-9]{4}-[0-9]{2}-[0-9]{2}$' -exec gzip {} \;

exit 0
