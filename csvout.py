#!/usr/bin/python

import os
import time
import re


class base(object):
    def __init__(self, port):
        pass


class mediator(base):
    """Hand over faux port and real lock to the writer"""

    def __init__(self, port, logger=None):
        base.__init__(self, port['obj'])
        self.base = port['obj']
        self.lock = port['lock']
        self.logger = logger


class writer(base):
    """CSV writer Capable of writing character-delimited text files with
defined columns, checks columns when opening file and creates a new
one with a timestamp if necessary.

Capable of timed roll-over of files (configurable to seconds, minutes,
hours, days, months (= 30 days), years (= 365 days), counting either
from time of creation or previous midnight.

Capable of size-based rollover of files, configurable to B-GiB
accuracy; if file size limit is exceeded during a day and midnight is
true, give a true time stamp for the new file.

Saves epoch time as well as ISO 8006 time stamp in local time.

Parameters:
connobject: connection object (faux)
variables: string: variable names (other than timestamp, epochtime, Source),
    separated by separator defined in sep
outfile: string: basename for output file, including extension
interval: int: interval of writing one observation into the file, in seconds
tstampcol: string: name of time stamp column
average: int: average over how many observations
sizelim: string: limit for file size
sep: string: separator character
recsource: boolean: record the sample source in column "Source"
timelim: string: limit maximum age of file before rollover
    (for example 1 d = 1 day)
midnight: boolean: set file timestamp to previous midnight
checkinterval: int: time between checking file size, in seconds

    """

    def __init__(self, connobject, variables, outfile, interval, tstampcol,
                 average=1, sizelim="10 MB", sep=',', recsource=False,
                 timelim=False, midnight=False, datadir=".", checkinterval=3600):
        self.connobject = connobject['obj']
        self.lock = self.connobject.lock
        self.base = self.connobject.base
        self.logger = self.connobject.logger
        self.checkinterval = checkinterval
        self.sep = sep
        self.datadir = datadir
        self.variables = variables.split(self.sep)
        self.average = average
        self.tstampcol = tstampcol
        self.interval = interval
        self.sizelim = self.parsesize(sizelim)
        self.timelim = timelim
        if self.timelim:
            self.timelim = self.parsetime(timelim)
        self.recsource = recsource
        if self.recsource:
            self.extracols = [self.tstampcol, 'epochtime', 'Source']
        else:
            self.extracols = [self.tstampcol, 'epochtime']
        self.colrow = self.extracols + self.variables
        self.colrow = self.sep.join(self.colrow)  # Column row for new files
        self.ctime = time.time()
        self.midnight = midnight
        self.filecreator(outfile)  # Outfile is base name and
        # extension of data file

    def filecreator(self, filename, newfile=False, oversize=False):
        """ Looks for latest file that starts with the base file name;
if latest file is too large or old, create new file with timestamp.
Notice that . and _ are reserved characters in the name!"""
        mismatch = False
        filename_list = re.split(r'[_\.]', filename)
        if len(filename_list) == 2:  # No timestamp in filename
            filename_list.append(filename_list[1])
            filename_list[1] = None
        if not newfile:
            basename = filename_list[0]
            extension = filename_list[2]
            files = os.listdir(self.datadir)
            files = [f for f in files if re.split(r'[_\.]', f)[0] == basename and
                     f.endswith(extension)]
            if not files:
                # If no file by that name is found, create one
                # jump straight to file creation
                newfile = True
            else:
                # If files with that basename found, seek the latest and
                # check its size and if the first row holds the right columns
                files.sort()
                latestfile = files[len(files)-1]
                latestfilepath = os.path.join(self.datadir, latestfile)
                stats = os.stat(latestfilepath)
                self.outfile = open(latestfilepath, mode='r')
                colline = self.outfile.readline().strip('\n')
                filename_list = re.split(r'[_\.]', latestfile)
                if self.timelim:
                    newfile = self.timelimcheck(filename_list)
                self.outfile.close()
                if not newfile:
                    if stats.st_size >= self.sizelim:
                        self.logger.info('csvout: File size limit exceeded, starting \
new file')
                        newfile = True
                        oversize = True
                    elif not all(a in self.extracols + self.variables
                                 for a in colline.split(self.sep)):
                        self.logger.warning("csvout: Columns in file don't match \
variables! Starting new file")
                        newfile = True
                        mismatch = True
                    elif not all(b in colline.split(self.sep)
                                 for b in self.extracols + self.variables):
                        self.logger.warning("csvout: Variables don't match \
columns in file! Starting new file")
                        newfile = True
                        mismatch = True
        if newfile:
            if self.midnight and not oversize and not mismatch:
                filename_list[1] = time.strftime('%Y-%m-%d 00:00:00',
                                                 time.localtime())
            else:
                filename_list[1] = time.strftime('%Y-%m-%d %H:%M:%S',
                                                 time.localtime())
            filename = "{a[0]}_{a[1]}.{a[2]}".format(a=filename_list)
            filenamepath = os.path.join(self.datadir, filename)
            self.outfile = open(filenamepath, mode='w+')
            self.outfile.write(self.colrow + '\n')
            self.ctime = time.mktime(time.strptime(filename_list[1],
                                                   '%Y-%m-%d %H:%M:%S'))
            self.lastcheck = self.ctime
        else:
            self.outfile = open(latestfilepath, mode='a+')
        self.lastcheck = time.time()
        self.logger.info('csvout: Opened file %s for output of variables %s',
                         self.outfile.name, repr(self.variables))

    def filecheck(self):
        """Checks file size, creates new file with timestamp if necessary"""
        filename = self.outfile.name
        oversize = False
        filename_list = re.split(r'[_\.]', os.path.basename(filename))
        if len(filename_list) == 2:  # No timestamp in filename
            filename_list.append(filename_list[1])
            filename_list[1] = None
        stats = os.stat(filename)
        newfile = False
        if self.timelim:
            newfile = self.timelimcheck(filename_list)
        if not newfile:
            if stats.st_size >= self.sizelim:
                self.logger.info('csvout: File size limit exceeded, starting \
new file')
                oversize = True
                newfile = True
        if newfile:
            self.outfile.close()
            self.filecreator(os.path.basename(filename), newfile=True,
                             oversize=oversize)
        else:
            self.lastcheck = time.time()

    def timelimcheck(self, filename_list):
        """Check file creation time from name or first row; signal to create
new file if too old"""
        if filename_list[1] is None:
            line1 = self.outfile.readline()
            if not line1 == '':
                # Datetime is first column
                ctime = line1.strip('\n').split(self.sep)[0]
                if self.midnight:
                    self.ctime = time.mktime(time.strptime(ctime.split(' ')[0],
                                                           '%Y-%m-%d'))
                else:
                    self.ctime = time.mktime(time.strptime(ctime,
                                                           '%Y-%m-%d %H:%M:%S'))
            else:
                # File without lines -> take current time
                self.ctime = time.time()
        else:
            if self.midnight:
                self.ctime = time.mktime(time.strptime(filename_list[1].split(' ')[0],
                                                       '%Y-%m-%d'))
            else:
                self.ctime = time.mktime(time.strptime(filename_list[1],
                                                       '%Y-%m-%d %H:%M:%S'))
        if time.time() - self.ctime >= self.timelim:
            self.logger.info('csvout: File age limit exceeded, starting \
            new file')
            return True
        return False

    @staticmethod
    def parsesize(size):
        """Parse kb, mb et al into numbers"""
        units = {"B": 1, "KB": 10**3, "MB": 10**6, "GB": 10**9, "TB": 10**12}
        number, unit = [string.strip() for string in size.split()]
        return int(float(number) * units[unit])

    @staticmethod
    def parsetime(intime):
        """Parse S into seconds, M into minutes, H into hours, d into days, w
into weeks, m into months, Y into years"""
        units = {"S": 1, "M": 60, "H": 3600, "d": 3600*24, "w": 7*3600*24,
                 "m": 30*3600*24, "Y": 365*3600*24}
        number, unit = [string.strip() for string in intime.split()]
        return(int(float(number) * units[unit]))

    def writerow(self, vals):
        """Write data row ('vals') to output file after adding extra columns
such as timestamps and source if applicable"""
        outrow = list()
        for col in self.extracols + self.variables:
            try:
                if vals[col] is None:
                    vals[col] = 'NA'
                outrow.append(vals[col])
            except KeyError:
                outrow.append('NA')
                self.logger.debug('csvout: variable {0} not found in row at \
                {1}'.format(col, time.strftime('%H:%M', time.localtime())))
        if all(k == 'NA' for k in outrow):
            self.logger.debug('csvout: Empty row!')
        unknown = [a for a in list(vals.keys()) if a not in [self.tstampcol, 'epochtime', 'Source']
                   + self.variables]
        if len(unknown) > 0:
            self.logger.debug('csvout: unknown variables in input: \
            {0}'.format(' '.join(unknown)))
        with self.lock:
            self.logger.debug('csvout: writing %s',
                              self.sep.join(map(str, outrow)))
            self.outfile.write(self.sep.join(map(str, outrow)) + '\n')
            self.outfile.flush()
            self.logger.debug('csvout: wrote')
        now = time.time()
        if ((now - self.lastcheck >= self.checkinterval) or
                (now - self.ctime >= self.timelim)):
            # Check for file size once every checkinterval seconds
            self.filecheck()

    def write(self, row):
        """Write method for datamanager"""
        self.writerow(row)
