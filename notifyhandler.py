#!/usr/bin/python

import logging
from notify_run import Notify


class NotifyHandler(logging.StreamHandler):
    def __init__(self):
        logging.StreamHandler.__init__(self)
        self.notify = Notify()

    def emit(self, record):
        msg = self.format(record)
        self.notify.send(msg)
