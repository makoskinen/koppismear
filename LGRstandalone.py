#!/usr/bin/python
"""LGR - interface for the LGR analysers, receives data over serial
port as it comes
"""
import datetime
import threading

import time

import pandas as pd


class LGRread(object):
    def __init__(self, port, logger=None, name=None, dtype="latest", debug=False):
        self.logger = logger
        self.port = port['obj']
        self.plock = port['lock']
        self.vlock = threading.Lock()
        self.name = name
        self.values = dict()
        self.maxobs = 20
        self.nmean = 5
        self.tmean = 5
        self.stop = threading.Event()
        self.rthread = threading.Thread(target=self.reader,
                                        args=(self.values, ))
        self.debug = debug
        self.getkeys('LGRkeys.csv')
        self.timestampcol = "LGRTimestamp"
        self.skipcols = ["C30", "C31", "C32"]
        if dtype == "latest":
            self.getval = self.getlatest
        if dtype == "nmean":
            self.getval = self.getmeanobs
        self.rthread.start()

    def getkeys(self, filename):
        with open(filename, "r") as f:
            self.keys = f.readline().strip().split(',')

    def tryconv(self, val):
        try:
            return int(val)
        except ValueError:
            pass
        try:
            return float(val)
        except ValueError:
            return(val)

    def reader(self, values):
        self.port.flushInput()
        discard = self.port.read_until('\n')
        if self.debug:
            print((repr(discard)))
        while True:
            if self.stop.is_set():
                break
            timestamp = int(time.time())
            with self.plock:
                datain = self.port.read_until('\n')
            if self.debug:
                print(repr(datain))
            datain = datain.strip().split(',')
            datain = [self.tryconv(a.strip(' ')) for a in datain]
            self.addobs(timestamp, datain)

    def addobs(self, timestamp, data):
        with self.vlock:
            if len(self.values) >= self.maxobs:
                if self.debug:
                    print("too many observations, removing oldest")
                keys = list(self.values.keys())
                keys.sort()
                self.values.pop(keys[0])
            outvals = dict(list(zip(self.keys, data)))
            for s in self.skipcols:
                outvals.pop(s)
            if self.debug:
                print(repr(outvals))
            self.values.update({timestamp: outvals})

    def getlatest(self, ctime):
        with self.vlock:
            keys = list(self.values.keys())
            keys.sort()
            n = len(keys)
            outval = self.values[keys[n-1]]
        return outval

    def getmeanobs(self, ctime):
        with self.vlock:
            keys = list(self.values.keys())
            keys.sort()
            keys = keys[-self.nmean:]
            n = len(keys)
            outval = {a: self.values[a] for a in keys}
        timestamp = outval[keys[int(n/2)]][self.timestampcol]
        outval = pd.DataFrame(outval).transpose().mean().round(6)
        outval = outval.to_dict()
        outval.update({self.timestampcol: timestamp})
        return outval
