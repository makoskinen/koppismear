#!/usr/bin/python3

"""Starter script for koppismear using the HASC soil chamber configuration files"""

import koppi_alternate as koppi
import time
import threading
import os
import sys
import socket_server
import argparse

argParser = argparse.ArgumentParser()
argParser.add_argument("-i", "--inifile", help="name of inifile", default="main_hasc.ini")  ## Change the default .ini-file to whatever is correct
args = argParser.parse_args()

gc = koppismear(args.inifile, forcestart=True)
if gc.cfg['general']['startserver']:
    stopper = threading.Event()
    print("Initializing socket server")
    ss = socket_server.koppiServer(gc=gc, host=gc.cfg['general']['hostaddr'], port=65432, stopper=stopper)
    ssthread = threading.Thread(target=ss.run)
    ssthread.start()
    print("Started socket server")
    time.sleep(5)
    print("Starting measurements!")
    gc.go()
    print("Started!")
    while True:
        if ss.doquit.is_set():
            try:
                if ssthread.is_alive():
                    ssthread.join()
                sys.exit(0)
            except SystemExit:
                os._exit(0)
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            stopper.set()
            gc.stop()
            gc.quitnow()
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
