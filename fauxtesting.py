#!/usr/bin/python

from ReadIni import ReadIni
import threading
import multiprocessing
import main
import mbed
import faux
import timer
import sourcemanager

ufaux = main.koppismear('voctest.ini')

running = multiprocessing.Event()

fauxmgr = sourcemanager.manager('sources_fauxtest.ini', ufaux.controls)
fauxtimer = timer.timer('timer_faux.ini', running, fauxmgr)

