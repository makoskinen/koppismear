#!/usr/bin/python3

class logger(object):
    def __init__(self, level=1):
        self.level = level

    def debug(self, m):
        if self.level >= 4:
            print(m)

    def info(self, m):
        if self.level >= 3:
            print(m)

    def warning(self, m):
        if self.level >= 2:
            print(m)

    def critical(self, m):
        if self.level >= 1:
            print(m)
