#!/usr/bin/python

import time
import datetime
import threading

from itertools import cycle
from ReadIni import ReadIni
from time import sleep


class timer(object):
    def __init__(self, cfg, running, controlmanager, currSource,
                 currSourcelock, messenger=None, logger=None,
                 writelog=True, mainproc=True):
        try:
            self.ttype = cfg['timer']['type']
            self.cfg = cfg
        except TypeError:
            self.cfg = ReadIni(cfg)
            self.ttype = self.cfg['timer']['type']
        try:
            self.name = self.cfg['timer']['name']
        except KeyError:
            self.name = 'Unnamed {} timer'.format(self.ttype)
        self.currSource = currSource
        self.currSourcelock = currSourcelock
        self.delayTimer = 0,
        self.running = running
        self.mainproc = mainproc
        # threading.Event object from the main process
        # to control running and pausing
        self.writelog = writelog
        self.controlmanager = controlmanager
        self.logger = logger
        self.dostop = threading.Event()
        self.messenger = messenger
        # going to be a direct messaging service or something
        # to inform via push notifications about completed runs et cetera

    def prime(self):
        self.dostop.clear()
        self.log = open(self.cfg['timer']['logfile'], mode='a+')
        self.timerobject = getattr(self, self.ttype)(self.cfg,
                                                     sendmsg=self.sendmsg,
                                                     running=self.running,
                                                     manager=self.controlmanager,
                                                     currSource=self.currSource,
                                                     currSourcelock=self.currSourcelock,
                                                     dostop=self.dostop,
                                                     log=self.log,
                                                     logger=self.logger)
        self.tolog = self.timerobject.tolog
        self.t1 = threading.Thread(target=self.timerobject.run)
        self.t1.start()
        self.wthread = threading.Thread(target=self.watcher,
                                        kwargs=dict(thread=self.t1))
        self.wthread.start()
        if self.mainproc:
            self.setdefsource()

    def start(self):
        if self.mainproc:
            self.logger.warning('Run %s starting', self.name)
        else:
            self.logger.info('Run %s starting', self.name)
        self.running.set()

    def pause(self):
        if self.mainproc:
            self.running.clear()
        self.setdefsource()

    def cont(self):
        if self.mainproc:
            self.running.set()

    def restart(self):
        self.stop()
        self.prime()
        self.start()

    def setdefsource(self):
        (source, delay, recchange) = self.controlmanager.setSource('default')
        if recchange:
            with self.currSourcelock:
                self.currSource = source

    def stop(self):
        if not self.mainproc:
            self.dostop.set()
            self.logger.info('Run %s over', self.name)
            self.t1.join()
            self.setdefsource()
            return
        if self.running.is_set():
            self.running.clear()
        else:
            return
        if self.dostop.is_set():
            return
        if not self.ttype == 'periodical':
            self.setdefsource()
        self.dostop.set()
        self.logger.warning('Run %s over', self.name)
        try:
            self.log.flush()
            self.log.close()
        except AttributeError:
            self.logger.debug('controltimer: Log already closed')
        self.t1.join()

    def sendmsg(self, msg):
        if self.writelog:
            self.logger.info(msg)
        if self.messenger is not None:
            try:
                self.messenger.send(msg)
            except ConnectionError:
                if not self.writelog:
                    self.logger.info(msg)
        else:
            pass

    def watcher(self, thread):
        while thread.is_alive():
            sleep(2)
        self.stop()
        return

    class basictimer(object):
        """Base object for the timers - do not run as itself"""

        def __init__(self, cfg, running, manager, dostop, log,
                     sendmsg, currSource, currSourcelock,
                     messenger=None, logger=None):
            self.cfg = cfg['timer']
            self.cfg_all = cfg
            self.name = self.cfg['name']
            self.sendmsg = sendmsg
            self.messenger = messenger
            self.log = log
            self.logger = logger
            self.dostop = dostop
            self.createsrclist()
            self.source = str()
            self.source_to_record = str()
            self.nexttime = float()
            self.status = 'waiting'
            self.running = running
            self.controlmanager = manager
            self.currSource = currSource
            self.currSourcelock = currSourcelock
            self.now = time.time()
            self.changerecd = False
            self.lastchange = time.time()
            self.time = float()
            self.recordchange = False
            self.delayTimer = 0
            self.initialize()

        def initialize(self):
            pass

        def tolog(self, msg):
            """Write given message to the log file"""
            self.log.write('%s: %s\n' % (time.strftime('%Y-%m-%d %H:%M:%S',
                                                       time.localtime()), msg))
            self.log.flush()

        def createsrclist(self):
            """Placeholder for the function defined in subclasses"""
            pass

    class cycling(basictimer):
        """Timer that cycles the sources again and again until stopped"""

        def __init__(self, cfg, running, manager, dostop, log,
                     sendmsg, currSource, currSourcelock, messenger=None,
                     logger=None):
            super(timer.cycling, self).__init__(cfg, running, manager,
                                                dostop, log,
                                                sendmsg,
                                                currSource,
                                                currSourcelock,
                                                messenger,
                                                logger)

        def createsrclist(self):
            self.sources = cycle(self.cfg['order'].split(','))
            try:
                times = self.cfg['times'].split(',')
            except AttributeError:
                times = [self.cfg['times']]
            if len(times) == 1:
                self.times = cycle([times[0]]
                                   * len(self.cfg['order'].split(',')))
                # If same period for all, one entry is enough
            else:
                self.times = cycle([int(i) for i in
                                    self.cfg['times'].split(',')])
                if (len(self.cfg['times'].split(',')) != len(self.cfg['order'].split(','))):
                    raise ValueError('Number of entries in sources order \
and times do not match!')

        def __next__(self):
            """Select next source on the list"""
            self.source = next(self.sources)
            self.time = next(self.times)
            (nextsource, delayTimer,
             recordchange) = self.controlmanager.setSource(self.source)
            if recordchange:
                (self.delayTimer, self.recordchange, self.changerecd,
                 self.source_to_record, self.lastchange) = (delayTimer,
                                                            recordchange,
                                                            False,
                                                            self.source,
                                                            time.time())
            self.nexttime = self.now + self.time
            self.logger.info('Source set to %s, delay %s' % (self.source,
                                                             self.delayTimer))

        def run(self):
            """Run the source changes and watch for signal to pause or stop"""
            while True:
                self.status = 'waiting'
                while not self.running.is_set():
                    sleep(1)
                    if self.dostop.is_set():
                        return
                while self.running.is_set():
                    self.status = 'running'
                    self.now = round(time.time())
                    next(self)
                    while self.now < self.nexttime:
                        sleep(0.2)
                        if ((self.now >= self.lastchange + self.delayTimer) and self.recordchange and not self.changerecd):
                            with self.currSourcelock:
                                self.currSource[0] = self.source_to_record
                            self.changerecd = True
                            self.logger.debug(
                                "cycling: source changed to %s!", self.currSource[0])
                        if self.dostop.is_set() or not self.running.is_set():
                            break
                        self.now = round(time.time())

    class runthrough(basictimer):
        """Timer that runs through a list of sources and stops"""

        def __init__(self, cfg, running, manager, dostop, log,
                     sendmsg, currSource, currSourcelock,
                     messenger=None, logger=None):
            super(timer.runthrough, self).__init__(cfg, running, manager,
                                                   dostop, log,
                                                   sendmsg, currSource,
                                                   currSourcelock,
                                                   messenger=None,
                                                   logger=logger)

        def createsrclist(self):
            repeat = 1
            if 'repeat' in self.cfg:
                repeat = int(self.cfg['repeat'])
            self.sources = self.cfg['order'].split(',')
            self.sources = self.sources * repeat
            if len(self.cfg['times'].split(',')) == 1:
                self.times = [int(self.cfg['times'])] * len(self.sources)
                # If same period for all, one entry is enough
            else:
                self.times = [int(i) for i in self.cfg['times'].split(',')]
                self.times = self.times * repeat
            if len(self.sources) != len(self.times):
                if len(self.sources) % len(self.times) != 0:
                    raise ValueError(
                        'Number of entries in sources order and times do not match!')
                else:
                    self.times = self.times * \
                        (len(self.sources) / len(self.times))
                    print('Times recycled to match number of steps')

        def run(self):
            """Run the source changes and watch for signal to pause or stop"""
            self.status = 'waiting'
            whatnext = 'continue'
            while True:
                while not self.running.is_set():
                    sleep(1)
                    if self.dostop.is_set():
                        return
                if self.dostop.is_set() or whatnext == 'finish':
                    return
                while self.running.is_set():
                    self.status = 'running'
                    self.now = round(time.time())
                    whatnext = next(self)
                    while self.now < self.nexttime or whatnext == 'finish':
                        sleep(0.2)
                        if ((self.now >= self.lastchange + self.delayTimer) and self.recordchange and not self.changerecd):
                            with self.currSourcelock:
                                self.currSource[0] = self.source_to_record
                            self.changerecd = True
                            self.logger.debug("runthrough: source\
 changed to %s!", self.currSource[0])
                        if not self.running.is_set():
                            break
                        if self.dostop.is_set() or whatnext == 'finish':
                            return
                        self.now = round(time.time())

        def __next__(self):
            """Select next source"""
            try:
                self.source = self.sources.pop(0)
                self.time = self.times.pop(0)
                val = 'continue'
            except IndexError:
                self.sendmsg('No more sources in {}'.format(self.name))
                self.source = 'default'
                self.time = 0
                val = 'finish'
            (nextsource, delayTimer,
             recordchange) = self.controlmanager.setSource(self.source)
            if recordchange:
                (self.delayTimer, self.recordchange, self.changerecd,
                 self.source_to_record, self.lastchange) = (delayTimer,
                                                            recordchange,
                                                            False,
                                                            nextsource,
                                                            time.time())
            self.nexttime = self.now + self.time
            self.logger.debug('controltimer: next: Source set to %s, delay %s',
                              self.source, self.delayTimer)
            return val

    class periodical(runthrough, basictimer):
        def __init__(self, cfg, running, manager, dostop, log,
                     sendmsg, currSource, currSourcelock,
                     messenger=None, logger=None):
            super(timer.periodical, self).__init__(cfg, running, manager,
                                                   dostop, log,
                                                   sendmsg, currSource,
                                                   currSourcelock,
                                                   messenger=None,
                                                   logger=logger)

        def createsrclist(self):
            pass

        def initialize(self):
            self.timers = self.cfg_all['timer']['timers'].split(',')
            self.cfgs = dict([(k, self.cfg_all[k]) for k in self.timers])
            self.times = dict([(k, self.cfgs[k]['runtimes'])
                               for k in self.cfgs])
            for k in self.times:
                self.times[k] = self.create_times(self.times[k])
            self.current = None

        def run(self):
            currtimer = None
            while True:
                now = datetime.datetime.now()
                while now.second != 0:
                    sleep(1)
#                    self.logger.debug('periodical timer: counting')
                    now = datetime.datetime.now()
                    if self.dostop.is_set():
                        if currtimer is not None:
                            currtimer.stop()
                        return
                if not self.running.is_set():
                    continue
                if self.current is None:
                    self.current = [k for k in self.times if(
                        now.weekday() in self.times[k][0] and
                        now.hour in self.times[k][1] and
                        now.minute in self.times[k][2]
                    )
                    ]
                    if self.current == []:
                        self.logger.debug('periodical timer: nothing to do')
                        self.current = None
                        sleep(1)
                        continue
                    if len(self.current) > 1:
                        self.logger.warning('periodical timer: no more than one run at a time!')
                        continue
                    self.logger.debug(
                        'periodical timer: looking for cfg %s' % self.current[0])
                    cfg = dict([('timer', self.cfgs[self.current[0]])])
                    currtimer = timer(cfg,
                                      self.running,
                                      self.controlmanager,
                                      self.currSource,
                                      self.currSourcelock,
                                      self.messenger,
                                      self.logger,
                                      mainproc=False
                                      )
                    currtimer.prime()
                    currtimer.start()
                else:
                    if currtimer is not None:
                        if not currtimer.wthread.is_alive():
                            self.current = None

        def create_times(self, times):
            times = times.split(";")
            if len(times) != 3:
                self.logger.warning('periodical timer: times need to be in format\
days-of-week[0-6, *];hours-of-day[0-23, *];minutes-of-hours[0-59, *]')
                raise ValueError
            days = times[0]
            hours = times[1]
            minutes = times[2]
            if "*" in days:  # Make * the wildcard
                daystmp = list(range(0, 7))
                if "/" in days:
                    div = int(days.split('/')[1])
                    days = [i for i in daystmp if i % div == 0]
                else:
                    days = daystmp
            else:
                # Otherwise discreet values are separated by comma
                days = days.split(",")
                daystemp = []
                for t in days:
                    try:
                        daystemp.append(int(t))
                    except ValueError:
                        try:
                            # And ranges are defined by hyphen
                            t = t.split("-")
                            t = list(range(int(t[0]), int(t[1])+1))
                            for i in t:
                                daystemp.append(i)
                        except ValueError:
                            raise  # Otherwise there is something wrong
                days = daystemp
            try:
                hours = [int(hours)]
            except ValueError:
                if "*" in hours:
                    hourstmp = list(range(0, 24))
                    if "/" in hours:
                        div = int(hours.split('/')[1])
                        hours = [i for i in hourstmp if i % div == 0]
                    else:
                        hours = hourstmp
                else:
                    hourstemp = []
                    if "," in hours:
                        hours = hours.split(",")
                        for t in hours:
                            if "-" in t:
                                t = t.split("-")
                                t = list(range(int(t[0]), int(t[1]+1)))
                                for i in t:
                                    hourstemp.append(i)
                            else:
                                hourstemp.append(int(t))
                    else:
                        if "-" in hours:
                            t = hours.split("-")
                            t = list(range(int(t[0]), int(t[1])+1))
                            for i in t:
                                hourstemp.append(i)
                    hours = hourstemp
            if "*" in minutes:
                self.logger.debug('timer: * in minutes')
                minutestmp = list(range(0, 60))
                if "/" in minutes:
                    div = int(minutes.split('/')[1])
                    self.logger.debug('timer: splitting minutes with %s' % div)
                    minutes = [i for i in minutestmp if i % div == 0]
                else:
                    minutes = minutestmp
            else:
                minutes = minutes.split(",")
                minstemp = []
                for t in minutes:
                    try:
                        minstemp.append(int(t))
                    except ValueError:
                        try:
                            t = t.split("-")
                            t = list(range(int(t[0]), int(t[1])+1))
                            for i in t:
                                minstemp.append(i)
                        except ValueError:
                            raise ValueError()
                minutes = minstemp
            return((days, hours, minutes))
