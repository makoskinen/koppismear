#!/usr/bin/env python3

import kivy
import time
# import koppi_alternate as koppi

import socket
import selectors
import types
import threading
import queue
import math

# from multiprocessing import Pipe

import pickle

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
# from kivy.uix.checkbox import CheckBox
from kivy.clock import Clock
from graph import Graph, MeshLinePlot
# from functools import partial

HOSTADDR = "192.168.4.1"
HOSTPORT = 65432

class NiceScale:
    def __init__(self, minv,maxv):
        self.maxTicks = 6
        self.tickSpacing = 0
        self.lst = 10
        self.niceMin = 0
        self.niceMax = 0
        self.minPoint = minv
        self.maxPoint = maxv
        try:
            self.calculate()
        except (ValueError, ZeroDivisionError, TypeError):
            self.niceMin = 0
            self.niceMax = 1
            self.tickSpacing = 0.1

    def calculate(self):
        self.lst = self.niceNum(self.maxPoint - self.minPoint, False)
        self.tickSpacing = self.niceNum(self.lst / (max([self.maxTicks - 1, 1])), True)
        self.niceMin = math.floor(self.minPoint / self.tickSpacing) * self.tickSpacing
        self.niceMax = math.ceil(self.maxPoint / self.tickSpacing) * self.tickSpacing

    def niceNum(self, lst, rround):
        self.lst = lst
        exponent = 0 # exponent of range */
        fraction = 0 # fractional part of range */
        niceFraction = 0 # nice, rounded fraction */

        exponent = math.floor(math.log10(self.lst))
        fraction = self.lst / math.pow(10, exponent)
        if fraction is None:
            fraction = 1 ## Another bloody hack
        if (self.lst):
            if (fraction < 1.5):
                niceFraction = 1
            elif (fraction < 3):
                niceFraction = 2
            elif (fraction < 7):
                niceFraction = 5;
            else:
                niceFraction = 10;
        else :
            if (fraction <= 1):
                niceFraction = 1
            elif (fraction <= 2):
                niceFraction = 2
            elif (fraction <= 5):
                niceFraction = 5
            else:
                niceFraction = 10
        return niceFraction * math.pow(10, exponent)

    def setMinMaxPoints(self, minPoint, maxPoint):
          self.minPoint = minPoint
          self.maxPoint = maxPoint
          self.calculate()

    def setMaxTicks(self, maxTicks):
        self.maxTicks = maxTicks;
        self.calculate()


class pollqueue(queue.Queue):
    def __init__(self, **kwargs):
        super(pollqueue, self).__init__(**kwargs)
        self.recv = self.get
        self.send = self.put

    def poll(self, timeout=None):
        if timeout is None:
            return not self.empty()
        t0 = time.time()
        while time.time() < t0 + timeout:
            if not self.empty():
                return True
            else:
                time.sleep(0.01)
        else:
            return False

class varDrop(DropDown):
    def __init__(self, variables, **kwargs):
        super(varDrop, self).__init__(**kwargs)
##        print("Creating var dropdown with variables %s" % variables)
        self.variables = variables
##        self.menu = DropDown()
        for index in range(len(self.variables)):
##            print("Creating option for %s" % self.variables[index])
            btn = Button(text=str(self.variables[index]), size_hint_y = None, height = 50)
            btn.bind(on_release=lambda btn: self.select(btn.text))
            self.add_widget(btn)


class varBtn(Button):
    def __init__(self, variable, cmdp, datap, graph, timelim=600, **kwargs):
        super(varBtn, self).__init__(**kwargs)
        self.variable = variable
        self.cmdp = cmdp
        self.datap = datap
        self.graph = graph
        self.plot = MeshLinePlot(color=[1, 0, 0, 1])
        self.graph.add_plot(self.plot)
        self.timelim = timelim
        self.data = list()
        self.time = round(time.time())

    def update(self, datarunning):
        print("Trying to update for button %s" %str(self))
        running = datarunning == 'True'
        if not running:
            print("Data not running")
            if self.variable == None:
                print("No variable")
                return
            else:
                self.text = str(self.variable)
            return
        if self.variable == None:
            print("No variable for button %s!"%str(self))
            return
        to_append = self.getdata(0)
        print("Update appending", repr(to_append))
        if to_append is None or len(to_append) == 0:
            print("Updated with no data")
            return
        elif not all((type(s) == int or type(s) == float) for s in to_append[0]):
            print("Data not numeric!")
            return
        else:
            self.data.append((to_append[0][0], to_append[0][1]))
        print("Data appended")
        self.data = [i for i in self.data if i[0] > (self.time - self.timelim)]
        self.plot.points = [(x, y) for x, y in self.data]
        # print(str(len(self.data)), "points appended")
        if(max([x[0] for x in self.data]) > (self.graph.xmax - int(self.timelim / 10))):
            # print("Updating x limits")
            self.graph.xmin = self.graph.xmin + int(self.timelim / 10)
            self.graph.xmax = self.graph.xmax + int(self.timelim / 10)
        if len(self.data) > 1 :
            try:
                self.nicelims = NiceScale(min([y[1] for y in self.data]), max([y[1] for y in self.data]))
            except TypeError:
                self.nicelims = NiceScale(0, 1)
            # print("Calculated lims")
            self.graph.ymin = self.nicelims.minPoint
            self.graph.ymax = self.nicelims.maxPoint
            if self.graph.ymin == self.graph.ymax:
                self.graph.ymax = self.graph.ymin + (0.5*(self.graph.ymin)) ## Hack..
            self.graph.y_ticks_major = self.nicelims.tickSpacing
            self.graph.y_ticks_minor = 0
        self.graph.ylabel = str(self.variable)
        try:
            self.plot.draw()
        except ZeroDivisionError:
            pass
        self.text = str(self.variable) + str('\n') + str(self.data[len(self.data) - 1][1])
        # print("1.7")
        # print("1.8")

    def getdata(self, dt):
        out = ('get', self.variable, dt)
        print("Putting", out, "to cmdp")
        self.cmdp.send(out)
        if self.datap.poll(2):
            data = self.datap.recv()
            print("Received", data, "from datap")
            return [(a, b) for a, b in data[self.variable].items()]
        else:
            return None

    def getrunning(self):
        out = ('running',)
        print("Putting", out, "to cmdp")
        self.cmdp.send(out)
        if self.datap.poll(2):
            data = self.datap.recv()
            print("Received", data, "from datap")
            return data
        else:
            return None

    def set_variable(self, variable):
##        print("Setting variable for %s" % self)
        self.variable = str(variable)
        data = self.getdata(self.timelim)
        print("set_variable got", repr(data))
        data =  [(s[0], s[1]) for s in data if all(type(r) == int or type(r) == float for r in s)] ## Make sure everything is numeric
        self.data = data


class initPopup(Popup):
    def __init__(self, mode, host, port, **kwargs):
        super(initPopup, self).__init__(**kwargs)
        self.host = host
        self.port = port
        self.mode = mode
        self.content = BoxLayout(orientation='horizontal')
        self.modebtn1 = ToggleButton(text='manual', state='down', group='mode')
        self.modebtn2 = ToggleButton(text='auto', group='mode')
        self.content.add_widget(self.modebtn1)
        self.content.add_widget(self.modebtn2)
        self.hostinput = TextInput(hint_text='Host IP address\n default %s' % HOSTADDR, multiline=False)
        self.portinput = TextInput(hint_text='Host port\n default %s' % HOSTPORT, multiline=False)
        self.submitbtn = Button(text='Submit')
        self.submitbtn.bind(on_release=self.dism)
        self.content.add_widget(self.hostinput)
        self.content.add_widget(self.portinput)
        self.content.add_widget(self.submitbtn)

    def dism(self, *kargs):
        try:
            self.host = [HOSTADDR if str(self.hostinput.text) == "" else str(self.hostinput.text)][0]
            self.port = [HOSTPORT if self.portinput.text == "" else int(self.portinput.text)][0]
            if self.modebtn1.state == 'down':
                self.mode = 'manual'
            elif self.modebtn2.state == 'down':
                self.mode = 'auto'
            self.dismiss()
        except Exception as e:
            self.host = None
            self.port = None
            self.mode = None

class koppiLayout(GridLayout):
    def __init__(self, host, port, mode = 'auto', datakeeptime = 600, **kwargs):
        super(koppiLayout, self).__init__(**kwargs)
        self.sel = selectors.DefaultSelector()
        self.cols = 1
        self.mode = mode
        self.host = host
        self.port = port
        self.dostart = threading.Event()
        self.datakeeptime = datakeeptime
        self.stopper = threading.Event()
        if self.mode is None or self.port is None or self.host is None:
            while self.mode is None or self.port is None or self.host is None:
                try:
                    self.getinit = initPopup(self.mode, self.host, self.port)
                    self.getinit.bind(on_dismiss= lambda *_: self.startconn(self.getinit.mode, self.getinit.host, self.getinit.port)) 
                    self.getinit.open()
                except ValueError:
                    self.getinit.close()
                    continue
                else:
                    break
        else:
            self.getinit.close()
            self.startconn(self.mode, self.host, self.port)

    def startconn(self, mode, host, port):
        self.mode = mode
        self.host = host
        self.port = port
        self.sconn = socketClient(host, port, self.stopper)
        self.dataq = self.sconn.outq
        self.msgq = self.sconn.inq
        self.scthread = threading.Thread(target = self.sconn.run)
        self.scthread.start()
        self.on_start()


    def on_start(self):
        self.clear_widgets()
        self.lastchange = int(time.time())
        self.send('SETTIME.%s' % time.time())
        self.selbar = BoxLayout(orientation='horizontal')
        self.nextlastbar = BoxLayout(orientation='horizontal')
        self.infobar = BoxLayout(orientation='horizontal')
        self.graphbar = BoxLayout(orientation='horizontal')
        self.add_widget(self.selbar)
        self.add_widget(self.nextlastbar)
        self.add_widget(self.infobar)
        self.add_widget(self.graphbar)
        self.sourcelist = self.getsources()
        print(repr(self.sourcelist))
        self.sourcelist = self.sourcelist.split(",")
        self.sourcemenu = DropDown()
        for index in range(len(self.sourcelist)):
            btn = Button(text = self.sourcelist[index], size_hint_y = None, height = 50)
            btn.bind(on_release=lambda btn: self.sourcemenu.select(btn.text))
            self.sourcemenu.add_widget(btn)
        self.varlist = self.getvars().split(",")
        print(repr(self.varlist))
        self.cmdp1, self.datap1 = pollqueue(), pollqueue()
        print(repr(self.cmdp1))
        print(repr(self.datap1))
        self.varmenu1 = varDrop(self.varlist)
        self.graph1 = Graph(xlabel='time', ylabel='none', x_ticks_minor=5, x_ticks_major=25, y_ticks_minor=5, y_ticks_major=25, x_grid_label=True, y_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=int(time.time()), xmax=int(time.time())+self.datakeeptime, ymin=0, ymax=1000)
        self.varbutton1 = varBtn(text = "Variable", variable = None, cmdp = self.cmdp1, datap = self.datap1, graph=self.graph1, timelim=self.datakeeptime, size_hint = (0.17, None))
        self.varbutton1.bind(on_release = self.varmenu1.open)
        self.varmenu1.bind(on_select = lambda instace, x: self.varbutton1.set_variable(str(x)))
        self.infobar.add_widget(self.varbutton1)
        self.cmdp2, self.datap2 = pollqueue(), pollqueue()
        self.varmenu2 = varDrop(self.varlist)
        self.graph2 = Graph(xlabel='time', ylabel='none', x_ticks_minor=5, x_ticks_major=25, y_ticks_minor=5, y_ticks_major=25, x_grid_label=True, y_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=int(time.time()), xmax=int(time.time())+self.datakeeptime, ymin=0, ymax=1000)
        self.varbutton2 = varBtn(text="Variable", variable=None, cmdp = self.cmdp2, datap = self.datap2, graph=self.graph2, timelim=self.datakeeptime, size_hint=(0.17, None))
        self.varbutton2.bind(on_release = self.varmenu2.open)
        self.varmenu2.bind(on_select = lambda instace, x: self.varbutton2.set_variable(str(x)))
        self.infobar.add_widget(self.varbutton2)
        self.varbuttons = [self.varbutton1, self.varbutton2]
        self.graphbar.add_widget(self.graph1)
        self.graphbar.add_widget(self.graph2)
        self.sourcebutton = Button(text = "Source", size_hint = (0.2, None))
        self.sourcebutton.bind(on_release = self.sourcemenu.open)
        self.sourcemenu.bind(on_select = lambda instance, x: self.source_set(instance, x))
        self.selbar.add_widget(self.sourcebutton)
        self.nxtsourcebutton = Button(text = "Next source", size_hint = (0.2, None))
        self.nxtsourcebutton.bind(on_release=self.nxtsrc)
        self.lstsourcebutton = Button(text = "Prev source", size_hint = (0.2, None))
        self.lstsourcebutton.bind(on_release=self.lstsrc)
        self.nextlastbar.add_widget(self.lstsourcebutton)
        self.nextlastbar.add_widget(self.nxtsourcebutton)
        self.gobutton = Button(text="Go", size_hint=(0.2, None))
        self.gobutton.bind(on_release= self.go)
        if self.mode == 'manual':
            self.gobutton.disabled = True
            self.gobutton.text = '<manual>'
        self.pausebutton = ToggleButton(text="pause", size_hint=(0.2, None))
        self.pausebutton.bind(on_release=self.pausetoggle)
        if self.mode == 'manual':
            self.pausebutton.disabled = True
        self.datagobutton = ToggleButton(text="Record data", size_hint=(0.2, None))
        self.datagobutton.bind(on_release=self.datatoggle)
        self.selbar.add_widget(self.datagobutton)
        self.selbar.add_widget(self.gobutton)
        self.selbar.add_widget(self.pausebutton)
        self.sourcelabel = Label(size_hint=(0.2, None))
        self.timelabel = Label(size_hint=(0.3, None))
        self.infobar.add_widget(self.sourcelabel)
        self.infobar.add_widget(self.timelabel)
        self.exitbutton = Button(text="Close", size_hint=(0.2, None))
        self.exitbutton.bind(on_release=self.exitprogram)
        self.selbar.add_widget(self.exitbutton)
        self.quitbutton = Button(text="Quit&Exit", size_hint=(0.2, None))
        self.quitbutton.bind(on_release=self.quitexitprogram)
        self.selbar.add_widget(self.quitbutton)
        self.pipes = [(self.cmdp1, self.datap1), (self.cmdp2, self.datap2)]
        self.servthread = threading.Thread(target = self.server)
        self.servthread.start()
        self.dostart.set()

    def nxtsrc(self, instance):
        currsourceidx = self.sourcelist.index(self.currsource)
        try:
            nextsource = self.sourcelist[currsourceidx + 1]
        except IndexError:
            nextsource = self.sourcelist[0]
        self.source_set(instance, nextsource)

    def lstsrc(self, instance):
        currsourceidx = self.sourcelist.index(self.currsource)
        nextsource = self.sourcelist[currsourceidx - 1]
        self.source_set(instance, nextsource)

    def send(self, msg):
        out = msg + '\n\r'
        out = pickle.dumps(out)
        self.msgq.put(msg)
        try:
            ret = self.dataq.get(1, timeout = 5)
        except queue.Empty:
            ret = None
        return ret

    def datatoggle(self, instance):
        if instance.state == 'normal':
            self.pauseall()
        elif instance.state == 'down':
            self.datago()

    def getvars(self):
        ret = self.send('GETVARIABLES')
        return ret

    def pause(self):
        ret = self.send('PAUSERUN')

    def getrunning(self):
        ret = self.send('GETRUNNING')
        return ret

    def getdatarunning(self):
        ret = self.send('GETDATARUNNING')
        return ret

    def pauseall(self):
        ret = self.send('PAUSE')

    def cont(self):
        ret = self.send('CONT')

    def datago(self):
        ret = self.send('DATAGO')

    def getdata(self, variables, dt):
        print("Koppilayout.getdata asking for", repr(variables))
        out = 'GETDATA.' + ','.join([variables]) + '.' + str(dt)
        ret = self.send(out)
        return ret

    def getsources(self):
        ret = self.send('GETSOURCES')
        return ret

    def getcurrsource(self):
        ret = self.send('GETCURRSOURCE')
        return ret

    def setsource(self, source):
        out = 'SETSOURCE.' + str(source)
        ret = self.send(out)

    def go(self, instance):
        ret = self.send('GO')
        if ret == "OK":
            self.state_run(True)

    def stop(self):
        ret = self.send('STOP')

    def quitnow(self):
        ret = self.send('QUIT')

    def pausetoggle(self, instance):
        if instance.state == 'down':
            self.pause()
            self.state_run(False)
        else:
            if instance.state == 'normal':
                self.cont()
                self.state_run(True)

    def state_run(self, state):
        if state:
            self.sourcebutton.disabled = True
            self.datagobutton.disabled = True
            self.gobutton.text = 'Running'
        else:
            self.sourcebutton.disabled = False
            self.datagobutton.disabled = False
            if self.mode == 'auto':
                self.gobutton.text = 'Go'
            elif self.mode == 'manual':
                self.gobutton.text = '<manual>'

    def source_set(self, instance, source):
        self.setsource(str(source))
        self.lastchange = int(time.time())
        setattr(self.sourcebutton, 'text', source)
        datarunning = self.getdatarunning()
        if datarunning == 'False':
            self.datago()
            self.datagobutton.state = 'down'

    def variable_set(self, instance, btn, variable):
##        print("Setting variable for %s!" % btn)
        setattr(btn, 'variable', str(variable))
##        print("Variable set to" % getattr(btn, 'variable'))

    def update(self, dt):
        if not self.dostart.is_set():
            return
        print("Updating..")
        try:
            prevsource = self.currsource
        except AttributeError:
            prevsource = None
        self.currsource = self.getcurrsource().strip("[]'")
        if self.currsource != prevsource:
            self.lastchange = int(time.time())
        print("Update: current source", repr(self.currsource))
        running = self.getrunning()
        if running == 'True' and not self.sourcebutton.disabled:
            self.state_run(True)
        elif running == 'False' and self.sourcebutton.disabled:
            self.state_run(False)
        if running == 'True' and self.mode == 'manual':
            self.pause()
            self.state_run(False)
        datarunning = self.getdatarunning()
        if datarunning == 'True':
            self.datagobutton.state = 'down'
        self.sourcedt = int(time.time()) - self.lastchange
        self.sourcelabel.text = str(self.currsource) + ' ' + str(self.sourcedt)
        print(2)
        print(len(self.varbuttons))
        for i in range(len(self.varbuttons)):
            print(i)
            try:
                self.varbuttons[i].update(datarunning = datarunning)
            except ZeroDivisionError:
                pass
        currtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        self.timelabel.text = str(currtime)

    def server(self):
        print("Sock interface server starting")
        while not self.stopper.is_set():
            while all([not p[0].poll() for p in self.pipes]) and not self.stopper.is_set():
                time.sleep(0.1)
            for p in self.pipes:
                if p[0].poll():
                    cmd = p[0].recv()
                    print("server Received", repr(cmd))
                    if cmd[0] == 'get':
                        ret = self.getdata(cmd[1], cmd[2])
                    elif cmd[0] == 'running':
                        ret = self.getrunning()
                    print("server Sending", repr(ret))
                    p[1].send(ret)
        print("Sock interface server exiting")
        return


    def exitprogram(self, instance):
##        print("GC stopping..")
##        self.stop()
##        print("GC stopped")
##        self.quitnow()
        self.stopper.set()
        self.scthread.join()
        self.servthread.join()
        App.get_running_app().stop()
        Window.close()
##        print("GC quitted")
##        App.stop(self)
##        self.root_window.close()

    def quitexitprogram(self, instance):
        print("GC stopping..")
        self.stop()
        print("GC stopped")
        self.quitnow()
        print("GC quit")
        self.stopper.set()
        print("stopper set")
        self.servthread.join()
        print("servthread joined")
        self.scthread.join()
        print("scthread joined")
        App.get_running_app().stop()
        Window.close()
##        print("GC quitted")
##        App.stop(self)
##        self.root_window.close()



class koppiAppi(App):
    def __init__(self, host, port, mode='auto', interval = 2, inifile = 'faux.ini', **kwargs):
        super(koppiAppi, self).__init__(**kwargs)
        self.host = host
        self.port = port
        self.interval = interval
        self.mode=mode
    
    def build(self):
        ourLayout = koppiLayout(self.host, self.port, mode=self.mode)
        Clock.schedule_interval(ourLayout.update, int(self.interval))
        return ourLayout

    def on_stop(self):
        return


class socketClient():
    def __init__(self, host, port, stopper):
        self.stopper = stopper
        self.outq = queue.Queue()
        self.inq = queue.Queue()
        server_addr = (host, port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##        startsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##        print("Sending command to start program")
##        startsock.connect((host, 9999))
##        print("Connected to server port 9999")
##        startsock.send('start'.encode('utf-8'))
##        print("Command sent, sleeping 10s")
##        time.sleep(10)
##        print("Closing start socket")
##        startsock.close()
        self.sock.setblocking(False)
##        self.sock.settimeout(0.2)
        self.sock.connect_ex(server_addr)

    def service_connection(self, msg):
        while True:
            try:
                print("service_connection trying to send")
                self.sock.send(msg.encode('utf-8'))
                break
            except BlockingIOError:
                continue
            except OSError:
                print("service_connection: OSError sending data!")
                self.outq.put(None)
                return
        time.sleep(0.1)
        tries = 0
        recv_data = False
        while True & tries <= 10:
            print("service_connection trying to read..")
            if self.stopper.is_set():
                return
            try:
                recv_data = self.sock.recv(1024)
            except (BlockingIOError, socket.timeout):
                tries += 1
                print("No data")
                time.sleep(0.5)
                continue
            except OSError:
                print("Server closed connection")
                ## recv_data = False
                self.stopper.set()
                return
            else:
                break
        if recv_data:
            print("service_connection received initial data, reading more..")
            while True:
                if self.stopper.is_set():
                    try:
                        self.sock.close()
                    except:
                        pass
                    return
                try:
                    recv_data = recv_data + self.sock.recv(1024)
                except BlockingIOError:
                    break
                except socket.timeout:
                    break
            print("service_connection finished reading")
            recv_data = pickle.loads(recv_data)
            print("service_connection putting received data to outq")
            self.outq.put(recv_data)
        if not recv_data:
            print("service_connection closing socket")
            self.sock.close()

    def run(self):
        while not self.stopper.is_set():
            if self.inq.empty():
                time.sleep(0.1)
                continue
            try:
                to_send = self.inq.get(timeout = 1)
            except queue.Empty:
                continue
            print(to_send)
            self.service_connection(to_send)
        print("Stopper set, returning")
        return

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--host", help="Address of server koppismear is running on", type=str, default=None)
    parser.add_argument("-p", "--port", help="Port of server", type=int, default=None)
    parser.add_argument("-m", "--mode", help="Type of interface, auto or manual", type=str, default="manual")
    parser.add_argument("-i", "--interval", help="Update interval, seconds", type=int, default=2)
    args = parser.parse_args()
#     print(args)
    Appi = koppiAppi(host=args.host, port=args.port, mode=args.mode, interval=args.interval)
    Appi.run()
