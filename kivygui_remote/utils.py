#!/usr/bin/python
"""Utility functions for chamber system"""

import time
import math
import datetime
from time import sleep


class NiceScale:
    def __init__(self, minv,maxv):
        self.maxTicks = 6
        self.tickSpacing = 0
        self.lst = 10
        self.niceMin = 0
        self.niceMax = 0
        self.minPoint = minv
        self.maxPoint = maxv
        try:
            self.calculate()
        except ValueError:
            pass

    def calculate(self):
        self.lst = self.niceNum(self.maxPoint - self.minPoint, False)
        self.tickSpacing = self.niceNum(self.lst / (self.maxTicks - 1), True)
        self.niceMin = math.floor(self.minPoint / self.tickSpacing) * self.tickSpacing
        self.niceMax = math.ceil(self.maxPoint / self.tickSpacing) * self.tickSpacing

    def niceNum(self, lst, rround):
        self.lst = lst
        exponent = 0 # exponent of range */
        fraction = 0 # fractional part of range */
        niceFraction = 0 # nice, rounded fraction */

        exponent = math.floor(math.log10(self.lst));
        fraction = self.lst / math.pow(10, exponent);

        if (self.lst):
            if (fraction < 1.5):
                niceFraction = 1
            elif (fraction < 3):
                niceFraction = 2
            elif (fraction < 7):
                niceFraction = 5;
            else:
                niceFraction = 10;
        else :
            if (fraction <= 1):
                niceFraction = 1
            elif (fraction <= 2):
                niceFraction = 2
            elif (fraction <= 5):
                niceFraction = 5
            else:
                niceFraction = 10

        return niceFraction * math.pow(10, exponent)

    def setMinMaxPoints(self, minPoint, maxPoint):
          self.minPoint = minPoint
          self.maxPoint = maxPoint
          self.calculate()

    def setMaxTicks(self, maxTicks):
        self.maxTicks = maxTicks;
        self.calculate()


class ChamberVolume(object):
    """Calculate chamber volume based on gas addition
1. set trace flow to 0
2. record start time
3. set trace flow to preset value
4. ???
5. PROFIT"""
    def __init__(self, cfg, controls, outvars, latest, logger=None):
        self.logger = logger
        self.control = controls[cfg['cname']]['obj']  # MFC object
        self.var = outvars[cfg['trace']]  # Variable = parameter name of
        # trace
        self.latest = latest[cfg['trace']]
        self.T = outvars[cfg['Tvar']]  # name of temperature variable
        self.flowtime = cfg['flowtime']  # period to flow gas for (seconds)
        self.flow = cfg["flow"]  # flow setting of MFC (% max)
        self.vdelay = cfg["vdelay"]  # delay of air flow from chamber to
        # analyzer (seconds)
        self.gconc = cfg["gconc"]  # concentration of trace in gas, ppm
        if "vwa" in cfg:
            # "a" and "b" parameters of trace for the Van der Waals equation
            self.vwa = cfg["vwa"]
            self.vwb = cfg["vwb"]
        else:
            self.vwa = None
            self.vwb = None

    def go(self):
        self.control.setstate(0)  # set flow to 0
        self.control.zerototal()  # Zero the totalizer on the MFC
        self.starttime = time.time()
        self.control.setstate(self.flow)
        while True:
            pass

    def vdwn(self, vol):
        """ Calculate n of trace based on van der Waals equation:
n = """
        pass


class HouseKeeping(object):
    def __init__(self, outvars, latest, dostop):
        self.outvars = outvars
        self.latest = latest
        self.lastdone = None
        self.dostop = dostop

    def clean_outvars(self):
        today = datetime.datetime.today().date()
        if self.lastdone is None or self.lastdone < self.today:
            for k in self.outvars:
                self.outvars[k] = dict([(s, self.latest[k][s])
                                        for s in self.latest[k]])
            self.lastdone = today

    def run(self):
        now = time.time()
        while not self.dostop.is_set():
            sleep(1)
            if time.time() - now >= 240:
                self.clean_outvars_now()
                now = time.time()

    def clean_outvars_now(self):
        for k in self.outvars:
            self.outvars[k] = dict([(s, self.outvars[k][s])
                                    for s in self.latest[k].lst])
