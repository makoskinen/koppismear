#!/usr/bin/python

"""Datamanager

Includes manager class for data handling (data sources and data
sinks). Reads data from sources and writes them into sinks, according
to which variables are meant to go where in main.ini

Class timer cycles through the sources and sinks and
reads from sources and writes to sinks according to their intervals

"""

import time
import threading
from prettytable import PrettyTable

from time import sleep


class manager(object):
    """Creates the sources and sinks.

Parameters:
sinks: dictionary created by createSinks in koppi
datasources: dictionary created by createSources in koppi
outvars: variable dictionary created by koppi
latest: list of latest observations per input created by koppi
lock: threading.Lock object for outvars
tstampcol: name for time stamp column (in local time)
currSource: list whose sole element has the name of current sample source
currSourcelock: lock for writing and reading the currSource variable
logger: logger object created by koppi
"""

    def __init__(self, sinks, datasources, outvars, latest,
                 lock, latestlock, tstampcol, currSource=None,
                 currSourcelock=None, logger=None):
        self.logger = logger
        self.sinks = [self.sinkwriter(sink=sinks[si]['obj'],
                                      outvars=outvars, latest=latest,
                                      lock=lock,
                                      latestlock=latestlock,
                                      tstampcol=tstampcol, name=si,
                                      logger=self.logger,
                                      currSource=currSource,
                                      currSourcelock=currSourcelock,
                                      recsource=sinks[si]['obj'].recsource,
                                      outofsync=threading.Event())
                      for si in list(sinks.keys())]
        self.datasources = [self.datasourcereader(datasource=datasources[so]['obj'],
                                                  outvars=outvars,
                                                  latest=latest,
                                                  lock=lock, latestlock=latestlock, name=so,
                                                  logger=self.logger)
                            for so in list(datasources.keys())]

    def getSinks(self, obj=False):
        if obj:
            return self.sinks
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.sinks:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)
        return None

    def getSources(self, asobj=False, asdict=False):
        if asobj:
            return self.datasources
        if asdict:
            return dict([(a, a.variables) for a in self.datasources])
        pt = PrettyTable(['name', 'variables', 'interval'])
        for a in self.datasources:
            try:
                variables = a.variables
            except AttributeError:
                variables = None
            pt.add_row([a.name, variables, a.interval])
        print(pt)

    class datasourcereader:
        """Reads a datasource and writes its variables into the outvars dictionary.
Reading interval and variables are defined in the datasource.

Locking for communications ports is handled by the datasources internally

Parameters:
datasource: data source object
outvars: dictionary for observations of variables
latest: list for times of latest observations of variables
lock: threading.Lock object for outvars
name: string: name of the datasource
logger: logger object
"""

        def __init__(self, datasource, outvars, latest, lock, latestlock, name, logger):
            self.name = name
            self.datasource = datasource
            self.logger = logger
            self.dostop = threading.Event()
            self.interval = int(self.datasource.interval)
            self.nextread = round(time.time()) + self.interval
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict to keep count of when variables have last been recored
            self.lock = lock
            # for locking the outvars dict to prevent race conditions
            self.latestlock = latestlock
            self.variables = self.datasource.variables
            self.now = time.time()
            self.row = False
            self.joined = False

        def read(self, now=False):
            """ Reads data from the source of the datasourcereader instance"""
            self.joined = False
            if not now:
                self.now = round(time.time())
            else:
                self.now = now
            self.nextread = round(self.now) + self.interval
            try:
                self.logger.debug('%s reading at %s' % (self.name, self.now))
                self.row = self.datasource.read()
                self.logger.debug('%s read %s at %s' %
                                  (self.name, self.row, self.now))
            except Exception as e:
                self.row = dict((s, 'NA') for s in self.variables)
                self.logger.debug('reader %s: %s', self.name, e.__name__)
            # source.read must return dict of values for variables
            self.collect()
            self.logger.debug('%s: next reading at %s' %
                              (self.name, self.nextread))
            # When to read the next time

        def collect(self):
            """Output the read data into the outvars dict element with the current
timestamp of the datasourcereader instance"""
            while self.lock.locked() and not self.dostop.is_set():
                sleep(0.01)
            if self.dostop.is_set():
                return
            with self.lock:
                for s in self.variables:
                    try:
                        self.logger.debug(
                            '%s collect: writing %s at %s' % (self.name, s, self.now))
                        self.outvars[s][self.now] = self.row[s]
                        while self.latestlock.locked():
                            sleep(0.01)
                        with self.latestlock:
                            self.latest[s].put(self.now)
                    except KeyError:
                        self.logger.debug(
                            '%s collect: %s not found at %s' % (self.name, s, self.now))
                        self.outvars[s][self.now] = None

        def stop(self):
            self.datasource.stop()
            self.dostop.set()
            return

    class sinkwriter:
        """Reads variables from the outvars dictionary and writes them to a
data sink.  Capable of outputting averaged values over n observations,
defined in the data sink.

Parameters:
sink: data sink object
outvars: the outvars dictionary
latest: the list of latest observations per input
lock: threading.Lock for outvars
latestlock: threading.Lock for latest
tstampcol: name for time stamp column
name: name of the sink
logger: logger object
currSource: list containing the current sample source
currSourcelock: lock for the currSource list
recsource: boolean: record the current source in the sink
outofsync: threading.Event to signal that the data sources are out of sync and need to be synchronized

        """

        def __init__(self, sink, outvars, latest, lock, latestlock, tstampcol, name,
                     logger, currSource, currSourcelock, recsource, outofsync):
            self.name = name
            self.sink = sink
            self.logger = logger
            self.currSource = currSource
            self.currSourcelock = currSourcelock
            self.tstampcol = tstampcol
            self.recsource = recsource
            self.outofsync = outofsync
            self.dostop = threading.Event()
            self.interval = int(self.sink.interval)
            try:
                self.average = int(self.sink.average)
            except AttributeError:
                self.average = 1
            try:
                self.vwindow = int(self.sink.vwindow)
            except AttributeError:
                self.vwindow = 1
            self.nextwrite = round(time.time()) + \
                (self.interval * self.average)
            self.outvars = outvars
            # Dict object that has all variables that are available for input
            self.latest = latest
            # Dict object that has times of latest records of all variables
            self.lock = lock
            # for locking the outvars dict to prevent race condition
            self.latestlock = latestlock
            self.variables = sink.variables
            self.row = dict()
            self.lastwrite = None
            self.sinknow = round(time.time())
            self.joined = False

        def write(self, now=False):  # Write data into the sink
            if not now:
                self.lastwrite = round(time.time())
            else:
                self.lastwrite = round(now)
            self.joined = False
            self.collect()
            self.logger.debug('%s writing at %s' % (self.name, self.lastwrite))
            self.sink.write(self.row)
            self.logger.debug('%s wrote' % self.name)
            # sink.write must accept dict object with var names and time stamp
            self.row = None
            self.nextwrite = int(self.lastwrite) + \
                (self.interval * self.average)

        def collect(self):  # Collect the data to write from the outvars
            complete = False
            c = 0
            while c < 18 and not complete:
                while self.lock.locked():
                    sleep(0.01)
                with self.lock:
                    if self.average > 1:
                        complete = self.averager()
                    # elif self.vwindow > 1:
                    #     self.window()
                    else:
                        complete = self.unival()
                if not complete:
                    c += 1
                    sleep(self.interval/10)
            if not complete:
                self.unival(force=True)
                self.logger.debug(
                    'writecollect: Possible missing variables at %s' % self.sinknow)
            self.logger.debug(
                'writecollect: adding time and source at %s' % self.sinknow)
            self.addtime()
            self.addsource()

        def averager(self):  # For sending out averaged values
            try:
                if all([self.testlatest(p) for p in range(self.average)]):
                    self.row = dict()
                    while self.latestlock.locker():
                        sleep(0.01)
                    self.latestlock.acquire()
                    gettimes = self.latest[self.variables[0]].get()[
                        0:self.average]
                    self.latestlock.release()
                    self.sinknow = sum(gettimes) / self.average
                    # Record timestamp for write step (mean of
                    # time stamps for averaging sinks)
                    for s in self.variables:
                        self.row[s] = []
                        for m in gettimes:
                            self.row[s].append(self.outvars[s][m])
                            #                        print self.row[s]
                        self.row[s] = sum(self.row[s]) / self.average
                    return True
            except IndexError:
                return False
            return False

        def unival(self, force=False):  # For sending out single values
            try:
                if self.testlatest(0) or force:
                    self.row = dict()
                    while self.latestlock.locked():
                        sleep(0.01)
                    self.latestlock.acquire()
                    if force:
                        print([self.latest[i].get()[0]
                               for i in self.variables])
                        gettimes = min([self.latest[i].get()[0]
                                        for i in self.variables])
                    else:
                        gettimes = max([self.latest[i].get()[0]
                                        for i in self.variables])
                    self.latestlock.release()
                    self.sinknow = int(gettimes)
                    if force:
                        self.logger.debug(
                            "unival: used force at %s" % self.sinknow)
                    for s in self.variables:
                        try:
                            self.row[s] = self.outvars[s][gettimes]
                        except KeyError:
                            self.row[s] = None
                    return True
            except IndexError:
                return False
            return False

        # def window(self):  # For sending out time windows of single values
        #     """ Don't use this, there's no reason for it! """
        #     if all([self.testlatest(p) for p in range(self.vwindow)]):
        #         self.row = dict()
        #         gettimes = self.latest[self.variables[0]].get()[self.vwindow]
        #         self.sinknow = gettimes
        #         for s in self.variables:
        #             self.row[s] = [self.outvars[s][t] for t in gettimes]
        #     else:
        #         raise ValueError('Not all variables have same time stamp!')

        def addtime(self):
            """Add timestamp here to not necessitate naming
               the timestamp column in each sink"""
            self.row[self.tstampcol] = time.strftime('%Y-%m-%d %H:%M:%S',
                                                     time.localtime(self.sinknow))
            self.row['epochtime'] = self.sinknow

        def addsource(self):
            if self.recsource:
                self.logger.debug('datamanager: Adding source')
                with self.currSourcelock:
                    self.row['Source'] = self.currSource[0]

        def testlatest(self, n):
            """Test whether all the sources have the same timestamp for their
latest observation"""
            while self.latestlock.locked():
                sleep(0.01)
            self.latestlock.acquire()
            testl = iter({k: self.latest[k].get()[n] for k in self.variables
                          if k not in
                          [self.tstampcol, 'epochtime', 'Source']}.values())
            self.latestlock.release()
            first = next(testl)
            if int(first) == self.sinknow:
                return False
            diffs = [first - item for item in testl]
            nsync = [i % self.interval for i in diffs]
            if not all(s == 0 for s in nsync):
                self.logger.debug('datanamager: %s out of sync' % self.name)
                self.outofsync.set()
            return all(item == 0 for item in diffs)


class timer(object):
    """Timer for data reading and writing. Performs threaded reads and
writes to speed up the operation.

Parameters:
datamanager: datamanager.manager object
running: threading.Event: are we running?
logger: logger object
sleeptime: float: time to sleep between checking if something should be done,
    seconds
"""

    def __init__(self, datamanager, running, logger=None, sleeptime=0.05):
        self.datamanager = datamanager
        self.running = running
        self.status = 'starting'
        self.sleeptime = sleeptime
        self.dostop = threading.Event()
        self.now = time.time()
        self.logger = logger
        self.rthreads = dict([(so.name, None)
                              for so in self.datamanager.datasources])
        self.wthreads = dict([(si.name, None)
                              for si in self.datamanager.sinks])

    def run(self):
        while True:
            self.status = 'waiting'
            while not self.running.is_set():
                if self.dostop.is_set():
                    for so in self.datamanager.datasources:
                        so.stop()
                    return
                sleep(1)
            while self.running.is_set():
                self.status = 'running'
                self.now = time.time()
                # self.act()
                if self.dostop.is_set():
                    for so in self.datamanager.datasources:
                        so.stop()
                    return
                self.act_th()  # Try the threading interface
                sleep(self.sleeptime)

    def act_th(self):
        #        self.logger.debug('datatimer: acting')
        for so in self.datamanager.datasources:
            if self.rthreads[so.name] is not None and not so.joined:
                if not self.rthreads[so.name].is_alive():
                    self.logger.debug('Joining %s', so.name)
                    so.joined = True
            if self.now >= so.nextread:
                self.rthreads[so.name] = threading.Thread(
                    target=so.read, args=(round(self.now), ))
                self.logger.debug('Starting %s', so.name)
                self.rthreads[so.name].start()
        for si in self.datamanager.sinks:
            if self.wthreads[si.name] is not None:
                if not self.wthreads[si.name].is_alive() and not si.joined:
                    self.wthreads[si.name].join()
                    si.joined = True
            if self.now >= si.nextwrite:
                if si.outofsync.is_set():
                    sources = self.datamanager.getSources(asdict=True)
                    tosync = []
                    for v in si.variables:
                        tosync = tosync + \
                            [so for so in self.datamanager.datasources if v in so.variables]
                    tosync = list(set(tosync))
                    synctime = round(self.now) + si.interval
                    self.logger.debug('Syncing %s with time %s' %
                                      (tosync, synctime))
                    for so in tosync:
                        so.nextread = synctime
                    si.nextwrite = round(self.now) + si.interval
                    si.outofsync.clear()
                    continue
                try:
                    if self.wthreads[si.name] is not None:
                        if not self.wthreads[si.name].is_alive():
                            self.wthreads[si.name] = threading.Thread(
                                target=si.write)
                            self.wthreads[si.name].start()
                            self.logger.debug('Started %s', si.name)
                    else:
                        self.wthreads[si.name] = threading.Thread(
                            target=si.write)
                        self.wthreads[si.name].start()
                        self.logger.debug('Started %s', si.name)
                except IndexError:  # Not enough observations for write yet
                    self.logger.debug('datatimer %s: Not enough \
observations for write' % si.name)
                    si.nextwrite = round(time.time()) + \
                        (si.interval * si.average)
                except ValueError as e:
                    self.logger.info('datatimer: %s', e.message)

    def stop(self):
        self.dostop.set()
