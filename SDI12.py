#!/usr/bin/python

from time import sleep

import time

import re

class SDI12con(object):
    """Connobject representing a SDI12 bus. 
port is port object, 
logger is a logger, 
name is an identifier for the bus, 
sleeptime is the time to sleep after sending command before waiting for reply in seconds (float), 
debug is boolean, whether to print debug information to the terminal, 
maxtries is int: how many times to try to read data before returning None 
"""
    def __init__(self, port, logger=None, name=None, sleeptime=0.4, debug=False, maxtries=10):
        self.port = port['obj']
        self.lock = port['lock']
        self.name = name
        self.debug = debug
        self.logger = logger.getChild(suffix = "SDI12_" + self.name)
        self.maxtries = maxtries
        self.availableIDs = list()
        self.availableDevs = dict()
        self.sleeptime = sleeptime
        sleep(2) ## Time for the serial port to settle
        self._getAvailableIds()
        self._getAvailableDevs()

    def send(self, msg, address=None, stripaddress=True, reply=True):
        if address:
            sendaddress = str(address)
        else:
            sendaddress = ""
        msg = sendaddress + msg + "!" + "\r\n"
        if self.debug:
            print(msg)
        while not self.lock.acquire(timeout = 0.1):
            pass
        self.port.flushInput()
        self.port.write(msg.encode())
        if reply:
            sleep(self.sleeptime)
            rep = self.recv()
            self.lock.release()
            if address:
                adtest = rep[0].partition(str(address))
                if not adtest[1] == str(address):
                    raise ValueError("Address mismatch!")
                if stripaddress:
                    rep = [adtest[2]]
            return rep
        self.lock.release()

    def recv(self):
        rep = list()
        tries = 0
        while self.port.inWaiting() == 0 and tries < self.maxtries:
            if self.debug:
                print("port inwaiting: " + str(self.port.inWaiting()))
            tries += 1
            sleep(self.sleeptime)
        if self.port.inWaiting() == 0:
            return None
        while self.port.inWaiting() > 0:
            if self.debug:
                print("Receiving msg")
            rep.append(self.port.read_until(b"\r\n"))
        rep = [r.strip(b"\r\n").decode() for r in rep]
        return rep

    def _getAvailableIds(self):
        rep=self.send("?", stripaddress=False)
        [self.availableIDs.append(r) for r in rep if not r in self.availableIDs]

    def _getAvailableDevs(self):
        for i in self.availableIDs:
            self.availableDevs.update({i: self._getInfo(i)})

    def _getInfo(self, address):
        rep = self.send("I", address=address, stripaddress=False)[0]
        out = dict()
        if self.debug:
            print(rep)
        out.update({"address": rep[0]})
        out.update({"SDI12comp": "".join([rep[s] for s in range(1, 3)])})
        out.update({"Manuf": "".join([rep[s] for s in range(3, 11)])})
        out.update({"Model": "".join([rep[s] for s in range(11, 17)])})
        out.update({"ver": "".join([rep[s] for s in range(17, 20)])})
        return out

class SDI12reader(SDI12con):
    """Class for reading a single device over SDI12 bus. 
Connobject is the connection object representing the bus,
address is the bus id of the device, 
name is an identifier for the device, 
interval is the measurement interval (seconds), 
variables is a list of variable names that the device produces, 
boolean includedatetime is whether to include time stamp of when the request for data was made in the data returned by the read function,
datetimecolname is the name for the datetime stamp column,
optional list varindices is a list of indices of the named variables in the data produced by the device"""
    def __init__(self, connobject, address, interval, variables, varindices=None, name=None, includedatetime=False, datetimecolname = "SDI12datetime", includedt=False, dtcolname="SDI12dt"):
        self.connobject = connobject["obj"]
        self.includeDatetime = includedatetime
        self.datetimecolname = datetimecolname
        self.address = address
        self.interval = interval
        self.variables = variables.split(",")
        if self.includeDatetime and not self.datetimecolname in self.variables:
            self.variables.append(self.datetimecolname)
        self.includedt = includedt
        self.dtcolname = dtcolname
        if self.includedt:
            self.prevmeas = 0
            if not self.dtcolname in self.variables:
                self.variables.append(self.dtcolname)
        self.internalvars = [s for s in self.variables if not s in [datetimecolname, dtcolname]]
        self.info = self.connobject.availableDevs[str(self.address)]
        if name is None:
            self.name = "_".join([self.info["Model"], self.info["address"]])
        else:
            self.name = name
        self.logger = self.connobject.logger.getChild(suffix = self.name)
        if varindices:
            self.varindices = varindices
            try:
                self.varindices = re.split("[,;]", self.varindices)
            except TypeError:
                pass
            if len(self.varindices) != len(self.internalvars):
                raise IndexError("%s: variables and varindices need to be of same length!" % self.name)
        else:
            self.varindices = range(len(self.internalvars))
        if self.includedt:
            self.read(zeroOnly=True)

    def getvals(self, register = 0):
        ini = self.connobject.send("M" + str(register), address = str(self.address), stripaddress = True)[0]
        twait = int(ini[0:3])
        nvals = int(ini[3])
        sleep(twait)
        i = 0
        rvals = list()
        while len(rvals) < nvals:
            vals = self.connobject.send("D" + str(i), address = str(self.address), stripaddress=False)[0]
            if vals is None:
                self.logger.warning("No reply from device!")
            if len(vals) == 1:
                raise IndexError("%s: Less values received than sensor declared!")
            vals = self.parsevals(vals)
            [rvals.append(s) for s in vals]
            i+=1
        return rvals

    def parsevals(self, vals):
        vals = re.split(r"([+\-])", vals)
        if self.connobject.debug:
            print(repr(vals))
        raddress = vals.pop(0)
        valrange = range(0, len(vals), 2)
        vals = [float("".join(vals[s:s+2])) for s in valrange]
        return(vals)

    def read(self, zeroOnly=False):
        timestamp = round(time.time())
        reg = 0
        vals = []
        while len(vals) < len(self.varindices):
            try:
                [vals.append(s) for s in self.getvals(reg)]
            except (ValueError,IndexError) as e:
                self.logger.exception(e)
                vals = [None] * len(self.varindices)
            reg += 1
        out = dict((k, v) for k, v in [(self.internalvars[i], vals[j]) for i, j in zip(self.varindices, range(len(self.varindices)))])
        if self.includeDatetime:
            out.update({self.datetimecolname: timestamp})
        if self.includedt:
            out.update({self.dtcolname: timestamp - self.prevmeas})
            self.prevmeas = timestamp
        if zeroOnly:
            return
        return(out)

    def stop(self):
        pass

class OTTPluvio2s(SDI12reader):
    def __init__(self, connobject, address, interval, variables, name=None, varindices=None, includedatetime=False, datetimecolname="Pluvio2sDatetime", includedt=True, dtcolname="Pluvio2sdt", Tunit=0, Iunit=1, Prate=0, Pfac=1, Heatermode=3, HeaterTaT="+2", HeaterLoT=-30, HeaterOnTime=20, HeaterStartTime="14:00:00", HeaterSelfTestInterval=60):
        super().__init__(connobject, address, interval, variables, varindices, name, includedatetime, datetimecolname, includedt, dtcolname)
        self.Tunit = Tunit
        self.Iunit = Iunit
        self.Prate = Prate
        self.Pfac = Pfac
        self.Heatermode = Heatermode
        self.HeaterTaT = HeaterTaT
        self.HeaterLoT = HeaterLoT
        self.HeaterOnTime = HeaterOnTime
        self.HeaterStartTime = HeaterStartTime
        self.HeaterSelfTestInterval = HeaterSelfTestInterval
        self.properties = {"Firmwarever": {"target" : "", "value": None, "cmd": "OOV"},
                           "Tunit": {"target" : str(Tunit), "value" : None, "cmd" : "OUT"},
                           "Iunit": {"target" : str(Iunit), "value" : None, "cmd" : "OUI"},
                           "Prate": {"target" : str(Prate), "value" : None, "cmd" : "OCI"},
                           "Pfac": {"target" : str(Pfac), "value" : None,  "cmd" : "OSI"},
                           "Heatermode": {"target" : str(Heatermode), "value" : None, "cmd" : "OCH"},
                           "HeaterTaT": {"target" : str(HeaterTaT), "value" : None, "cmd" : "OCHS"},
                           "HeaterLoT": {"target" : str(HeaterLoT), "value": None, "cmd": "OCHG"},
                           "HeaterOnTime": {"target": str(HeaterOnTime), "value": None, "cmd": "OCHD"},
                           "HeaterStartTime": {"target": str(HeaterStartTime), "value": None, "cmd": "OCHZ"},
                           "HeaterSelfTestInterval": {"target": str(HeaterSelfTestInterval), "value": None, "cmd": "OCHT"}
                           }
        self.readProps()

    def readProps(self, prop="all"):
        if prop == "all":
            prop = self.properties.keys()
        else:
            prop = [prop]
        [self.gsProp(s) for s in prop]

    def setProps(self, prop="all"):
        if prop == "all":
            prop = self.properties.keys()
        else:
            prop = [prop]
        for s in prop:
            if not self.properties[s]["target"] == self.properties[s]["value"]:
                self.gsProp(s, self.properties[s]["target"])

    def gsProp(self, prop, newval=None):
        if newval:
            newval = str(newval)
        else:
            newval = ""
        ret = self.connobject.send(self.properties[prop]["cmd"] + newval, address = str(self.address))[0]
        self.properties[prop].update({"value": ret})
