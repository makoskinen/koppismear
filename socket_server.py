#!/usr/bin/env python3

"""Socket server - a socket interface to koppismear. Listens to a port
for commands and passes them to a koppismear instance. Able to deliver
data points from outvars of the instance on demand. Currently
available commands include starting of data collection, starting run,
pausing and resuming data collection and run, delivering list of
available measurement sources and variables and manual setting of
measurement source.

Currently requires sudo rights for measurement user in order to set
raspberry pi system time (if dotimeset is True).

"""

import socket
import threading
import time
import pickle
import zlib
import selectors
import types
import os
import sys
import subprocess

from ReadIni import ReadIni

# class koppiWaiter:
#     def __init__(self, inifile, host=None, port=None, mountdev=None, mountdir=None):
#         self.inifile = inifile
#         self.stopper = threading.Event()
#         self.cfg = ReadIni(inifile)['waiter']
#         if 'host' in self.cfg:
#             self.host = self.cfg['host']
#         else:
#             self.host = host
#         if self.host is None:
#             exit("Need host address!")
#         if 'port' in self.cfg:
#             self.port = int(self.cfg['port'])
#         else:
#             self.port = int(port)
#         if self.port is None:
#             exit("Need host port!")
#         self.opensock()
#         while True:
#             if self.testsockClosed():
#                 self.opensock()
#             try:
#                 events = self.sock.accept(timeout=1)
#             except KeyboardInterrupt:
#                 self.sock.close()
#                 sys.exit(0)
#             if len(events) == 0:
#                 continue
#             if mountdir is not None:
#                 mountsuc = self.mountvolume(mountdir, "mount")
#                 if mountsuc =! 0:
# ## JATKA TÄSTÄ
#             self.sel = selectors.DefaultSelector()
#             self.sel.register(self.sock, selectors.EVENT_READ, data=None)
#             import koppi_alternate as koppi
#             gc = koppi.koppismear(self.inifile, forcestart=True)
#             ss = koppiServer(gc, host=gc.cfg["general"]["hostaddr"], port=port, stopper=stopper, sock=self.sock, sel=self.sel, inievents=events)
#             ssthread = threading.Thread(target = ss.run)
#             ssthread.start()
#             while ssthread.is_alive():
#                 try:
#                     time.sleep(1)
#                 except KeyboardInterrupt:
#                     stopper.set()
#                     ssthread.join()
#                     self.sel.unregister(self.sock)
#                     self.sock.close()
#                     self.sel.close()
#                     sys.exit(0)
                
        

#     def opensock(self):        
#         self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         for i in range(10):
#             try:
#                 self.sock.bind((self.host, self.port))
#             except OSError:
#                 time.sleep(0.2)
#                 continue
#             break
#         self.sock.listen()
#         self.sock.setblocking(False)

#     def testsockClosed(self) -> bool:
#         try:
#             data = self.sock.recv(16, socket.MSG_DONTWAIT | socket.MSG_PEEK)
#             if len(data) == 0:
#                 return True
#         except BlockingIOError:
#             return False
#         except ConnectionResetError:
#             return True
#         except Exception as e:
#             print("Unexpected exception while checking if socket is closed!")
#             return False
#         return False

#     def mountvolume(self, volume: str, action: str):
#         if not action in ["mount", "umount"]:
#             return 1
#         try:
#             ret = subprocess.check_call([action, volume])
#             return ret
#         except subprocess.CalledProcessError e:
#             return e.returncode

class koppiServer:
    def __init__(self, gc, host=None, port=None, stopper=None, sock=None, sel=None, inievents=None, skipvariables=['LGRTimestamp'], dotimeset=True):
        self.gc = gc
        self.datalock = self.gc.datalock
        self.latestlock = self.gc.latestlock
        self.outvars = self.gc.outvars
        self.latest = self.gc.latest
        self.skipvariables = skipvariables
        self.stopper = stopper
        self.dotimeset = dotimeset
        if self.sock is None:
            self.createsock(host, port)
            self.extsock = False
        else:
            self.sock = sock
            self.sel = sel
            self.extsock = True
        self.inievents = inievents
        self.doquit = threading.Event()

    def createsock(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        while True:
            try:
                self.sock.bind((host, port))
            except OSError:
                time.sleep(0.2)
                continue
            break
        self.sock.listen()
        self.sock.setblocking(False)
        self.sel = selectors.DefaultSelector()
        self.sel.register(self.sock, selectors.EVENT_READ, data=None)
        
    def run(self):
        if self.inievets is not None:
            self.processevents(self.inievents)
            self.inievents = None
        while True:
            time.sleep(0.01)
            events = self.sel.select(timeout=None)
            self.processevents(events)
            if self.stopper:
                if self.stopper.is_set():
                    self.sel.unregister(self.sock)
                    self.sock.close()
                    self.sel.close()
                    print("Socket server exiting")
                    return

    def processevents(self, events):
        for key, mask in events:
            if key.data is None:
                self.accept_wrapper(key.fileobj)
            else:
                self.service_connection(key, mask)

    def accept_wrapper(self, sock):
        conn, addr = sock.accept()
        print('accepted connection from', addr)
        conn.setblocking(False)
        data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self.sel.register(conn, events, data=data)

    def service_connection(self, key, mask):
        sock = key.fileobj
        data = key.data
        if mask & selectors.EVENT_READ:
            try:
                recv_data = sock.recv(4069)                
                if recv_data:
                    data.outb += recv_data
                else:
                    print('closing connection to', data.addr)
                    self.sel.unregister(sock)
                    sock.close()
            except ConnectionResetError:
                print('Connection reset to', data.addr, ', closing connection')
                self.sel.unregister(sock)
                sock.close()
                
        if mask & selectors.EVENT_WRITE:
            if data.outb:
                ret = self.command(data.outb.decode().strip())
#                print(repr(ret))
#                print('echoing', repr(data.outb), 'to', data.addr)
                ret = pickle.dumps(ret)
                data.outb = ret
                try:
                    sent = sock.send(ret)
                    data.outb = data.outb[sent:]
                except OSError:
                    pass

    def listener(self, sock):
        pass

    def command(self, cmd):
        cmd = cmd.split(".")
        print("Server commanded with", repr(cmd))
        if cmd[0] == "GETDATA":
            try:
                variables = cmd[1].split(",")
            except IndexError:
                return "ERR:NODATA"
            try:
                dt = int(cmd[2])
            except IndexError:
                dt = 0
            if len(cmd) == 5:
                dopickle = int(cmd[3])
                docompress = int(cmd[4])
            else:
                dopickle = 0
                docompress = 0
            out = self.servedata(dt, variables, dopickle, docompress)
            return out
        elif cmd[0] == "GETSOURCES":
            out = ",".join([s for s in list(self.gc.controlmanager.sources.keys()) if self.gc.controlmanager.sources[s]['record'] == 'True'])
            return out
        elif cmd[0] == "SETSOURCE":
            source = str(cmd[1])
            if not source in list(self.gc.controlmanager.sources.keys()):
                return "ERR:NOSOURCE"
            else:
                self.gc.setsource(source)
        elif cmd[0] == "GETCURRSOURCE":
            while self.gc.currsourcelock.locked():
                time.sleep(0.01)
            out = str(self.gc.currSource)
            return out
        elif cmd[0] == "GETVARIABLES":
            varlist = list(self.gc.outvars.keys())
            varlist = [a for a in varlist if a not in self.skipvariables]
            out = ",".join(varlist)
            return out
        elif cmd[0] == "GETRUNNING":
            out = repr(self.gc.controltimer.running.is_set())
            return out
        elif cmd[0] == "GETDATARUNNING":
            out = repr(self.gc.datarunning.is_set())
            return out
        elif cmd[0] == "GO":
            if self.gc.controltimer.running.is_set():
                return "ERR:RUNNING"
            if self.gc.dthread.is_alive():
                return "ERR:PAUSED"
            else:
                self.gc.go()
        elif cmd[0] == "DATAGO":
            if self.gc.datarunning.is_set():
                return "ERR:RUNNING"
            else:
                self.gc.datago()
        elif cmd[0] == "PAUSE":
            self.gc.pause()
        elif cmd[0] == "PAUSERUN":
            self.gc.pauserun()
        elif cmd[0] == "CONT":
            if self.gc.controltimer.running.is_set():
                return "ERR:RUNNING"
            self.gc.cont()
        elif cmd[0] == "STOP":
            self.gc.stop()
        elif cmd[0] == "QUIT":
            self.gc.quitnow()
            if self.stopper:
                self.stopper.set()
            self.doquit.set()
        elif cmd[0] == "SETTIME":
            if self.dotimeset:
                print("Setting time to %s" % time.ctime(float(cmd[1])))
                os.system('sudo date -s "%s"' % time.ctime(float(cmd[1])))
            else:
                print("No right to set time!")
        else:
            return "ERR:command %s not recognized" % str(cmd)
        return "OK"

    def servedata(self, dt=0, variables=None, dopickle=False, docompress=False):
        print("Servedata being asked for", repr(variables))
        if variables is None:
            variables = self.latest.keys()
        times = dict()
        data = dict()
        while self.latestlock.locked():
            sleep(0.01)
        self.latestlock.acquire()
        try:
            for variable in variables:
                try:
                    times[variable] = self.latest[variable].get()[0]
                except IndexError:
                    times[variable] = None
        finally:
            self.latestlock.release()
        while self.datalock.locked():
            time.sleep(0.01)
        self.datalock.acquire()
        try:
            for variable in list(times.keys()):
                data[variable] = dict()
                if dt == 0:
                    try:
                        data[variable][times[variable]] = self.outvars[variable][times[variable]]
                    except KeyError:
                        continue
                else:
                    for t in range(int(times[variable]) - dt + 1, int(times[variable]) + 1):
                        try:
                            data[variable][t] = self.outvars[variable][t]
                        except KeyError:
                            continue
        finally:
            self.datalock.release()
            if dopickle:
                data = pickle.dumps(data)
                if docompress:
                    data = zlib.compress(data)
            return data
