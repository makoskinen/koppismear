#!/usr/bin/python
# coding: utf-8
from time import sleep
import struct
import time

"""Module for Bürkert MFCs; uses HART protocol. Added a reader class
to enable recording of totalizer values."""


class mfc_mediator():
    """Mediator object for MFC"""

    def __init__(self, port, logger=None):
        self.port = port['obj']
        self.lock = port['lock']
        self.logger = logger
        self.errors = dict([(0x82, 'Overflow'), (0x88, 'Checksum'), (0x90, 'Framing'), (0xA0, 'Overrun'), (0xC0, 'Parity'), (0x02, 'Invalid selection'), (0x03, 'Parameter too large'), (0x04, 'Parameter too small'),
                            (0x05, 'Too few data bytes'), (0x07, 'Write protected'), (0x10, 'Access restricted'), (0x40, 'No command'), (0x20, 'Device busy'), (0x01, 'Timeout'), (0x41, 'Wrong command')])

    def send(self, cmd, reply=False):
        with self.lock:
            self.port.write(cmd)
            sleep(0.15)  # Give just a bit of time for the MFC to reply
            msg = self.getval()
        if reply:
            return(msg)

    def getval(self):
        starttime = time.time()
        while self.port.inWaiting == 0:
            if time.time() - starttime > self.port.timeout:
                self.logger.warning("No reply from mfc!")
                raise ValueError
        msg = self.port.read_until()
        if len(msg) == 0:
            self.logger.warning("No reply from mfc!")
        return(msg)

    def preparemsg(self, msg):
        """Add necessary stuff to the message bytes (start code, host
        address and destination address)"""
        self.msg = [0xFF, 0xFF, 0x02] + [self.hostaddress + self.name] + msg
        crc = self.xorcrc(self.msg)
        self.msg = self.msg + [crc]

    def sendmsg(self, msg):
        """Send the prepared message to the device"""
        self.preparemsg(msg)
        for att in range(5):
            try:
                repl = self.send(self.msg, reply=True)
                repl = self.processrep(repl)
            except Exception as ex:
                self.logger.debug('{}: Failed message at {}, ({}), trying again..'.format(
                    self.nick, time.strftime('%Y-%m-%s %H:%M:%S', time.localtime()), type(ex).__name__))
                self.port.flushInput()
                self.port.flushOutput()
                sleep(0.5)
            else:
                break
        return(repl)

    def stop(self):
        self.setstate(0)
        self.port.flushInput()
        self.port.flushOutput()
        pass

    def setstate(self, q):
        """Set flow value, q = % of maximum flow"""
        self.goal = q
        val = self.float2hex(q)
        self.sendmsg([0x92, 0x05, 0x01] + val)

    def getstate(self):
        """Read the current flow value as % of maximum"""
        repl = self.sendmsg([0x01, 0x00])
        # first byte is code for unit, 0x39 = %
        try:
            unit = repl["data"].pop(0)
        except TypeError:
            return((self.goal, 'NA'))
        try:
            val = self.tup2float(repl["data"])
        except TypeError:
            val = 'NA'
        return((self.goal, val))

    def setaddr(self, newaddr):
        """Set device address"""
        repl = self.sendmsg([0x06, 0x01, newaddr])
        self.name = newaddr
        print(repl)

    def writetoeeprom(self):
        repl = self.sendmsg([0x27, 0x01, 0x00])
        print(repl)

    def gettotal(self):
        """Get the totalizer value (accumulated flow since last reset in Nl)"""
        repl = self.sendmsg([0x96, 0x01, 0x00])
        gas = repl["data"].pop(0)
        unit = repl["data"].pop(0)
        val = self.tup2float(repl["data"])
        return((gas, unit, val))

    def zerototal(self):
        """Reset the totalizer"""
        self.sendmsg([0x97, 0x01, 0x00])
        self.logger.info('%s: Totalizer reset', self.nick)

    def getaddinfo(self):
        """Get additional information on the device"""
        repl = self.sendmsg([0x93, 0x00])
        errs = repl["data"][0:2]
        others = repl["data"][2:4]
        limits = repl["data"][4:6]
        res = repl["data"][6:8]
        return((errs, others, limits, res))

    def tup2float(self, tup):
        """Converts tuple to float"""
        return struct.unpack('!f', bytes(a for a in tup))[0]

    def float2hex(self, flt):
        """Converts float to hex"""
        ba = bytearray(struct.pack('>f', int(flt)))
        out = [b for b in ba]
        return out

    def xorcrc(self, inmsg, f='create'):
        """Calculate CRC by XOR algorithm"""
        try:
            msgl = [int(a, 16) for a in inmsg.split(' ')]
        except AttributeError:  # Sometimes we get tuple instead of string
            msgl = [b for b in inmsg]
        out = 0
        for i in range(len(msgl)):
            out = out ^ msgl[i]
        if f == 'check':
            if out != 0:
                raise ValueError('%s: Failed CRC (received %s)' %
                                 (self.nick, inmsg))
        elif f == 'create':
            return(out)

    def processrep(self, msg):
        """Process reply given by the MFC device"""
        docrc = True
        msg = struct.unpack('%sB' % len(msg), msg)
        # Often also the sent message is returned; find the beginning of the reply
        replstr = [255, 255, 6]
        msg = list(msg)
        self.logger.debug('MFC %s: msg length %s' % (self.nick, len(msg)))
        ind = [i for i in range(len(msg)) if msg[i:i+len(replstr)] == replstr]
        msg = msg[ind[0]:]
        databytes = int(msg[5])
        data = msg[3:6+databytes]
        crc = msg[6+databytes]
        # print msg
        if docrc:
            self.xorcrc([msg[2]] + data + [crc], f='check')
        # msg = ' '.join([format(a, '#04x') for a in msg])
        # databytes = msg[6]
        # if len(msg) < databytes + 8:
        #     self.logger.debug('MFC %s: missing end of message, padding with 0 and disabling CRC check' % self.nick)
        #     print msg
        #     msg = msg + [0] * (databytes + 8 - msg)
        #     print msg
        #     docrc = False
        # remove the initial bytes and device ID
        out = dict()
        out["deviceID"] = data[0]-self.hostaddress
        out["command"] = data[1]
        out["databytes"] = data[2]
        out["devstatus"] = data[3:5]
        if out["devstatus"] != [0, 0]:
            self.logger.warning("%s: %s", self.nick,
                                self.errors[out["devstatus"][0]])
        out["data"] = data[5:]
        # if len(out["data"]) < out["databytes"]:
        #     out["data"]
        return out


class control(mfc_mediator):
    """Controller object for Burkert MFC:s (HART protocol) - use serial
port straight"""

    def __init__(self, connobject, nick, name, value, **kwargs):
        self.connobject = connobject['obj']
        self.port = self.connobject.port
        self.lock = self.connobject.lock
        self.logger = self.connobject.logger
        self.errors = self.connobject.errors
        self.value = value
        self.goal = self.value[0]
        self.nick = nick  # nick = name of slave (MFC) to be used in logging
        self.name = int(name)  # name = address of slave (MFC) in decimal
        self.hostaddress = 128
        self.msleep = 0.2
        self.msg = None

    def on(self):
        pass

    def off(self):
        pass


class reader(mfc_mediator):
    def __init__(self, connobject, nick, name, variables, interval, **kwargs):
        self.connobject = connobject['obj']
        self.port = self.connobject.port
        self.lock = self.connobject.lock
        self.logger = self.connobject.logger
        self.errors = self.connobject.errors
        self.variables = variables.split(',')
        self.interval = int(interval)
        self.nick = nick  # nick = name of slave (MFC) to be used in logging
        self.name = int(name)  # name = address of slave (MFC) in decimal
        self.hostaddress = 128
        self.msleep = 0.2
        self.msg = None
        self.outdata = dict()

    def read(self):
        """Read totalizer value"""
        val = self.gettotal()
        self.outdata[self.variables[0]] = val[2]
        return(self.outdata)
