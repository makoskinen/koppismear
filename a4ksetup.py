#!/usr/bin/python3
""" utility script for setting addresses of ADAM modules"""
import sys, getopt, adam, serial

def main(argv):
    portbaud = 9600
    old = 0
    new = None
    portpath = None
    try:
        args, opts = getopt.getopt(argv, "hp:b:o:n:", ["help","port=","baudrate=","old=","new="])
    except getopt.GetoptError:
        print("a4ksetup.py -p <path-to-port> -b <baudrate (default 9600)> -o <oldaddress (default 0)> -n <newaddress>")
        sys.exit(2)
    for opt, arg in args:
        if opt == '-h':
            print("a4ksetup.py -p <path-to-port> -b <baudrate (default 9600)> -o <oldaddress (default 0)> -n <newaddress>")
            sys.exit()
        elif opt in ("-p", "--port"):
            portpath = arg
        elif opt in ("-b", "--baudrate"):
            portbaud = arg
        elif opt in ("-o", "--old"):
            old = arg
        elif opt in ("-n", "--new"):
            new = arg
    if None in [portpath, new]:
        print("Path to serial port (-p <path-to-port>) and new address for Adam module (-n <newaddress>) required!")
        sys.exit(1)
    port = serial.Serial(portpath, baudrate = portbaud)
    ao = adam.adamsetup(port, old)
    ao.changeaddr(new)

if __name__ == '__main__':
    main(sys.argv[1:])
