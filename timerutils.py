""" utilities for calculating next time divisible by certain amount of time, defined as weeks, days, hours, minutes, seconds
"""
import time
import re

def nexteven(timedefn, now = None):
    div = divsum(timedefn)
    if now is None:
        now = time.time()
    tthen = (now // div + 1) * div
    ## print("Now is {}, next time divisible by {} is {}".format(time.ctime(now), div, time.ctime(tthen)))
    return tthen

def preveven(timedefn, now = None):
    div = divsum(timedefn)
    if now is None:
        now = time.time()
    tthen = ((now - div) // div + 1) * div
    return tthen

def timestampmult(stamp, mults = {"w": 86400*7, "d": 86400, "h": 3600, "m": 60, "s": 1, "f": 0.001}):
    splits = "(["+"".join(mults.keys())+"])"
    stamp = re.split(splits, stamp)
    stamp[0] = int(stamp[0])
    out = stamp[0] * mults[stamp[1]]
    return out

def timedefnsplit(defn, splits = ["w", "d", "h", "m", "s", "f"]):
    defn = re.sub("[^a-z^0-9]", "", defn)
    defn = re.findall("([0-9]*["+"".join(splits)+"])", defn)
    return defn

def divsum(timedefn):
    try:
        return(float(timedefn))
    except ValueError:
        timedefn = timedefnsplit(timedefn)
        div = sum([timestampmult(a) for a in timedefn])
        return div

def sleep_until(stime, precision = 10, debug=False, breaker = None):
    if debug:
        print("Now {}, sleeping until {}".format(time.ctime(time.time()), time.ctime(stime)))
    timediv = (stime - time.time()) / precision
    while time.time() < stime:
        if breaker:
            break
        if debug:
            print("check")
        time.sleep(timediv)
    if debug:
        print("Now {}".format(time.ctime(time.time())))
