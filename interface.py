#!/usr/bin/env python3

import kivy
import time
import koppi_alternate as koppi

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.clock import Clock
from kivy_garden.graph import Graph, MeshLinePlot
# from functools import partial
from utils import NiceScale


class varDrop(DropDown):
    def __init__(self, variables, **kwargs):
        super(varDrop, self).__init__(**kwargs)
##        print("Creating var dropdown with variables %s" % variables)
        self.variables = variables
##        self.menu = DropDown()
        for index in range(len(self.variables)):
##            print("Creating option for %s" % self.variables[index])
            btn = Button(text=str(self.variables[index]), size_hint_y = None, height = 40)
            btn.bind(on_release=lambda btn: self.select(btn.text))
            self.add_widget(btn)


class varBtn(Button):
    def __init__(self, variable, gc, graph, timelim=600, **kwargs):
        super(varBtn, self).__init__(**kwargs)
        self.variable = variable
        self.gc = gc
        self.graph = graph
        self.plot = MeshLinePlot(color=[1, 0, 0, 1])
        self.graph.add_plot(self.plot)
        self.timelim = timelim
        self.data = list()
        self.datalock = self.gc.datalock
        self.latestlock = self.gc.latestlock
        self.time = round(time.time())

    def update(self):
##        print("Trying to update for button %s" %str(self))
        if not self.gc.datarunning.is_set():
##            print("Data not running")
            if self.variable == None:
                return
            else:
                self.text = str(self.variable)
                return
        if self.variable == None:
            return
        while self.latestlock.locked():
            time.sleep(0.01)
        # print("1.1")
        self.latestlock.acquire()
        # print("1.2")
        try:
            self.time = self.gc.latest[self.variable].get()[0]
        except IndexError as e:
            # print(e)
            self.latestlock.release()
            return
        # print(self.time)
        self.latestlock.release()
        # print("1.4")
        while self.datalock.locked():
            time.sleep(0.01)
        # print("1.5")
        self.datalock.acquire()
        # print(str(self.variable) + str(self.time))
        try:
            self.data.append((int(self.time), float(self.gc.outvars[str(self.variable)][int(self.time)])))
        except (ValueError, TypeError, IndexError):
            print("Faulty data in %s" % self.variable)
            self.datalock.release()
            return
        # print("Data appended")
        self.data = [i for i in self.data if i[0] > (self.time - self.timelim)]
        self.plot.points = [(x, y) for x, y in self.data]
        # print(str(len(self.data)), "points appended")
        if(max([x[0] for x in self.data]) > (self.graph.xmax - int(self.timelim / 10))):
            # print("Updating x limits")
            self.graph.xmin = self.graph.xmin + int(self.timelim / 10)
            self.graph.xmax = self.graph.xmax + int(self.timelim / 10)
        if len(self.data) > 1 :
            self.nicelims = NiceScale(min([y[1] for y in self.data]), max([y[1] for y in self.data]))
            # print("Calculated lims")
            self.graph.ymin = self.nicelims.minPoint
            self.graph.ymax = self.nicelims.maxPoint
            self.graph.y_ticks_major = self.nicelims.tickSpacing
            self.graph.y_ticks_minor = 0
        self.graph.ylabel = str(self.variable)
        self.plot.draw()
        self.text = str(self.variable) + str('\n') + str(self.gc.outvars[str(self.variable)][int(self.time)])
        # print("1.7")
        self.datalock.release()
        # print("1.8")

    def set_variable(self, variable):
##        print("Setting variable for %s" % self)
        self.variable = str(variable)
        self.data = list()
        while self.datalock.locked():
            sleep(0.01)
##        print("2.1")
        self.datalock.acquire()
        mtimes = [int(a) for a in list(self.gc.outvars[str(self.variable)].keys()) if a >= self.time - self.timelim]
##        print("2.2")
        for t in mtimes:
            try:
                self.data.append((int(t), float(self.gc.outvars[str(self.variable)][t])))
            except (ValueError, TypeError):
                pass
        self.datalock.release()
##        print(str(self) + ' ' + self.variable)


class koppiLayout(GridLayout):
    def __init__(self, gc, maxvars = 8, mode = 'auto', datakeeptime = 600, **kwargs):
        super(koppiLayout, self).__init__(**kwargs)
        self.gc = gc
        self.cols = 1
        self.mode = mode
        self.datakeeptime = datakeeptime
        self.on_start()

    def on_start(self):
        self.selbar = BoxLayout(orientation='horizontal')
        self.infobar = BoxLayout(orientation='horizontal')
        self.graphbar = BoxLayout(orientation='horizontal')
        self.add_widget(self.selbar)
        self.add_widget(self.infobar)
        self.add_widget(self.graphbar)
        self.sourcelist = [k for k in list(self.gc.sourcecfg.keys()) if k != 'general']
        self.sourcemenu = DropDown()
        for index in range(len(self.sourcelist)):
            btn = Button(text = self.sourcelist[index], size_hint_y = None, height = 40)
            btn.bind(on_release=lambda btn: self.sourcemenu.select(btn.text))
            self.sourcemenu.add_widget(btn)
        self.varlist = list(self.gc.outvars.keys())
        self.varmenu1 = varDrop(self.varlist)
        self.graph1 = Graph(xlabel='time', ylabel='none', x_ticks_minor=5, x_ticks_major=25, y_ticks_minor=5, y_ticks_major=25, x_grid_label=True, y_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=int(time.time()), xmax=int(time.time())+self.datakeeptime, ymin=0, ymax=1000)
        self.varbutton1 = varBtn(text = "Variable", variable = None, gc = self.gc, graph=self.graph1, timelim=self.datakeeptime, size_hint = (None, None))
        self.varbutton1.bind(on_release = self.varmenu1.open)
        self.varmenu1.bind(on_select = lambda instace, x: self.varbutton1.set_variable(str(x)))
        self.infobar.add_widget(self.varbutton1)
        self.varmenu2 = varDrop(self.varlist)
        self.graph2 = Graph(xlabel='time', ylabel='none', x_ticks_minor=5, x_ticks_major=25, y_ticks_minor=5, y_ticks_major=25, x_grid_label=True, y_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=int(time.time()), xmax=int(time.time())+self.datakeeptime, ymin=0, ymax=1000)
        self.varbutton2 = varBtn(text="Variable", variable=None, gc=self.gc, graph=self.graph2, timelim=self.datakeeptime, size_hint=(None, None))
        self.varbutton2.bind(on_release = self.varmenu2.open)
        self.varmenu2.bind(on_select = lambda instace, x: self.varbutton2.set_variable(str(x)))
        self.infobar.add_widget(self.varbutton2)
        self.varbuttons = [self.varbutton1, self.varbutton2]
        self.graphbar.add_widget(self.graph1)
        self.graphbar.add_widget(self.graph2)
        # for index in range(min([len(self.varlist), maxvars])):
            # self.varcells.append(DropDown())
            # for i in range(len(self.varlist)):
            #     print("Adding option %s" % self.varlist[i])
            #     btn1 = Button(text = str(self.varlist[i]), size_hint_y = None, heigth = 40)
            #     print("Created button")
            #     btn1.bind(on_release=lambda btn: self.varcells[index].select(btn.text))
            #     print("Bound select method")
            #     self.varcells[index].add_widget(btn1)
            #     print("Added widget")
            # self.varcells.append(varDrop(self.varlist))
            # self.varbuttons.append(varBtn(text = "Variable", variable = None, gc = self.gc, size_hint = (None, None)))
            # print("Added varBtn %s" % self.varbuttons[index])
            # self.varbuttons[index].bind(on_release = self.varcells[index].open)
            # print("Bound open menu")
            # # self.varcells[index].bind(on_select = lambda instance, x: self.varbuttons[index].set_variable(str(x)))
            # # self.varfuncs[index] = lambda instance, x: self.varbuttons[index].set_variable(str(x))
            # # print("Created varfunc")
            # self.infobar.add_widget(self.varbuttons[index])
        # self.varfuncs
        # for index in range(len(self.varcells)):
        #     self.varcells[index].bind(on_select = self.varfuncs[index])
        self.sourcebutton = Button(text = "Source", size_hint = (None, None))
        self.sourcebutton.bind(on_release = self.sourcemenu.open)
        self.sourcemenu.bind(on_select = lambda instance, x: self.source_set(instance, x))
        self.selbar.add_widget(self.sourcebutton)
        self.gobutton = Button(text="Go", size_hint=(None, None))
        self.gobutton.bind(on_release= self.go)
        if self.mode == 'manual':
            self.gobutton.disabled = True
        self.pausebutton = ToggleButton(text="pause", size_hint=(None, None))
        self.pausebutton.bind(on_release=self.pausetoggle)
        if self.mode == 'manual':
            self.pausebutton.disabled = True
        self.datagobutton = ToggleButton(text="Record data", size_hint=(None, None))
        self.datagobutton.bind(on_release=self.datatoggle)
        self.selbar.add_widget(self.datagobutton)
        self.selbar.add_widget(self.gobutton)
        self.selbar.add_widget(self.pausebutton)
        self.sourcelabel = Label(size_hint=(100, None))
        self.timelabel = Label(size_hint=(200, None))
        self.infobar.add_widget(self.sourcelabel)
        self.infobar.add_widget(self.timelabel)
        self.exitbutton = Button(text="Exit", size_hint=(None, None))
        self.exitbutton.bind(on_release=self.exitprogram)
        self.selbar.add_widget(self.exitbutton)

    def datatoggle(self, instance):
        if instance.state == 'normal':
            self.gc.pause()
        else:
            if instance.state == 'down':
                self.gc.datago()

    def pausetoggle(self, instance):
        if instance.state == 'down':
            self.gc.pause()
            self.sourcebutton.disabled = False
            self.datagobutton.disabled = False
            self.gobutton.text = 'Paused'
        else:
            if instance.state == 'normal':
                self.gc.cont()
                self.sourcebutton.disabled = True
                self.datagobutton.disabled = True
                self.gobutton.text = 'Running'

    def source_set(self, instance, source):
        self.gc.setsource(source)
        setattr(self.sourcebutton, 'text', source)

    def variable_set(self, instance, btn, variable):
##        print("Setting variable for %s!" % btn)
        setattr(btn, 'variable', str(variable))
##        print("Variable set to" % getattr(btn, 'variable'))

    def update(self, dt):
##        print("Updating..")
        while self.gc.currsourcelock.locked():
            time.sleep(0.01)
##        print(1)
        self.gc.currsourcelock.acquire()
##        print(2)
        source = self.gc.currSource
##        print(3)
        self.gc.currsourcelock.release()
##        print(4)
        self.sourcelabel.text = str(source)
##        print(5)
##        print(len(self.varbuttons))
        for i in range(len(self.varbuttons)):
##            print(i)
            self.varbuttons[i].update()
        self.timelabel.text = str(time.asctime())

    def go(self, instance):
        self.gc.go()
        self.sourcebutton.disabled = True
        self.datagobutton.disabled = True
        self.gobutton.text = 'Running'

    def exitprogram(self, instance):
##        print("GC stopping..")
        self.gc.stop()
##        print("GC stopped")
        self.gc.quitnow()
##        print("GC quitted")
        App.stop(self)
        Window.close(self)


class koppiAppi(App):
    def __init__(self, gc=None, mode='auto', interval = 2, inifile = 'faux.ini', **kwargs):
        super(koppiAppi, self).__init__(**kwargs)
        if gc == None:
            self.gc = koppi.koppismear(inifile)
        else:
            self.gc = gc
        self.interval = interval
        self.mode=mode
    
    def build(self):
        ourLayout = koppiLayout(self.gc, mode=self.mode)
        Clock.schedule_interval(ourLayout.update, int(self.interval))
        return ourLayout
    
    def on_stop(self, instance):
        pass



if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inifile", help="Name of main ini file of koppismear", type=str, default="faux.ini")
    parser.add_argument("-m", "--mode", help="Type of interface, auto or manual", type=str, default="auto")
    args = parser.parse_args()
    print(args)
    Appi = koppiAppi(inifile=args.inifile, mode=args.mode)
    Appi.run()
