#!/usr/bin/python
# -*- coding: UTF-8

"""

"""

import serial
import time
import struct
from time import sleep


class mbed:  # Basic connection object for mbed board
    def __init__(self, cfg=None, port=None, timeout=2, inifile=None, msleep=0.1, logger=None):
        print(port)
        self.lock = port['lock']
        self.logger = logger
        if cfg is None and port is None:
            raise KeyError('Need cfg or parameter value for serial port!')
        if cfg is not None:
            cfg = cfg['main']
            if 'sport' in cfg:
                if port is None:
                    port = cfg['sport']
            if 'timeout' in cfg:
                self.timeout = cfg['timeout']
            else:
                self.timeout = timeout
            if 'inifile' in cfg:
                self.inifile = cfg['inifile']
            else:
                self.inifile = inifile
            if 'msleep' in cfg:
                self.msleep = cfg['msleep']
            else:
                self.msleep = msleep
        self.port = port['obj']
        self.msleep = float(msleep)
        self.send('write rtc %s' % int(time.time()))
        if inifile is not None:
            with open(inifile, 'r') as f:
                for l in f:
                    self.send(l)

    def acquire(self):  # Deprecated, use with lock statement
        if not self.lock.acquire(timeout=1):
            raise IOError

    def release(self):  # deprecated, use with lock statement
        self.lock.release()

    def stop(self):
        pass

    def send(self, cmd, reply=False):
        with self.lock:
            if not cmd.endswith('\n'):
                cmd = cmd + '\n'
                # print(cmd)
            self.port.flushInput()
            self.port.write(cmd.encode('utf-8'))
            msg = self.getval()
            if msg is None:
                return msg
            msg = msg.decode()
        if reply:
            return msg

    def getval_old(self):
        iw = self.port.inWaiting()
        sleep(self.msleep)
        w = self.port.inWaiting()
        while w > iw:
            iw = w
            sleep(self.msleep)
            w = self.port.inWaiting()
        if w == 0:
            print("No reply from mbed!")
            return None
        msg = self.port.read(w)
        return msg.strip('\r\n')

    def getval(self):
        msg = self.port.read_until()
        if not msg:
            print("No reply from mbed!")
            return None
        return msg.strip(b'\r\n')


class onoff(mbed):  # controller object for controlling ON-OFF-type devices like valves
    def __init__(self, connobject, nick, name, value, **kwargs):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger
        # The lock is not necessary, might remove this line in the future
        self.lock = self.connobject.lock
        self.msleep = self.connobject.msleep
        self.port = self.connobject.port
        self.value = value
        self.nick = nick  # Nickname to be used in logging
        self.name = name  # name of channel in mbed; usually do0-4
        self.off()

    def stop(self):
        return

    def off(self):  # deprecated; use setstate instead
        self.send('write %s 0' % self.name)
        self.state = 0

    def on(self):  # deprecated; use setstate instead
        self.send('write %s 1' % self.name)
        self.state = 1

    def setstate(self, val):
        self.send('write %s %s' % (self.name, val))
        self.state = val

    def getstate(self):
        msg = self.send('read %s' % (self.name), reply=True)
        return (self.state, msg)


class mfc(mbed):
    # Controller object for Burkert MFC:s (HART
    # protocol) !!! Not used, use mfc_standalone class
    # instead!
    def __init__(self, connobject, nick, name, value, **kwargs):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger
        self.lock = self.connobject.lock  # Lock is not necessary
        self.value = value
        self.nick = nick  # nick = name of slave (MFC) to be used in logging
        self.name = name  # name = address of slave (MFC) in decimal
        self.hostaddress = 128
        self.errors = dict([(0x82, 'Overflow'), (0x88, 'Checksum'), (0x90, 'Framing'), (0xA0, 'Overrun'), (0xC0, 'Parity'), (0x02, 'Invalid selection'), (0x03, 'Parameter too large'), (0x04, 'Parameter too small'),
                            (0x05, 'Too few data bytes'), (0x07, 'Write protected'), (0x10, 'Access restricted'), (0x40, 'No command'), (0x20, 'Device busy'), (0x01, 'Timeout'), (0x41, 'Wrong command')])

    def on(self):
        pass

    def off(self):
        pass

    def preparemsg(self, msg):
        self.msg = '0xFF 0xFF 0x02 ' + \
            hex(self.hostaddress + self.name) + ' ' + msg
        crc = self.xorcrc(self.msg)
        self.msg = self.msg + ' ' + crc

    def sendmsg(self, msg):
        self.preparemsg(msg)
        repl = self.send('write message %s\n' % self.msg, reply=True)
        repl = self.processrep(repl)
        return(repl)

    def setstate(self, q):  # Set flow value, q = % of maximum flow
        self.goal = q
        val = self.float2hex(q)
        self.sendmsg('0x92 0x05 0x01 ' + ' '.join(val))

    def getstate(self):  # Read the current flow value as % of maximum
        repl = self.sendmsg('0x01 0x00')
        unit = repl["data"].pop(0)
        val = self.tup2float(repl["data"])
        return((self.goal, val))

    def setaddr(self, newaddr):
        repl = self.sendmsg('0x06 0x01 %s' % hex(newaddr))
        self.name = newaddr
        print(repl)

    def writetoeeprom(self):
        repl = self.sendmsg('0x27 0x01 0x00')
        print(repl)

    def gettotal(self):
        repl = self.sendmsg('0x96 0x01 0x00')
        gas = repl["data"].pop(0)
        unit = repl["data"].pop(0)
        val = self.tup2float(repl["data"])
        return((gas, unit, val))

    def getaddinfo(self):
        repl = self.sendmsg('0x93 0x00')
        errs = repl["data"][0:2]
        others = repl["data"][2:4]
        limits = repl["data"][4:6]
        res = repl["data"][6:8]
        return((errs, others, limits, res))

    def tup2float(self, tup):
        return struct.unpack('!f', ''.join(chr(a) for a in tup))[0]

    def float2hex(self, flt):
        ba = bytearray(struct.pack('>f', flt))
        out = ["0x%02x" % b for b in ba]
        return out

    def xorcrc(self, inmsg, f='create'):
        try:
            msgl = [int(a, 16) for a in inmsg.split(' ')]
        except AttributeError:  # Sometimes we get tuple instead of string
            msgl = [b for b in inmsg]
        out = 0
        for i in range(len(msgl)):
            out = out ^ msgl[i]
        if f == 'check':
            if out != 0:
                raise ValueError('Failed CRC')
        elif f == 'create':
            return(format(out, '#04x'))

    def processrep(self, msg):
        # Often also the sent message is returned; find the beginning of the reply
        msg = struct.unpack('%sB' % len(msg), msg)
        replstr = [255, 255, 6]
        msg = list(msg)
        ind = [i for i in range(len(msg)) if msg[i:i+len(replstr)] == replstr]
        msg = msg[ind[0]:]
        self.xorcrc(msg, f='check')
        # msg = ' '.join([format(a, '#04x') for a in msg])
        # remove the initial bytes and device ID
        msg = msg[3:]
        msg = msg[:len(msg)-1]  # remove CRC
        out = dict()
        out["deviceID"] = msg[0]-self.hostaddress
        out["command"] = msg[1]
        out["databytes"] = msg[2]
        out["devstatus"] = msg[3:5]
        if out["devstatus"] != [0, 0]:
            print(self.nick + ':' + self.errors[out["devstatus"][0]])
        out["data"] = msg[5:]
        return(out)


class sensor(mbed):
    """Reads the sensors defined in mbed's initiation file"""

    def __init__(self, connobject, variables, interval, **kwargs):
        self.connobject = connobject['obj']
        self.logger = self.connobject.logger
        self.port = self.connobject.port
        self.msleep = self.connobject.msleep
        self.lock = self.connobject.lock
        self.createsout(variables)
        self.interval = interval

    def on(self):
        pass

    def off(self):
        pass

    def createsout(self, variables):
        """Creates the sout channel in mbed"""
        self.send('create format sout', reply=False)
        soutlen = len(variables.split(','))
        soutstr = ['%s'] * soutlen
        soutstr = ','.join(soutstr)
        self.variables = variables.split(',')
        variablestr = variables.split(',')
        variablestr = ' '.join(variablestr)
        self.send('setup sout "%s\\n" %s' % (soutstr, variablestr))

    def read(self):
        """Read the sout channel and return results as dict"""
        self.send('write sout', reply=False)
        sleep(self.msleep)
        msgin = self.send('read sout', reply=True)
        msgin = msgin.split(',')
        msg = dict(list(zip(self.variables, msgin)))
        return msg


class MFCError(Exception):
    pass
