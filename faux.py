#!/usr/bin/python

import random
from time import sleep

import time


class fauxport:
    def __init__(self, port):
        self.port = port


class fauxconn:
    def __init__(self, port, logger, name="fauxconn"):
        self.port = port['obj']
        self.lock = port['lock']
        self.name = name
        self.logger = logger.getChild(name)


class fauxsink:
    def __init__(self, connobject, interval):
        self.interval = interval
        self.variables = 'fauxCO2,fauxCH4'

    def write(self, row):
        pass


class fauxsrc:
    def __init__(self, connobject, variable, interval, failrate=0, slowrate=0, faultrate=0, mini=100, maxi=200, timeprec=3):
        self.lock = connobject['obj'].lock
        self.interval = interval
        self.variables = variable.split(',')
        self.fail = [1] * failrate + [0] * (100 - failrate)
        self.slow = [1] * slowrate + [0] * (100 - slowrate)
        self.fault = [1] * faultrate + [0] * (100 - faultrate)
        self.mini = mini
        self.maxi = maxi
        self.sleeptime = eval(f"1e-{timeprec + 2}")

    def read(self):
        while self.lock.locked():
            sleep(self.sleeptime)
        self.lock.acquire()
        if random.choice(self.fail):
            print("faux fail! %s" % int(time.time()))
            self.lock.release()
            return
        if random.choice(self.slow):
            print("faux slow! %s" % int(time.time()))
            sleep(random.uniform(0, 6*self.sleeptime))
        self.lock.release()
        if not random.choice(self.fault):
            outdata = dict([(v, round(random.uniform(self.mini, self.maxi), 3))
                            for v in self.variables])
        else:
            print("faux fault! %s" % int(time.time()))
            outdata = dict([(v, 'abcdkissakävelee') for v in self.variables])
        return(outdata)

    def stop(self):
        pass


class fauxsrc_slow(fauxsrc):
    def __init__(self, connobject, variable, interval, mini=100, maxi=200):
        self.interval = interval
        self.variables = variable.split(',')
        self.mini = mini
        self.maxi = maxi

    def read(self):
        sleep(random.uniform(3, 5))
        return(dict([(v, round(random.uniform(self.mini, self.maxi), 3)) for v in self.variables]))


class fauxcontrol(fauxsrc):
    def __init__(self, connobject, nick, name, value):
        self.connobject = connobject
        self.state = int()
        self.nick = nick
        self.name = name
        self.value = value

    def setstate(self, val):
        self.state = int(val)

    def getstate(self):
        return(self.state)

    def zerototal(self):
        return(True)


class fauxlogger:
    def __init__(self):
        pass

    def debug(self, msg):
        pass

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def critical(self, msg):
        pass
